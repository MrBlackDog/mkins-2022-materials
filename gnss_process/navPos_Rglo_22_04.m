%% Загрузка .mat файла из Rinex 2.11 наблюдений
load('GNSS.mat')
%%
% Входные данные:
%
c = 299792458;
%
%%
% сохранить gnss_struct,TOW1,EPH,EPH_DMY, 
% time_block переделать и сохранить
%%
Xb=[ 2846079.9100  2200502.1000  5249252.6600];%м !!%%, м WGS-84. из файла стенда "0-база"
Vb=[0 0 0]; %% в ГЦСК метры/сек

T_temp=.2;%.1;
sigmaA=20.;%20;%8; %меньше проблема сужать полосу??
%%% Дисперсия приращений ускорения в динам. модели потребителя, разм. м^2/с^2. %%%
Dksidyn=0.2*sigmaA^2*0.005;
%
%Выходные данные: 

% %%%%%%%%%%%%%%%%%%%%%

%Ввод входных данных

Lam1lit0=c/1602e6;%
deltT=0.2; %сек, период записи данных в исходном rinex-файле
%% Разбор измерений
% N = length(gnns_struct);
N = 100;
iStart=1;%7000

[ B, L, H ] = XYZ_to_BLH_GOST32453_2013( Xb );
	
NED2ECEF = [	-sind(B)*cosd(L)	-sind(L)	-cosd(B)*cosd(L);
				-sind(B)*sind(L)	cosd(L)		-cosd(B)*sind(L);
				cosd(B)				0			-sind(B)];
ECEF2NED = NED2ECEF';

%%
NoSV=4;%5;% макс.количество видимых НКА
% SV_pos = zeros(12, 1+4+1+2); % sv X Y Z dT CodeRange Az Elev
Result = zeros(N, 4);
ResultF = NaN(N, 8);
dPos = NaN(N, 3);
dPos2 = NaN(N, 3);
ErrENU = NaN(N, 3);
dTr = NaN(N,1);
dTrf = NaN(N,1);
dVrf = NaN(N,1);
% XSV= NaN(N,3);
dObs=NaN(N,NoSV);
dPhR=NaN(N,NoSV);
dObs0=NaN(N,24);
dPhR0=NaN(N,24);
PRCorr=NaN(N,NoSV);

Xr = [0;0;0;0;0;0;0;.008];% Нач приближение фильтра
dF = 0; % ВПРОК для поправки НКА по частоте

%%
% NGR_time = time;%TOW1 заменить на time надо загрузить в мат файл.
NGR_time = TOW1+(3*3600);% (3*3600) - разница между UTC и GPST
%%%%проверка равных интервалов обсерваций
iM = 1;
dTime = diff(NGR_time);
intMax = find(abs(dTime-deltT)>deltT*0.5)+1;
if isempty(intMax)
%     iM = 1;
    intMax(iM) = NaN;
end    

%     [F,G,Q,E]=InitNavPosFK_RCM3_v0(c,T_temp,Dksidyn);
    [F,G,Q,E]=InitNavPosFK_R99v0(c,T_temp,Dksidyn);
%     [F,G,Q,E]=InitNavPosFK_RCM3_v01delt(c,T_temp,Dksidyn);

N_mnk=1;
iepCorr=10000;% момент начала коррекция

limCor=1000;%500;
iep4end = 0;
PosCorr = [];
RRg = NaN(N,4);
VVg = NaN(N,4);

iepLimX = [];
%%
for i=iStart:N
	if NGR_time(i)==0 
		break
	end
	
	% время(секунды) внутри суток
	t = rem(NGR_time(i), 86400);

	% теперь для каждого НКА в отсчёте ищем ближайшие эфемериды по Tb и рассчитываем положение НКА
	m = 1;
%      SV_pos = zeros(12, 1+4+1+2);
%      SV_vel = NaN(12, 6);
    SV_pos0 = NaN(24, 8);
    SV_vel0 = NaN(24, 6);
    PhaseRange0 =  NaN(24,1);
%     PhaseRangePrev = NaN(1,12);
%     PhaseRange= NaN(1,12);
    
%    for j=1:24 %12
    for j=1:length(gnss_struct(i).GLO_obs)
		sv = str2double(gnss_struct(i).GLO_obs(j).sv(2:3));
		if (sv == 0)
%             disp(sv)
			break
		end
		
		if (~isempty(EPH{sv}))
			Tbs = [EPH{sv}.Tb_sec]';
			[a,idx] = min(abs(Tbs - t));
			% idx - номер блока эфемерид которые надо использовать
% 			CodeRange = NGR{i}(j).CodeRange;
%             PhaseRange(m) = NGR{i}(j).PhaseRange;
%             PhaseRange0(sv,1) = NGR{i}(j).PhaseRange;
            CodeRange = gnss_struct(i).GLO_obs(j).CodeRange_L1;
            PhaseRange = gnss_struct(i).GLO_obs(j).Phase_L1; %!!! фаза в циклах берется из структуры
% 			[ X, Y, Z, dT ] = satpos_glo(t, CodeRange, EPH{sv}(idx), Xb);% работало!
            [ Xsv, dX, ddX, dT ] = satpos_Glo1(t, CodeRange, EPH{sv}(idx), Xb); % переделать обращение EPH{sv}(idx) 
            % До сих работает на 07_04 19:28 по мск
            X=Xsv(1);
            Y=Xsv(2);
            Z=Xsv(3);
			Kecef = [X Y Z] - Xb;
			NED = ECEF2NED * Kecef';

			elevation = atan2d( -NED(3), sqrt(NED(1)*NED(1) + NED(2)*NED(2) ) );
			azimuth = atan2d( NED(2), NED(1) );
			if azimuth < 0
				azimuth = azimuth + 360.0;
			end

			if (elevation > 1.0)% изм. 10 на 1.0 град.
                
                %%%%%%%%%%%%% tropo corr      
%             [~,el,~] = topocent(Xr(1:3),Rot_X-Xr(1:3));                                                            
%             El(i,1)=el;
%                 trop = tropo(sin(elevation*deg2rad),0.0,1013.0,293.0,50.0,...
%                     0.0,0.0,0.0);
%                 CodeRange=CodeRange-trop; % коррекция: ПД - тропо
            

                SV_pos0(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation];
%
                SV_vel0(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF NaN];
				m = m + 1;
			end
		end
   end


         %%%%%% дополнено! Отбор только видимых по порядку убывания номеров НКА
     SV_pos = SV_pos0(~isnan(SV_pos0(:,1)),:);%
     SV_vel = SV_vel0(~isnan(SV_vel0(:,1)),:);


    
        %%%%%%%%%%%%%%% приращение ПД для каждого НКА  {Iono Corr Krasn }
     if i>iStart

         dObs0(i,:) = (SV_pos0(:,6) - ObsPrev0(:,6))';%приращение ПД для каждого НКА 
         dPhR0(i,:) = PhaseRange0(:) - PhaseRangePrev0(:);

        for k=1:numel(SV_pos(:, 1)) %NoSV
          dObs(i,k) =  dObs0(i,SV_pos(k, 1));
          dPhR(i,k) =  dPhR0(i,SV_pos(k, 1));
        end

     end
end