function [Xr,E]=NavPosFK_99_MKINS(Xr,N_mnk,SV_pos,SV_vel,KALMAN_ENABLE,F,Q,E,T_temp)
%% !! вариант только для ГЛОНАСС измерений!!
%%  SV_pos,SV_vel - это координаты и вектор скорости НКА
%% SV_pos(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation]
%% измерения: по коду - CodeRange, могут быть сглажены! до фильтрации, метры
%             по фазе - скорректированы на скачки цикла фазы, приращение фазы на такт! ФК , метры!
%% SV_vel(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF dPhR]; 

    c = 299792458; % скорость света
    %---------- MNK implementation --------------------
    [Nvis1,~] = size(SV_pos);
    Nvis_GPS1=0;%Nvis_GPS-enable_lostG*num_lost_gps;
    
    H=zeros(Nvis1+Nvis_GPS1,4);
    dR=zeros(Nvis1+Nvis_GPS1,1);
    Vg= NaN(1,Nvis1);% это служебный склад для текущих невязок по радиальной скорости
    Rg= NaN(1,Nvis1);% это служебный склад для текущих невязок по радиальной дальности

    if (KALMAN_ENABLE==1)
        time_count_kalman_on=2;%10;
    elseif (KALMAN_ENABLE==0)
        time_count_kalman_on=10e9;%мс!! 10е9 это больше месяца!! было 10000 мс;
    end

    if (N_mnk < time_count_kalman_on)    
    %%%%%% MNK implemenation %%%%
    for k=1:5 % итерации МНК

        Coords=Xr(1:3);
        Rcl=Xr(4);
        %%%GLONASS%%%
        for m=1:Nvis1

            Xsv=[Coords(1) - SV_pos(m,2); Coords(2) - SV_pos(m,3); Coords(3) - SV_pos(m,4)];
            Rglo=norm(Xsv);

           H(m,:)=[Xsv(1)/Rglo Xsv(2)/Rglo Xsv(3)/Rglo 1];%

        dR(m)=SV_pos(m,6) + SV_pos(m,5)*c - Rglo - Rcl;%  %проверить ЗНАКи! SV_pos(m,5)!  %проверен ЗНАК -Rcl !!!  
%       
        Rg(m)=dR(m);
        end
        %%%%%%%%%%%%%
        
        if (Nvis1+Nvis_GPS1 >= 4)
            
            inHH = inv(H'*H);
            %%% Calculation of coordinates %%%
             Xr(1:4)=Xr(1:4)+inHH*H'*dR;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        else
            %%% Calculation of coordinates %%%
             Xr(1:4)=Xr(1:4)+H'*inv(H*H')*dR;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        end

    end
    
    else
        %%%% Extended Kalman Filter %%%
       
        [F,Q] = generate_F_Q_matrix(T_temp);
        Xr=F*Xr; %%16
         
        Coords=Xr(1:3);
        Rcl=Xr(4);
        Velcts=Xr(5:7); % вектор скорости НАП
        Vcl=Xr(8); % поправка частоты ОГ НАП в м/с

        D = zeros(2*Nvis1+2*Nvis_GPS1,2*Nvis1+2*Nvis_GPS1);% % 2 порядок

        dVr = zeros(Nvis1+Nvis_GPS1,1);

        for m=1:Nvis1
            
            Xsv=[Coords(1) - SV_pos(m,2); Coords(2) - SV_pos(m,3); Coords(3) - SV_pos(m,4)];
            Rglo=norm(Xsv);
            Rg(m)=Rglo;
            
            D(m,m) = 10.;%(+- 5 � ������ 3 ���); 5.;%%(+- 8 � ������ 3 ���)
             D(Nvis1+Nvis_GPS1+m,Nvis1+Nvis_GPS1+m) = .04;%.04 ;%%           

            H(m,:)=[Xsv(1)/Rglo Xsv(2)/Rglo Xsv(3)/Rglo 1];%
            dR(m)=SV_pos(m,6) + SV_pos(m,5)*c - Rglo - Rcl;%
%             if dR(m)>50.
% %                ttext=['epoch',num2str(i),' Nsv' num2str(m),'  dR=', num2str(dR(m))];
%                disp(ttext )
%                
%             end 
            Rg(m)=dR(m);

%% именно здесь использовано приращение псевдофазы данного НКА: SV_vel(m,6)) 
        dVr(m)=(SV_vel(m,6))/T_temp - (((Velcts(1)-SV_vel(m,2))*Xsv(1)+(Velcts(2)-SV_vel(m,3))*Xsv(2)+(Velcts(3)-SV_vel(m,4))*Xsv(3))/Rglo + Vcl);% 
        Vg(m)=dVr(m);
        end

        H2=[H zeros(Nvis1+Nvis_GPS1,4);
           zeros(Nvis1+Nvis_GPS1,4) H ];
               
        if (Nvis1+Nvis_GPS1 >= 0)
            E=F*E*F' + Q;
            E=inv(inv(E)+H2'*(D\H2));%%
            Krcm=E*(H2'/D);
            dRV=[dR; dVr];
            Xr=Xr + Krcm*dRV;%

        else
            E=inv((sqrt(D)\H2)*H2'*inv(sqrt(D))'); %
            Xr=Xr + H2'*(E/D)*[dR; dVr];     %
        end

    end
end