% 2020 08 06 ����������� �� ������ �������� � ���������
% 2019 11 27 kapa (����������� �� ����� �� ����� ������ ��������)
function [ X, dX, ddX, dT ] = satpos_Glo1(Time, CodeRange, OI, XYZ0)
% ���������
    C = 299792458; % �������� ����� 
	Omegae_dot     = 7.292115e-5;  % Earth rotation rate, [rad/s]

% ��������� ����� - ������� ���������
	Traw = Time - ( CodeRange / C);
	Traw = rem(Traw, 86400) - OI.Tb_sec;

	dt = Traw;
	if (dt < -43200) dt = dt + 86400;	end
	if (dt > 43200) dt = dt - 86400;	end
 	clkcorr = -OI.Tau + OI.Gamma*dt;

 	Tglo = Traw - clkcorr;
    
	Tk = Tglo;
	% ����� �� ��� ��������
	if (Tk < -43200) Tk = Tk + 86400; end
	if (Tk > 43200) Tk = Tk - 86400;  end
% ���������� ���������
	STEP = 30; % ��� �������������� 1 ������
	steps = floor(abs(Tk)/STEP) + 1;
	dt = Tk/steps;
    
	x(1) = OI.X;
	x(2) = OI.Y;
	x(3) = OI.Z;
	x(4) = OI.dX;
	x(5) = OI.dY;
	x(6) = OI.dZ;
	acc(1) = OI.ddX;
	acc(2) = OI.ddY;
	acc(3) = OI.ddZ;
	w = zeros(6);
	
	for j=1:steps
%     while (Tk ~= 0)
%         if (abs(Tk) < STEP)
%             dt = Tk;
%         else
%             dt = STEP * sign(Tk);
%         end
%         Tk = Tk - dt;
% RungeKutta 4
		k1 = deq(x, acc);
		for i=1:6
			w(i) = x(i) + k1(i)*dt/2.0;
		end
	
		k2 = deq(w, acc);
		for i=1:6
			w(i) = x(i) + k2(i)*dt/2.0;
		end
		
		k3 = deq(w, acc);
		for i=1:6
			w(i) = x(i) + k3(i)*dt;
		end

		k4 = deq(w, acc);
		for i=1:6
			x(i) = x(i) + (k1(i) + 2.0*k2(i) + 2.0*k3(i) + k4(i))*dt/6.0;
		end
	end
	
	x = x * 1000; % ������� � ����� � �/�
	
	% ���� �������� �����
% 	omegatau   = Omegae_dot * (NKA.Delay / C); % �.�. � �� ���� dT ���������, �� ��� �����������, ����� ���� ������� �� dT ���������
	% � ������ 1� ����� ������������ ����� �� ����� ��������������� �� �������������� ���������, ���������� ��� ��������
	omegatau = Omegae_dot * norm(x(1:3) - XYZ0) / C; % ����.��������� � ������� � �������
	
	R3 = [ cos(omegatau)    sin(omegatau)   0;
	      -sin(omegatau)    cos(omegatau)   0;
	       0                0               1];

	X_sat_rot = R3 * x(1:3)';
% 	X = X_sat_rot(1);
% 	Y = X_sat_rot(2);
% 	Z = X_sat_rot(3);
    X = X_sat_rot;
    dX = x (4:6)';
    ddX = acc';
% �������, ����� ���������� ����������, �� ����� ����������� �� dT ���������
% 	omegatau   = Omegae_dot * ((NKA.Delay / C)  );
% 	R3 = [ cos(omegatau)    sin(omegatau)   0;
% 	      -sin(omegatau)    cos(omegatau)   0;
% 	       0                0               1];
% 	X_sat_rot = R3 * x(1:3)'
	
% �������� �����
  	dT = clkcorr;% - (2/(C^2))*10^6*(x(1)*x(4)+x(2)*x(5)+x(3)*x(6));
	
end

