 function [sp3_30s_R] = parsR_sp3_30s_v00(indexGNSS,sp3_30)
% ����� � ������ ������ ��� ������ ���� �� ������:
% =< 100 == �������; 
%

% indexGNSS = 100;

sp3_30s_R = [];%NaN;

n_SP3 = length(sp3_30.data(:,2));

for i=1:n_SP3
    
    if sp3_30.data(i,3) < indexGNSS  %100
        sp3_30s_Ri = sp3_30.data(i,:);
        sp3_30s_R = [sp3_30s_R; sp3_30s_Ri];
    end    
    

end

