function [xdot] = deq(x, acc)
% rtklib
	RE_GLO = 6378.1360;   %/* radius of earth (GLONASS-ICD) (m) */
	MU_GLO = 398600.4418; %/* gravitational constant (GLONASS-ICD) (m^3/s^2) */
	J2_GLO = 1082625.75E-9; %/* 2nd zonal harmonic of geop (GLONASS-ICD) */
	OMGE_GLO = 7.292115E-5; %/* earth angular velocity (GLONASS-ICD) (rad/s) */
    
	r2 = x(1)*x(1) + x(2)*x(2) + x(3)*x(3);
	r3 = r2*sqrt(r2);
	omg2=OMGE_GLO*OMGE_GLO;
    
    %/* GLONASS ICD A.3.1.2 with xdot[4],xdot[5] correction */
    a = 1.5*J2_GLO*MU_GLO*RE_GLO*RE_GLO/r2/r3; %/* 3/2*J2*myu*Ae^2/r^5 */
    b = 5.0*x(3)*x(3)/r2;                      %/* 5*z^2/r^2 */
    c = -MU_GLO/r3-a*(1.0-b);                  %/* -myu/r^3-a(1-b) */
    xdot(1) = x(4);
	xdot(2) = x(5);
	xdot(3) = x(6);
    xdot(4) = (c+omg2)*x(1) + 2.0*OMGE_GLO*x(5) + acc(1);
    xdot(5) = (c+omg2)*x(2) - 2.0*OMGE_GLO*x(4) + acc(2);
    xdot(6) = (c-2.0*a)*x(3) + acc(3);
end

