function [Xr,E]=NavPosFK_RCM3_99v03a(Xr,N_mnk,SV_pos,SV_vel,KALMAN_ENABLE,F,Q,E,T_temp, i)
%% !! ������� ������ ��� ������� ���������!!
%%  SV_pos,SV_vel - ��� ���������� � ������ �������� ���
%% SV_pos(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation]
%% ���������: �� ���� - CodeRange, ����� ���� ��������! �� ����������, �����
%             �� ���� - ��������������� �� ������ ����� ����, ���������� ���� �� ����! �� , �����!
%% SV_vel(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF dPhR]; 
 global Rg Vg %ChannelsData ChannelsData_GPS DynData NavSolM3 %NavSolM
% global c Nvis NumVis Nvis_GPS NumVis_GPS T T_temp COUNT
% global num_lost_glon num_lost_gps enable_lost enable_lostG% KALMAN_ENABLE
% global K_4 K_8 dRVA H p10 Krcm K4_3 K8_3 ChannelsSm_GPS ChannelsSm NOsmoos %%k8_v

%%% ��������� ������������������ ��������� �����������, �/�^2 %%%


c = 299792458; % �������� �����
    %---------- MNK implementation --------------------
%     Nvis1=Nvis-enable_lost*num_lost_glon;
    [Nvis1,~] = size(SV_pos);
    Nvis_GPS1=0;%Nvis_GPS-enable_lostG*num_lost_gps;
%     NavSolM3.Nvis_sum=Nvis1 + Nvis_GPS1;
    
    H=zeros(Nvis1+Nvis_GPS1,4);
    dR=zeros(Nvis1+Nvis_GPS1,1);
    Vg= NaN(1,Nvis1);% ��� ��������� ����� ��� ������� ������� �� ���������� ��������
    Rg= NaN(1,Nvis1);% ��� ��������� ����� ��� ������� ������� �� ���������� ���������
%     dVr=zeros(Nvis1+Nvis_GPS1,1);
%     dAr=zeros(Nvis1+Nvis_GPS1,1);
        
    if (KALMAN_ENABLE==1)
        time_count_kalman_on=50;%10;
    elseif (KALMAN_ENABLE==0)
        time_count_kalman_on=10e9;%��!! 10�9 ��� ������ ������!! ���� 10000 ��;
    end
%     if ~mod(COUNT,round(T_temp/T))
%     if ((COUNT/round(T_temp/T)) < time_count_kalman_on)
    if (N_mnk < time_count_kalman_on)    
    %%%%%% MNK implemenation %%%%
    for k=1:5 % �������� ���

%{
        Coords=NavSolM3.Xk(1:3);
        Velcts=NavSolM3.Xk(5:7);
        Axyz=NavSolM3.Xk(9:11);
        Rcl=NavSolM3.Xk(4);
        Vcl=NavSolM3.Xk(8);
        Acl=NavSolM3.Xk(12);
%         Rcl=DynData.X(4);% TEST!!
%         Vcl=DynData.V(4);% TEST!!
%}
        Coords=Xr(1:3);
        Rcl=Xr(4);
        %%%GLONASS%%%
        for m=1:Nvis1
%             n=NumVis(m);
            Xsv=[Coords(1) - SV_pos(m,2); Coords(2) - SV_pos(m,3); Coords(3) - SV_pos(m,4)];
            Rglo=norm(Xsv);
%             Rg(m)=Rglo;
%            H(m,:)=[(Coords-DynData.Ephem(1:3,n))'/norm(Coords-DynData.Ephem(1:3,n)) 1];%%% + -
           H(m,:)=[Xsv(1)/Rglo Xsv(2)/Rglo Xsv(3)/Rglo 1];%

%        dR(m)=ChannelsData(n).Xest_DLL(1)-(norm(Coords-DynData.Ephem(1:3,n))+Rcl);%�������� ���� +Rcl !!!    
        dR(m)=SV_pos(m,6) + SV_pos(m,5)*c - Rglo - Rcl;%  %��������� �����! SV_pos(m,5)!  %�������� ���� -Rcl !!!  
%         dVr(m)=-ChannelsData(n).Xest_PLL(2)*c/ChannelsData(n).Wlit-((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/norm(Coords-DynData.Ephem(1:3,n))+Vcl);%    
        
%         dAr(m)=-ChannelsData(n).Xest_PLL(3)*c/ChannelsData(n).Wlit - (H(m,1:3)*Axyz+(Velcts-DynData.Ephem(4:6,n))'*(Velcts-DynData.Ephem(4:6,n))/norm(Coords-DynData.Ephem(1:3,n))-...
%                                                                         (((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/norm(Coords-DynData.Ephem(1:3,n)))^2)/norm(Coords-DynData.Ephem(1:3,n)));
        Rg(m)=dR(m);
        end
        %%%%%%%%%%%%%
        
        %%%%%GPS%%%%
%{
        for m=1:Nvis_GPS1
            n=NumVis_GPS(m);
            H(Nvis1+m,:)=[(Coords-DynData.Ephem_GPS(1:3,n))'/norm(Coords-DynData.Ephem_GPS(1:3,n)) 1];%%% + -
%             dR(Nvis1+m)=ChannelsData_GPS(n).Xest_DLL(1)-(norm(Coords-DynData.Ephem_GPS(1:3,n))+NavSolM.Xk(4));%%% + -
         dR(Nvis1+m)=ChannelsData_GPS(n).Xest_DLL(1)-(norm(Coords-DynData.Ephem_GPS(1:3,n))+Rcl);%   
%             dVr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(2)*c/ChannelsData_GPS(n).W_L1-((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/norm(Coords-DynData.Ephem_GPS(1:3,n))+NavSolM.Xk(8)); %%% + -
         dVr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(2)*c/ChannelsData_GPS(n).W_L1-((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/norm(Coords-DynData.Ephem_GPS(1:3,n))+Vcl); %   
        %% �������� ������� ������ ������� �� ���������!!
         dAr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(3)*c/ChannelsData_GPS(n).W_L1 + Acl - (H(Nvis1+m,1:3)*Axyz+(Velcts-DynData.Ephem_GPS(4:6,n))'*(Velcts-DynData.Ephem_GPS(4:6,n))/norm(Coords-DynData.Ephem_GPS(1:3,n))-...
                                                                                       (((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/norm(Coords-DynData.Ephem_GPS(1:3,n)))^2)/norm(Coords-DynData.Ephem_GPS(1:3,n)));
        end
        %%%%%%%%%%%%
%}
        
        %%%GDOP calculation%%%
%         NavSolM3.H=H;
%         inHH = inv(H'*H);
%         if (Nvis1+Nvis_GPS1 >= 4)
% %             NavSol.GDOP=sqrt(trace(inv(H'*H)));
%             NavSolM3.GDOP=sqrt(trace(inHH));
%         else
%              NavSolM3.GDOP=sqrt(trace(H'*inv(H*H')*inv(H*H')*H));
%            % ��������� ��������������� ������!!
%         end
        %%%%%%%%%%%%%%%%%%%%%%
        
        if (Nvis1+Nvis_GPS1 >= 4)
            
            inHH = inv(H'*H);
            %%% Calculation of coordinates %%%
%             NavSol.Xk(1:4)=NavSol.Xk(1:4)+inv(H'*H)*H'*dR;
%              NavSolM3.Xk(1:4)=NavSolM3.Xk(1:4)+inHH*H'*dR;
             Xr(1:4)=Xr(1:4)+inHH*H'*dR;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
            %%%%% Calculation velocity %%%%%%
%{
%             NavSol.Xk(5:8)=NavSol.Xk(5:8)+inv(H'*H)*H'*dVr;
             NavSolM3.Xk(5:8)=NavSolM3.Xk(5:8)+inHH*H'*dVr;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%% Calculation acceleration %%%
            NavSolM3.Xk(9:11)=NavSolM3.Xk(9:11)+inv(H(:,1:3)'*H(:,1:3))*H(:,1:3)'*dAr;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}
        else
            %%% Calculation of coordinates %%%
%             NavSolM3.Xk(1:4)=NavSolM3.Xk(1:4)+H'*inv(H*H')*dR;
             Xr(1:4)=Xr(1:4)+H'*inv(H*H')*dR;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
            %%%%% Calculation acceleration %%%
            NavSolM3.Xk(9:11)=NavSolM3.Xk(9:11)+H(:,1:3)'*inv(H(:,1:3)*H(:,1:3)')*dAr;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%% Calculation velocity %%%%%%%
            NavSolM3.Xk(5:8)=NavSolM3.Xk(5:8)+H'*inv(H*H')*dVr;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}
        end

    end
    
    else
        %%%% Extended Kalman Filter %%%
%         [F,G,Q,E]=InitNavPosFK_RCM3_v0(c,T_temp,Dksidyn);
        
         Xr=F*Xr; %%16
%         Coords=NavSolM3.Xk(1:3);
%         Velcts=NavSolM3.Xk(5:7);
%         Axyz=NavSolM3.Xk(9:11);
%         Rcl=NavSolM3.Xk(4);
%         Vcl=NavSolM3.Xk(8);
%         Acl=NavSolM3.Xk(12);
%         Rcl=DynData.X(4);% TEST!!
%         Vcl=DynData.V(4);% TEST!!
        
        Coords=Xr(1:3);
        Rcl=Xr(4);
        Velcts=Xr(5:7); % ������ �������� ���
        Vcl=Xr(8); % �������� ������� �� ��� � �/�
        
%         D=zeros(3*Nvis1+3*Nvis_GPS1,3*Nvis1+3*Nvis_GPS1);% 3 �������
        D=zeros(2*Nvis1+2*Nvis_GPS1,2*Nvis1+2*Nvis_GPS1);% % 2 �������
%         HV(Nvis1+Nvis_GPS1,4)=NaN;
        dVr=zeros(Nvis1+Nvis_GPS1,1);

        for m=1:Nvis1
%             n=NumVis(m);
            
            Xsv=[Coords(1) - SV_pos(m,2); Coords(2) - SV_pos(m,3); Coords(3) - SV_pos(m,4)];
            Rglo=norm(Xsv);
            Rg(m)=Rglo;
            %��������� �� �� ��� � ���
%             NavSolM3.D(m,m)=ChannelsData(n).E_DLL; % �� �������� �� ���
%             NavSolM3.D(Nvis1+Nvis_GPS1+m,Nvis1+Nvis_GPS1+m)=((c/ChannelsData(n).Wlit)^2)*ChannelsData(n).E_PLL(2,2); %*100.; % �� �������� �� ���(���)
%             NavSolM3.D(2*Nvis1+2*Nvis_GPS1+m,2*Nvis1+2*Nvis_GPS1+m)=((c/ChannelsData(n).Wlit)^2)*ChannelsData(n).E_PLL(3,3);% �� ��������� �� ���(���)
            
%             D(m,m)=25.;%% ��� ������ ��� ���??
%%%%%%%%%% ������ �������� ��������� �Ѩ ��� ������� .04 ; ��� dRfi!!% ��� ��� 25. :(+- 5 � ������ 3 ���); ��� 4. :(+- 8 � ������ 3 ���); 
             D(m,m) = 10.;%(+- 5 � ������ 3 ���); 5.;%%(+- 8 � ������ 3 ���)
            %ChannelsData(n).E_DLL; % �� �������� �� ���
%             D(Nvis1+Nvis_GPS1+m,Nvis1+Nvis_GPS1+m)=0.3;%((c/ChannelsData(n).Wlit)^2)*ChannelsData(n).E_PLL(2,2); %*100.; % �� �������� �� ���(���)
%%%%%%%%%% ������ �������� ��������� ��� ���������� ���� �Ѩ ��� �������  ��� D(m,m) = 25.;%%��
%.06 ;%.1;%%0.3;%%%++  %%% .02 ;%����� ��������, �� � �������� %%% vs .01 ;2.5e-5 --!! ���������� �� 
             D(Nvis1+Nvis_GPS1+m,Nvis1+Nvis_GPS1+m) = .04;%.04 ;%%           

%             H(m,:)=[(Coords-DynData.Ephem(1:3,n))'/Rglo 1];
            H(m,:)=[Xsv(1)/Rglo Xsv(2)/Rglo Xsv(3)/Rglo 1];%
            %{
      fx1=(Velcts(1)-DynData.Ephem(4,n))*(Coords(1)-DynData.Ephem(1,n)) + ...
          (Velcts(2)-DynData.Ephem(5,n))*(Coords(2)-DynData.Ephem(2,n)) + ...
          (Velcts(3)-DynData.Ephem(6,n))*(Coords(3)-DynData.Ephem(3,n));
          
          HV(m,1)=(Velcts(1)-DynData.Ephem(4,n))*Rglo - fx1*(Coords(1)-DynData.Ephem(1,n))/Rglo;
          HV(m,2)=(Velcts(2)-DynData.Ephem(5,n))*Rglo - fx1*(Coords(2)-DynData.Ephem(2,n))/Rglo;
          HV(m,3)=(Velcts(3)-DynData.Ephem(6,n))*Rglo - fx1*(Coords(3)-DynData.Ephem(3,n))/Rglo;
          HV(m,:)=[HV(m,1)/(Rglo*Rglo) HV(m,2)/(Rglo*Rglo) HV(m,3)/(Rglo*Rglo) 0];
            %}
            
%             dR(m)=ChannelsData(n).Xest_DLL(1)-(norm(Coords-DynData.Ephem(1:3,n))+NavSolM.Xk(4)); %�������� ���������� ���
            dR(m)=SV_pos(m,6) + SV_pos(m,5)*c - Rglo - Rcl;%
            if dR(m)>50.
               ttext=['epoch',num2str(i),' Nsv' num2str(m),'  dR=', num2str(dR(m))];
               disp(ttext )
               
            end 
            Rg(m)=dR(m);
%{
             if NOsmoos==1
          dR(m)=ChannelsData(n).Xest_DLL(1)-(Rglo+Rcl); %�������� ���������� ��� %%�������� ���� +Rcl !!!
             else
         %%% Smoos var.
          dR(m)=ChannelsSm(n).Rs -(Rglo+Rcl);
             end
%}       
       
       %             dVr(m)=-ChannelsData(n).Xest_PLL(2)*c/ChannelsData(n).Wlit-((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/norm(Coords-DynData.Ephem(1:3,n))+NavSolM.Xk(8));
    % ������ ���� � �/�!! 
    %ChannelsData(k).Wlit=1602.*2*pi*1e6 - ��� ����.�������, ������?
%        dVr(m)=-ChannelsData(n).Xest_PLL(2)*c/ChannelsData(n).Wlit-((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/Rglo + Vcl);
%        dVr(m)=0;%-ChannelsData(n).Xest_PLL(2)*c/ChannelsData(n).Wlit-((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/Rglo + Vcl);
%        dVr(m)=SV_vel(m,6)-T_temp*(Velcts-(SV_vel(m,2:4))')'*(Xsv)/Rglo + Vcl;%+-?
%        dVr(m)=SV_vel(m,6)-T_temp*(Velcts-(SV_vel(m,2:4))')'*(Xsv)/Rglo - Vcl;%% ���-�� ��������, �� ������, ����� ����������:dRfi ��� Vrad=dRfi/T_temp??
%          dVr(m)=(SV_vel(m,6))/T_temp - (Velcts-(SV_vel(m,2:4))')'*(Xsv)/Rglo - Vcl;% ������ ���� �/�!!

%% ������ ����� ������������ ���������� ���������� ������� ���: SV_vel(m,6)) 
         dVr(m)=(SV_vel(m,6))/T_temp - (((Velcts(1)-SV_vel(m,2))*Xsv(1)+(Velcts(2)-SV_vel(m,3))*Xsv(2)+(Velcts(3)-SV_vel(m,4))*Xsv(3))/Rglo + Vcl);% 
        Vg(m)=dVr(m);
%         if i==11496 %% �������!! �� ������ ����� (������� ��������� � �������� ����� 0402Glo_st2.21O !!)
%             dVr(m)=0;
%         end
       %% �������� ������� ������ ������� �� ���������!!
%        dAr(m)=-ChannelsData(n).Xest_PLL(3)*c/ChannelsData(n).Wlit + Acl - (H(m,1:3)*Axyz+(Velcts-DynData.Ephem(4:6,n))'*(Velcts-DynData.Ephem(4:6,n))/Rglo -...
%                              (((Velcts-DynData.Ephem(4:6,n))'*(Coords-DynData.Ephem(1:3,n))/Rglo)^2)/Rglo);
        end
%{        
        for m=1:Nvis_GPS1
            n=NumVis_GPS(m);
            Rgps=norm(Coords-DynData.Ephem_GPS(1:3,n));
            %��������� �� �� ��� � ���
            NavSolM3.D(Nvis1+m,Nvis1+m)=ChannelsData_GPS(n).E_DLL;
            NavSolM3.D(2*Nvis1+Nvis_GPS1+m,2*Nvis1+Nvis_GPS1+m)=((c/ChannelsData_GPS(n).W_L1)^2)*ChannelsData_GPS(n).E_PLL(2,2); %*100.;
            NavSolM3.D(3*Nvis1+2*Nvis_GPS1+m,3*Nvis1+2*Nvis_GPS1+m)=((c/ChannelsData_GPS(n).W_L1)^2)*ChannelsData_GPS(n).E_PLL(3,3);
            
            H(Nvis1+m,:)=[(Coords-DynData.Ephem_GPS(1:3,n))'/Rgps 1];

            %
       fx=(Velcts(1)-DynData.Ephem_GPS(4,n))*(Coords(1)-DynData.Ephem_GPS(1,n)) + ...
          (Velcts(2)-DynData.Ephem_GPS(5,n))*(Coords(2)-DynData.Ephem_GPS(2,n)) + ...
          (Velcts(3)-DynData.Ephem_GPS(6,n))*(Coords(3)-DynData.Ephem_GPS(3,n));
          
          HV(Nvis1+m,1)=(Velcts(1)-DynData.Ephem_GPS(4,n))*Rgps - fx*(Coords(1)-DynData.Ephem_GPS(1,n))/Rgps;
          HV(Nvis1+m,2)=(Velcts(2)-DynData.Ephem_GPS(5,n))*Rgps - fx*(Coords(2)-DynData.Ephem_GPS(2,n))/Rgps;
          HV(Nvis1+m,3)=(Velcts(3)-DynData.Ephem_GPS(6,n))*Rgps - fx*(Coords(3)-DynData.Ephem_GPS(3,n))/Rgps;
          HV(Nvis1+m,:)=[HV(Nvis1+m,1)/(Rgps*Rgps) HV(Nvis1+m,2)/(Rgps*Rgps) HV(Nvis1+m,3)/(Rgps*Rgps) 0];
            %}
            
            % �������� ���������� ����� ������!!
%             dR(Nvis1+m)=ChannelsData_GPS(n).Xest_DLL(1)-(norm(Coords-DynData.Ephem_GPS(1:3,n))+NavSolM.Xk(4));
%{    
             if NOsmoos==1
              dR(Nvis1+m)=ChannelsData_GPS(n).Xest_DLL(1)-(Rgps+Rcl);%�������� ���� +Rcl !!!
             else
       %%% Smoos var.
              dR(Nvis1+m)=ChannelsSm_GPS(n).Rs -(Rgps+Rcl);
             end
%    
%             dVr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(2)*c/ChannelsData_GPS(n).W_L1-((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/norm(Coords-DynData.Ephem_GPS(1:3,n))+NavSolM.Xk(8));
            dVr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(2)*c/ChannelsData_GPS(n).W_L1-((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/Rgps + Vcl);        
           %% �������� ������� ������ ������� �� ���������!!
            dAr(Nvis1+m)=-ChannelsData_GPS(n).Xest_PLL(3)*c/ChannelsData_GPS(n).W_L1 + Acl - (H(Nvis1+m,1:3)*Axyz+(Velcts-DynData.Ephem_GPS(4:6,n))'*(Velcts-DynData.Ephem_GPS(4:6,n))/Rgps -...
                                                                                       (((Velcts-DynData.Ephem_GPS(4:6,n))'*(Coords-DynData.Ephem_GPS(1:3,n))/Rgps)^2)/Rgps);
        end
%}        
        %%� ������ ����������� �� ����������� ��� ������� ��������
%         NavSolM3.H=[H zeros(Nvis1+Nvis_GPS1,4) zeros(Nvis1+Nvis_GPS1,4);%% 
%                     HV H zeros(Nvis1+Nvis_GPS1,4);
%                     zeros(Nvis1+Nvis_GPS1,8) H]; %%!! �������� ����������� �� ����������� � �������� ��� ���������!
        H2=[H zeros(Nvis1+Nvis_GPS1,4);
           zeros(Nvis1+Nvis_GPS1,4) H ];
               
        if (Nvis1+Nvis_GPS1 >= 0)
            E=F*E*F' + Q;
            E=inv(inv(E)+H2'*(D\H2));%%
            Krcm=E*(H2'/D);
            dRV=[dR; dVr];
            Xr=Xr + Krcm*dRV;%
%             NavSolM3.E=NavSolM3.F*NavSolM3.E*NavSolM3.F' + NavSolM3.Q;%%NavSolM.G*NavSolM.Q*NavSolM.G';%% ����� 17,  !!�������� GG' ���� ��������� ��!
% %             NavSol.E=inv(inv(NavSol.E)+NavSol.H'*inv(NavSol.D)*NavSol.H);
%             NavSolM3.E=inv(inv(NavSolM3.E)+NavSolM3.H'*(NavSolM3.D\NavSolM3.H));%% 
% %             NavSol.Xk=NavSol.Xk + NavSol.E*NavSol.H'*inv(NavSol.D)*[dR; dVr; dAr];
%             Krcm=NavSolM3.E*(NavSolM3.H'/NavSolM3.D);
                    %
                    %                    
%             NavSolM.Xk=NavSolM.Xk + NavSolM.E*(NavSolM.H'/NavSolM.D)*[dR; dVr; dAr];% ����� 11
%             NavSolM3.Xk=NavSolM3.Xk + Krcm*dRVA;%
        else
            E=inv((sqrt(D)\H2)*H2'*inv(sqrt(D))'); %
            Xr=Xr + H2'*(E/D)*[dR; dVr];     %
% %             NavSol.E=inv(inv(sqrt(NavSol.D))*NavSol.H*NavSol.H'*inv(sqrt(NavSol.D))'); %!!!!????
%             NavSolM3.E=inv((sqrt(NavSolM3.D)\NavSolM3.H)*NavSolM3.H'*inv(sqrt(NavSolM3.D))'); %
% %             NavSol.Xk=NavSol.Xk + NavSol.H'*NavSol.E*inv(NavSol.D)*[dR; dVr; dAr];     %!!!!????
%             NavSolM3.Xk=NavSolM3.Xk + NavSolM3.H'*(NavSolM3.E/NavSolM3.D)*[dR; dVr; dAr];     %
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
    end
    
%     end
    %----------------------------------------------------------