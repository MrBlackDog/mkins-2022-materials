
clear all
clc;

global Rg Vg
% ������� ������:
%
c = 299792458;
%
%
Xb=[2844675.716  2161053.951  5266369.927];%� !!%%, � WGS-84. �� ����� ������ "0-����"
Vb=[0 0 0]; %% � ���� �����/���

T_temp=.1;%.1;
sigmaA=20.;%20;%8; %������ �������� ������ ������??
%%% ��������� ���������� ��������� � �����. ������ �����������, ����. �^2/�^2. %%%
Dksidyn=0.2*sigmaA^2*0.005;
%
%�������� ������: 

% %%%%%%%%%%%%%%%%%%%%%

%���� ������� ������

Lam1lit0=c/1602e6;%
% %%%%%%%%%%%%%%%%%%%%%
% % � ������� �����!!
% Dat ='';
%   Dat1 ='krsn1108a_all.19o';
%   Dat1 ='210701with.20O';
%   Dat2 ='210701.20O';
% %   Dat3 ='20191109-124435_bis104.O';
% %  Dat ='glo.18O';
%%%%%%%%%%%%%%%%%%%%%
% ����� ��� R
%   PRN_listR = [5, 13, 20, 21];% out 20
%   PRN_listR = [5]; 

% %%%%%%%%%%%%%%%%%%%%%

%������: ��������� �����
% timeStart = [year, month, day, hour, minute, second];
% timeStart = [2020, 07, 21, 11, 52, 24];
%  [gpsWeekStart, towStart, gps_dowStart] = date2gps(timeStart);


deltT=0.1; %���, ������ ������ ������ � �������� rinex-�����
% v_light = 299792458;	     % vacuum speed of light m/s

% Lam1lit0=v_light/1602e6;%R11
% Lam1lit6=v_light/(1602e6+6*562500);%R08
% Lam1lit_7=v_light/(1602e6-7*562500);%R10
% 
% Lam2lit0=v_light/1246e6;
% Lam2lit6=v_light/(1246e6+6*437500);
% Lam2lit_7=v_light/(1246e6-7*437500);

% Lam1_G=v_light/1575.42e6;
% Lam2_G=v_light/1227.60e6;

% test ������ �� �����.
%% ������ ��������

%    [ EPH_DMY, EPH ] = read_eph_glo( '0102whithout.21G' );
    [ EPH_DMY, EPH ] = read_eph_glo( '0402Glo_st2.21G' );
%    [ EPH_DMY, EPH ] = read_eph_glo( '0402_R43_99.21G' );
%   [ EPH_DMY, EPH ] = read_eph_gloTime( '0402_R43_99.21G' );
%  [ EPH_DMY, EPH ] = read_eph_glo( '0502Glo_st2.21G' );
% [ EPH_DMY, EPH ] = read_eph_glo( '0502Glo_1.21G' );
% [ EPH_DMY, EPH ] = read_eph_glo( '2107_1.20G' );

%% ������ ���������
%  N = 35000;% 0402Glo_st2.21O
%25000;%35000;%30000;%10000;% '2307with.20O'
% N = 22700;%0502Glo_st2.21O
 N = 15000;%37400;%26745;%35000;%11497;%11496;%14000;%36000;%
iStart=1;%7000;
% N = 20000;%21100;%30000;%10000;% '2107with.20O'
% iStart=1;
% N = 28000;%21100;%30000;%10000;% '2307st.20O'
% iStart=1;

%   [NGR, NGR_time] = read_rinex_navisG1_0( '0102with.21O', N );
%    [NGR, NGR_time] = read_rinex_navisG1_0t( '0402Glo_st1.21O', N );
%    [NGR, NGR_time] = read_rinex_navisG1_0t( '0402_R43_99_2.21O', N );%%???
   [NGR, NGR_time] = read_rinex_navisG99_0t( '0402_R43_99_2.21O', N );
%    [NGR, NGR_time] = read_rinex_navisG1_0( '0502Glo_st2.21O', N ); %
%   [NGR, NGR_time] = read_rinex_navisG1_0( '0502Glo_st1.21O', N ); %
% [NGR, NGR_time] = read_rinex_navisG1_0( '2107_1.20O', N ); 

%%%%
[ B, L, H ] = XYZ_to_BLH_GOST32453_2013( Xb );
	
NED2ECEF = [	-sind(B)*cosd(L)	-sind(L)	-cosd(B)*cosd(L);
				-sind(B)*sind(L)	cosd(L)		-cosd(B)*sind(L);
				cosd(B)				0			-sind(B)];
ECEF2NED = NED2ECEF';
% rad2deg = 180.0 / pi;
deg2rad = pi / 180.0;

NoSV=4;%5;% ����.���������� ������� ���
% SV_pos = zeros(12, 1+4+1+2); % sv X Y Z dT CodeRange Az Elev
Result = zeros(N, 4);
ResultF = NaN(N, 8);
dPos = NaN(N, 3);
dPos2 = NaN(N, 3);
ErrENU = NaN(N, 3);
dTr = NaN(N,1);
dTrf = NaN(N,1);
dVrf = NaN(N,1);
% XSV= NaN(N,3);
dObs=NaN(N,NoSV);
dPhR=NaN(N,NoSV);
dObs0=NaN(N,24);
dPhR0=NaN(N,24);
PRCorr=NaN(N,NoSV);

% Xr = [0;0;0;0]; % init XYZt, center of Earth
Xr = [0;0;0;131770.606527352;0;0;0;.008];
dF = 0; % ����� ��� �������� ��� �� �������

%%%%�������� ������ ���������� ����������
 iM = 1;
dTime = diff(NGR_time);
intMax = find(abs(dTime-deltT)>deltT*.5)+1;
if isempty(intMax)
%     iM = 1;
    intMax(iM) = NaN;
end    

%     [F,G,Q,E]=InitNavPosFK_RCM3_v0(c,T_temp,Dksidyn);
    [F,G,Q,E]=InitNavPosFK_R99v0(c,T_temp,Dksidyn);
%     [F,G,Q,E]=InitNavPosFK_RCM3_v01delt(c,T_temp,Dksidyn);

N_mnk=1;
 iepCorr=10000;%10000;%7000;% ��� 0402Glo_st2% 2500;%7000;%500;%10000;
% iepCorr=3500;% ��� 0502Glo_st2%
limCor=1000;%500;
iep4end = 0;
PosCorr = [];
RRg = NaN(N,4);
VVg = NaN(N,4);

iepLimX = [];

for i=iStart:N
	if NGR_time(i)==0 
		break
	end
	
	% �����(�������) ������ �����
	t = rem(NGR_time(i), 86400);

	% ������ ��� ������� ��� � ������� ���� ��������� ��������� �� Tb � ������������ ��������� ���
	m = 1;
%      SV_pos = zeros(12, 1+4+1+2);
%      SV_vel = NaN(12, 6);
      SV_pos0 = NaN(24, 8);
      SV_vel0 = NaN(24, 6);
      PhaseRange0 =  NaN(24,1);   
%     PhaseRangePrev = NaN(1,12);
%     PhaseRange= NaN(1,12);
    
   for j=1:24 %12
		sv = NGR{i}(j).sv;
		if (sv == 0)
%             disp(sv)
			break
		end
		
		if (~isempty(EPH{sv}))
			Tbs = [EPH{sv}.Tb_sec]';
			[a,idx] = min(abs(Tbs - t));
			% idx - ����� ����� �������� ������� ���� ������������
			CodeRange = NGR{i}(j).CodeRange;
%             PhaseRange(m) = NGR{i}(j).PhaseRange;
            PhaseRange0(sv,1) = NGR{i}(j).PhaseRange;
% 			[ X, Y, Z, dT ] = satpos_glo(t, CodeRange, EPH{sv}(idx), Xb);% ��������!
            [ Xsv, dX, ddX, dT ] = satpos_Glo1(t, CodeRange, EPH{sv}(idx), Xb);
            
            X=Xsv(1);
            Y=Xsv(2);
            Z=Xsv(3);
			Kecef = [X Y Z] - Xb;
			NED = ECEF2NED * Kecef';
	
			elevation = atan2d( -NED(3), sqrt(NED(1)*NED(1) + NED(2)*NED(2) ) );
			azimuth = atan2d( NED(2), NED(1) );
			if azimuth < 0
				azimuth = azimuth + 360.0;
			end

			if (elevation > 1.0)% ���. 10 �� 1.0 ����.
                
                %%%%%%%%%%%%% tropo corr      
%             [~,el,~] = topocent(Xr(1:3),Rot_X-Xr(1:3));                                                            
%             El(i,1)=el;
            trop = tropo(sin(elevation*deg2rad),0.0,1013.0,293.0,50.0,...
                0.0,0.0,0.0);
            CodeRange=CodeRange-trop; % ���������: �� - �����
            
%                    if i>26736
%                 svN = [num2str(i), '  Np',num2str(m), '  Sv',num2str(sv) ];
%                 disp(svN);
%                    end
                
% 				SV_pos(m, 1:8) = [sv X Y Z dT CodeRange azimuth elevation];
% %                 SV_vel(m, 1:4) = [sv dX(1) dX(2) dX(3) ];% ��������
%                 SV_vel(m, 1:6) = [sv dX(1) dX(2) dX(3) dF dPhR(i,m)];
                SV_pos0(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation];
%                 SV_vel(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF dPhR(i,sv)];
                SV_vel0(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF NaN];
				m = m + 1;
			end
		end
   end
%     k1=find(SV_pos(:,1)==5);
%     XSV(i, 1)=SV_pos(k1,6);
%     k2=find(SV_pos(:,1)==13);
%     XSV(i, 2)=SV_pos(k2,6);
%     k3=find(SV_pos(:,1)==21);
%     XSV(i, 3)=SV_pos(k3,6);
       
%     SV_pos = SV_pos(1:m-1, :);%% ������� ������!
%     SV_vel = SV_vel(1:m-1, :); 
    
    %%%%%%%%%% ������� �� ����� ������� ��� � ����������! ������ ��� ����� 0402Glo_st2.21O   !!
%     if i>26737
%     aSp=[SV_pos(1, :);SV_pos(3, :);SV_pos(2, :)];
%     SV_pos = aSp;
%       aSv=[SV_vel(1, :);SV_vel(3, :);SV_vel(2, :)];
%       SV_vel = aSv;  
%     end    

         %%%%%% ���������! ����� ������ ������� �� ������� �������� ������� ���
     SV_pos = SV_pos0(~isnan(SV_pos0(:,1)),:);%
     SV_vel = SV_vel0(~isnan(SV_vel0(:,1)),:);
%      dObs = dObs0(~isnan(dObs0(:,1)),:);
%      dPhR = dPhR0(~isnan(dPhR0(:,1)),:);

    
        %%%%%%%%%%%%%%% ���������� �� ��� ������� ���  {Iono Corr Krasn }
     if i>iStart
%          if i==11495 %% ������� ����.�������� > .1 ���!!
%                disp(i) 
%          end 
        %{
%          if (length(SV_pos(:, 1))==length(sats_01));%&&(ObsPrev(:,1)==SV_pos(:,1))
         if (length(SV_pos(:, 1))==length(sats_01))&&(length(ObsPrev(:,1))==length(SV_pos(:,1)))
     dObs(i,1:m-1)=(SV_pos(1:m-1,6)-ObsPrev(1:m-1,6))';%���������� �� ��� ������� ���
     dPhR(i,1:m-1)=PhaseRange(1:m-1)-PhaseRangePrev(1:m-1);
         else
             for sv=1:length(sats_01)
               [ n1 ] = find(ObsPrev(:,1)==sats_01(sv) );
               [ n2 ] = find(SV_pos(:,1)==sats_01(sv) );
               if (~isempty(n1))&&(~isempty(n2))
               dObs(i,sv)=SV_pos(n2,6)-ObsPrev(n1,6);
               dPhR(i,sv)=PhaseRange(n2)-PhaseRangePrev(n1);
               end  
             end
         end
%%%%%%% ���� ����. ��������: ���-����
%         if ~mod(epoch,round(1/decIon))
%         iIon=iIon+1;
%     [ Iv(iIon), Filt,K_iono(iIon) ] = iono_dcpV_0(M, Mprev, Filt,T_temp/decIon );
%         end
     else
         sats_01=SV_pos(:, 1);
         dPhR(i,6)= 0;
  %}
         dObs0(i,:) = (SV_pos0(:,6) - ObsPrev0(:,6))';%���������� �� ��� ������� ��� 
         dPhR0(i,:) = PhaseRange0(:) - PhaseRangePrev0(:);
         
%          dObs(i,:) = dObs0(i,~isnan(dObs0(i,:)));%%!!!!!!!!!!!! ����������!!
%          dPhR(i,:) = dPhR0(i,~isnan(dPhR0(i,:)));
        for k=1:numel(SV_pos(:, 1)) %NoSV
          dObs(i,k) =  dObs0(i,SV_pos(k, 1));
          dPhR(i,k) =  dPhR0(i,SV_pos(k, 1));
        end

     end
     
     
%      ObsPrev=SV_pos(1:m-1,:);%% ������� ������!
     ObsPrev0=SV_pos0(:,:);%
     PhaseRangePrev0=PhaseRange0;
     
    
     if (m-1) >= 4%
        R = LSQ_3D(SV_pos);
        Result(i, :) = R;
        dPos(i, :) = Result(i,1:3) - Xb;
        dTr(i) = Result(i,4); %�������!
     else
            step=i;
     end
     
     if (m-1<4)&&(iep4end<1)
        iep4end=i;
    end
     
%      SV_vel(1:m-1, 6) = dPhR(i,1:m-1)';

    if i>2 
     if max(dObs(i,1:NoSV),[],'omitnan')>1000.
%         N_mnk=1;
        Xr(4)=Xr(4)+.002*c;%!!! ��������� ������ ���
%          dPhR(i,1:NoSV)=dPhR(i,1:NoSV)-.002*c;
%         Xr(8)=Xr(8)-.002*c;%!!! ��������� ������ ���������
         %%%%%%%% ������� ��� ��������� ������ �� ����!!
         for kf=1:numel(SV_pos(:, 1)) %NoSV
            dPhR(i,kf)=dPhR(i-1,kf)+(dPhR(i-1,kf)-dPhR(i-2,kf));
         end
     end  
     
     if max(dObs(i,1:NoSV),[],'omitnan')< -1000.
        Xr(4)=Xr(4)-.002*c;%!!! ��������� ������ ���
%          dPhR(i,1:NoSV)=dPhR(i,1:NoSV)+.002*c;
%         Xr(8)=Xr(8)+.002*c;%!!! ��������� ������ ���������
        %%%%%%%% ������� ��� ��������� ������ �� ����!!
         for kf=1:numel(SV_pos(:, 1)) %NoSV
            dPhR(i,kf)=dPhR(i-1,kf)+(dPhR(i-1,kf)-dPhR(i-2,kf));
         end
     end
     
    end%% i>2
     
     if ( i==iepCorr)    
        AA=['Ntest=',num2str(i)];
%          disp(AA)
%         Ntest=i
%         PosCorr=Pos(1:3,iep-1); %% ������-�������!
%         mPos=mean(ResultF(iepCorr-1000:iepCorr-1,:),1);
        mPos=mean(ResultF(iepCorr-limCor:iepCorr-1,:),1);
        PosCorr=mPos(1:3); %% ������-�������!
%         countFK=1;
%         Xr=[mPos;0;0;0;0];
     end 
     
       kk=1;
    for k=1:NoSV
        
      if (~isnan(dPhR(i,k)))
      SV_vel(kk, 6) = dPhR(i,k)';
      
      %%%%%%%%%%% ������ ����.������� � ���������������
         if (~isempty(PosCorr))
          RangeCorr=norm(SV_pos(kk,2:4) - PosCorr) - norm(SV_pos(kk,2:4) - Xb);
         SV_pos(kk,6)=SV_pos(kk,6)-RangeCorr; % ��������� ��
         PRCorr(i,k)=RangeCorr;
         end
      
      kk=kk+1;
      end
            
    end % NoSV      
        
        if  i==intMax(iM)
            %%%% �������� ��������� �������� ��� �������� ���������
             dTmax = dTime(intMax(iM) - 1);
             [Fm,Gm,Qm,Em]=InitNavPosFK_R99v0(c,dTmax,Dksidyn);
             [Xr,E]=NavPosFK_RCM3_99v02(Xr,N_mnk,SV_pos,SV_vel,1,Fm,Qm,E,dTmax, i);
%             [Xr,E]=NavPosFK_RCM3_v0(Xr,N_mnk,SV_pos,1,Fm,Qm,E);%
            if numel(intMax(iM))>iM
                iM = iM + 1;
            end
        else
%              [Xr,E]=NavPosFK_RCM3_v0(Xr,N_mnk,SV_pos,1,F,Q,E);%1-EFK, 0-MNK
             [Xr,E]=NavPosFK_RCM3_99v02(Xr,N_mnk,SV_pos,SV_vel,1,F,Q,E,T_temp, i);
        end
            
        ResultF(i, :) = Xr;
        N_mnk=N_mnk+1;
        dPos2(i, 1:3) = Xr(1:3)' - Xb;
        dPos2(i, 4:6) = Xr(5:7)' - Vb;
        
        dTrf(i) = Xr(4)/c; %�������!
        dVrf(i) = Xr(8)/Lam1lit0; %��!
        ErrENU(i,1:3) = err2enu(dPos2(i, 1:3)',B, L);
        ErrENU(i,4:6) = err2enu(dPos2(i, 4:6)',B, L);
        
        if max(abs(ErrENU(i,1:3))> 10.)&&(isempty(iepLimX))&&(i>iepCorr)
            iepLimX = i;
        end    
        
         RRg(i,:) = Rg(:);
         VVg(i,:) = Vg(:);
end % i ���� ����

%% zx=diff(RRg);

plot(dPos,'DisplayName','dPos')
hold on
plot(dPos2,'DisplayName','dPos2')
grid on

figure(22);
plot(1:N,ErrENU(:,1),'r','LineWidth',1)
hold on
plot(1:N,ErrENU(:,2),'g','LineWidth',1)
hold on
plot(1:N,ErrENU(:,3),'b','LineWidth',1)
grid on
xlabel('t, 0.1 �');
ylabel('������ ���������, �');

figure(23);
plot(iepCorr:N,ErrENU((iepCorr:N),1),'r','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),2),'g','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),3),'b','LineWidth',1)
grid on
xlabel('�����, [�������� 0,1 �]');
ylabel('������ ���������, �');

figure(230);
plot(iepCorr:N,ErrENU((iepCorr:N),1),'r','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),2),'g','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),3),'b','LineWidth',1)
grid on
xlabel('Epochs, [ 0,1 s]');
ylabel('Positioning errors, m');

figure(32);
plot(1:N,ErrENU(:,4),'r','LineWidth',1)
hold on
plot(1:N,ErrENU(:,5),'g','LineWidth',1)
hold on
plot(1:N,ErrENU(:,6),'b','LineWidth',1)
grid on
xlabel('t, 0.1 �');
ylabel('������ ������� ��������, �/�');

figure(33);
plot(iepCorr:N,ErrENU((iepCorr:N),4),'r','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),5),'g','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),6),'b','LineWidth',1)
grid on
xlabel('�����, [�������� 0,1 �]');
ylabel('������ ������� ��������, �/�');

figure(330);
plot(iepCorr:N,ErrENU((iepCorr:N),4),'r','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),5),'g','LineWidth',1)
hold on
plot(iepCorr:N,ErrENU((iepCorr:N),6),'b','LineWidth',1)
grid on
xlabel('Epochs, [ 0,1 s]');
ylabel('Velocity estimation errors, m/s');

h=figure(41);
 plot(1:N,dTrf,'.b','LineWidth',1);
 hold on
plot(1:N,dTr,'.k','LineWidth',1)
grid on
xlabel('t, 0.1 �');
ylabel('������ �������� ���, c');

figure(42);
 plot(iepCorr:N,dTrf(iepCorr:N),'.b','LineWidth',1);
 hold on
plot(iepCorr:N,dTr(iepCorr:N),'.k','LineWidth',1)
grid on
xlabel('�����, [�������� 0,1 �]');
ylabel('������ �������� ���, c');

figure(420);
 plot(iepCorr:N,dTrf(iepCorr:N),'.b','LineWidth',1);
 hold on
plot(iepCorr:N,dTr(iepCorr:N),'.k','LineWidth',1)
grid on
xlabel('Epochs, [ 0,1 s]');
ylabel('Clock error estimation, s');

figure(43);
 plot(1:N,dVrf,'.b','LineWidth',1);
grid on
xlabel('t, 0.1 �');
ylabel('���������� ������� ��, ��');

figure(44);
 plot(iepCorr:N,dVrf(iepCorr:N),'.b','LineWidth',1);
 grid on
xlabel('�����, [�������� 0,1 �]');
ylabel('���������� ������� ��, ��');

figure(440);
 plot(iepCorr:N,dVrf(iepCorr:N),'.b','LineWidth',1);
 grid on
xlabel('Epochs, [ 0,1 s]]');
ylabel('Frequency estimate, Hz');

mErrENU=mean(ErrENU(iepCorr:iep4end-1,:));
sErrENU=std(ErrENU(iepCorr:iep4end-1,:));

m3ErrENU=mean(ErrENU(iep4end:N,:));
s3ErrENU=std(ErrENU(iep4end:N,:));

m3ErrENUlim=mean(ErrENU(iep4end:iepLimX,:));
s3ErrENUlim=std(ErrENU(iep4end:iepLimX,:));

% std1a1=std(diff(dPhR(1:1000,1)),'omitnan');
% std1a2=std(diff(dPhR(iep4end-1000:iep4end-1,1)),'omitnan');
% 
% std2a1=std(diff(dPhR(1:1000,2)),'omitnan');
% std2a2=std(diff(dPhR(iep4end-1000:iep4end-1,2)),'omitnan');
% std2a3=std(diff(dPhR(iep4end+1:iep4end+1000,1)),'omitnan');
% std2a4=std(diff(dPhR(N-1000:N-1,1)),'omitnan');
% 
% std3a1=std(diff(dPhR(1:1000,3)),'omitnan');
% std3a2=std(diff(dPhR(iep4end-1000:iep4end-1,3)),'omitnan');
% std3a3=std(diff(dPhR(iep4end+1:iep4end+1000,2)),'omitnan');
% std3a4=std(diff(dPhR(N-1000:N-1,2)),'omitnan');
% 
% std4a1=std(diff(dPhR(1:1000,4)),'omitnan');
% std4a2=std(diff(dPhR(iep4end-1000:iep4end-1,4)),'omitnan');
% std4a3=std(diff(dPhR(iep4end+1:iep4end+1000,3)),'omitnan');
% std4a4=std(diff(dPhR(N-1000:N-1,3)),'omitnan');

% %
% stdR1a1=std((dObs(1:1000,1)),'omitnan');
% stdR1a2=std((dObs(iep4end-1000:iep4end-1,1)),'omitnan');
% 
% stdR2a1=std((dObs(1:1000,2)),'omitnan');
% stdR2a2=std((dObs(iep4end-1000:iep4end-1,2)),'omitnan');
% stdR2a3=std((dObs(iep4end+1:iep4end+1000,1)),'omitnan');
% stdR2a4=std((dObs(N-1000:N-1,1)),'omitnan');
% 
% stdR3a1=std((dObs(1:1000,3)),'omitnan');
% stdR3a2=std((dObs(iep4end-1000:iep4end-1,3)),'omitnan');
% stdR3a3=std((dObs(iep4end+1:iep4end+1000,2)),'omitnan');
% stdR3a4=std((dObs(N-1000:N-1,2)),'omitnan');
% 
% stdR4a1=std((dObs(1:1000,4)),'omitnan');
% stdR4a2=std((dObs(iep4end-1000:iep4end-1,4)),'omitnan');
% stdR4a3=std((dObs(iep4end+1:iep4end+1000,3)),'omitnan');
% stdR4a4=std((dObs(N-1000:N-1,3)),'omitnan');
% %

%%%