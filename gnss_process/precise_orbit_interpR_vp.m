function [XYZpos, Vsv, dTsv, dotTsv] = precise_orbit_interpR_vp(Ttime, PRN, sp3,plt)
% ��������� VP 2022 04 09 ������ sp3_30s_R!! ������ ���!!
%% !! *note - GPS_TOW is in seconds;  x,y,z are in m; Tbiase in seconds;
% speed of light m/s
% c=299792458;  
% relativity = 0;
sidereal_day = 0.99726956634;
period = sidereal_day;
P0 = 2.0*pi / period;
% T0 = 0;
% i = find(sp3.data(:,3) == PRN);%% ���� � �������� �������
i = find(sp3(:,3) == PRN);%
% OrigTime = sp3.data(i,2);
OrigTime = sp3(i,2); % � ���, GPST ��������, ��� ������ �������� ��� PRN;
% X = sp3.data(i,4);
% Y = sp3.data(i,5);
% Z = sp3.data(i,6);
% Time = sp3.data(i,2);
Time = sp3(i,2);% � ���, GPST ��������
%find the nearest zero point
X = sp3(i,4)/1000.; % � [��]!!!
Y = sp3(i,5)/1000.;
Z = sp3(i,6)/1000.;
%%
 dTsv0 = sp3(i,7);%% � ���!! ��� ������ �������� ��� PRN; mod VP!!
%%

[~, closestI] = min(abs(Ttime- OrigTime));

%% mod VP!!   
    dTsv = dTsv0(closestI);% [sek] ��� ��������� ������ �������� ��� PRN; 
%%
NDAT = 4;
% indices to use in the fit
if closestI < 5
%     k=[1:9];
    k=1:9;
 % mod VP!!   
   dotTsv = (sp3(closestI+1,7) - sp3(closestI,7))/(Time(closestI+1) - Time(closestI));% [sek/sek] ������ �������� ������ �������� ��� ��� PRN;
end
if closestI>length(OrigTime)-4
%     k=[length(OrigTime)-8:length(OrigTime)];
    k=length(OrigTime)-8:length(OrigTime);
  % mod VP!!   
   dotTsv = (sp3(closestI,7) - sp3(closestI-1,7))/(Time(closestI+1) - Time(closestI));% [sek/sek] ������ �������� ������ �������� ��� ��� PRN;  
end
if closestI>=5&&closestI<=length(OrigTime)-4
%     k = [closestI-NDAT:closestI+NDAT];
    k = closestI-NDAT:closestI+NDAT;
  % mod VP!!   
   dotTsv = (sp3(closestI+1,7) - sp3(closestI,7))/(Time(closestI+1) - Time(closestI));% [sek/sek] ������ �������� ������ �������� ��� ��� PRN;  
end

Xi = X(k);
Yi = Y(k);
Zi = Z(k);
Timei = Time(k)/86400;
Nest = 9;
Nmeas = length(k);
A  = zeros(Nmeas, Nest);
A(:,1) = ones(Nmeas,1); 
B  = zeros(1, Nest);
B(1,1) = 1;
gps_rel_time = (Ttime)/86400;
ND = (Nest-1)/2;
for i = 1:ND
  kk = 2+(i-1)*2;
  P =  P0*i;
  A(:,kk) = sin(P*Timei);
  A(:,kk+1) = cos(P*Timei);
  B(1,kk) = sin(P*gps_rel_time);
  B(1,kk+1) = cos(P*gps_rel_time);
end
XCoeffs = A\Xi; newX = A*XCoeffs;
YCoeffs = A\Yi; newY = A*YCoeffs;
ZCoeffs = A\Zi; newZ = A*ZCoeffs;
XYZpos = 1000*[B*XCoeffs B*YCoeffs B*ZCoeffs];% ������� �� [��] � [�]
XYZpos = XYZpos';
%% ������ ������������ ������� �������� ���
if Ttime >= OrigTime(closestI)
   Vx = (XYZpos(1) - X(closestI)*1000.)/(Ttime - OrigTime(closestI));
   Vy = (XYZpos(2) - Y(closestI)*1000.)/(Ttime - OrigTime(closestI));
   Vz = (XYZpos(3) - Z(closestI)*1000.)/(Ttime - OrigTime(closestI));
end

if Ttime < OrigTime(closestI)
   Vx = (-XYZpos(1) + X(closestI)*1000.)/(-Ttime + OrigTime(closestI));
   Vy = (-XYZpos(2) + Y(closestI)*1000.)/(-Ttime + OrigTime(closestI));
   Vz = (-XYZpos(3) + Z(closestI)*1000.)/(-Ttime + OrigTime(closestI));
end
Vsv = [Vx; Vy; Vz];
%% ������ �������: �������� �� ��������� �������������
d3 = 1e6;% ������� �� [��] � [��]! ������ ��� ���������!
if (plt == 1)
  plot(Time(k), d3*(X(k)- newX),'o-', Time(k), d3*(Y(k)- newY),'o-', ... 
  Time(k), d3*(Z(k)- newZ),'o-')
end