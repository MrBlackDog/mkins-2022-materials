% 2020 04 15 kapa % 2020 06 15 kapa
% ������ ���������\��������� ������! ��� ��� ������ ������
% ��������� VP 2022 04 09 ������ sp3_30s_R!! ������ ���!!

function [ Xsv, Vsv, Ttx, dt_sv, df_sv ] = satellite_positionR_sp3_v00(sv, Trx, PD, sp3_30s_R )

	C		= 299792458;
% 	F		= -4.442807633e-10; % c/sqrt(�), ��� �������������� ��������
% 	w_earth	= 7.2921151467e-5; % �������� �������� �����, ���/�

	% 0. ����� ����� �� �� ���
	%Trx 

	% 1. ����� ��������������� ������� �� ��, �.�. � ������� �� ��������� �� ��� � �� ���
	% ������� ���������� � ����� ��� ���� ��
	Ttrvl = PD / C; % ����� ���������������, �

	% 2. ������������ ����� ���������
	Ttx = Trx - Ttrvl;

	%% �������

	% 3. ������������ ����� ��������� �� ��������� �� ���
%	1step  �� ������ sp3_30s_R � �� UTC! ������������ �������� ����� �� ���!
    
    [XsvTtx, VsvTtx, dt_sv, df_sv] = precise_orbit_interpR_vp(Ttx, sv, sp3_30s_R,0);  
%   dt_sv �������� �� ���
%  	df_sv ������ ������ - �������� �� ��� [�/�]
%	2step
	% ������������ ������ ���������
	Ttx = Ttx - dt_sv;
	% ������������ ����� ���������������
	Ttrvl = Ttrvl + dt_sv;%  % ��������� ��������� ��, ��������� ����� �� ��� �������� ���������

	% 4. ������������ ��������� ��� �� ������ ���������
	% � ����������� �� ������
    %% ��!! ����� ������ ���� �������� ��� �� �������� dt_sv !! ��������� ���� ��������!!
	XsvTtx = XsvTtx - VsvTtx*dt_sv;
    %% !! ���������� VsvTtx �� �������� dt_sv ���� ������������

	% 5. ���� �������� ����� �� ����� ���������������
	% ������� ����� ��������� ��������� ��� ������ ��� �������� �����
	[Xsv] = earth_rotation(XsvTtx, Ttrvl);
	[Vsv] = earth_rotation(VsvTtx, Ttrvl);

	% 6. ��������� ������� ���������
% 	X	= pos(1);
% 	Y	= pos(2);
% 	Z	= pos(3);
% 	Vx	= vel(1);
% 	Vy	= vel(2);
% 	Vz	= vel(3);

end