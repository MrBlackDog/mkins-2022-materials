function [ X] = err2enu( err, lat0, lon0 )
    
        phi = lat0*180/pi;
        lam = lon0*180/pi;
     R = [-sin(lam) cos(lam) 0;
         -sin(phi)*cos(lam) -sin(phi)*sin(lam) cos(phi);
         +cos(phi)*cos(lam) +cos(phi)*sin(lam) sin(phi)];
     
    X = R * err;
    
end

