% 2020 03 16 kapa
% ���� �������� ����� �� ����� ��������������� ������� 
% ����
%	in - ������-������� ���������\... �� ������ ���������
%	travel_time - �����, �������
% �����
%	out - ������-������� ���������\... �� ������ �����
function [out] = earth_rotation(in, travel_time)

	we_rot = 7.292115e-5;  % �������� �������� �����, ���/�
 	
	
	omegatau = we_rot * travel_time; % ���� ��������
	
	R = [  cos(omegatau)    sin(omegatau)   0;
	      -sin(omegatau)    cos(omegatau)   0;
	       0                0               1];
	   
	out = R * in;
	
end