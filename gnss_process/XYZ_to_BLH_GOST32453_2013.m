function [ B, L, H ] = XYZ_to_BLH_GOST32453_2013( XYZ )
X = XYZ(1);
Y = XYZ(2);
Z = XYZ(3);

% �������������� ����� �� �����, ��. ������ 5.1.2
a = 6378136.0;			%	������� ������� 
alpha = 1/298.25784;	%	������

e2 = 2*alpha - alpha^2;	%	��������������^2
% N = a / sqrt( 1 - e2*(sin()))

	D = sqrt(X*X + Y*Y);
	if (D == 0)
		B = pi*Z/(2*abs(Z));
		L = 0;
		H = Z*sin(B)-a*sqrt(1-e2*(sin(B))^2);
	else
		La = abs(asin(Y/D));
		
		if ((Y < 0) && (X > 0))
			L = 2*pi - La;
		elseif ((Y < 0) && (X < 0))
			L = pi + La;
		elseif ((Y > 0) && (X < 0))
			L = pi - La;
		elseif ((Y > 0) && (X > 0))
			L = La;
		elseif ((Y == 0) && (X > 0))
			L = 0;
		elseif ((Y == 0) && (X < 0))
			L = pi;
		end
		
		if (Z == 0)
			B = 0;
			H = D - a;
		else
			r = sqrt(X*X + Y*Y + Z*Z);
			c = asin(Z/r);
			p = e2*a/(2*r);

			s1 = 0;
			d = Inf;
			while (d > 5e-10)
				b = c + s1;
				s2 = asin(p*sin(2*b) / sqrt(1-e2*(sin(b))^2));
				d = abs(s2-s1);
				s1 = s2;
			end
			B = b;
			H = D*cos(B) + Z*sin(B) - a*sqrt(1-e2*(sin(B))^2);
		end	
	end

	
	B = B*180/pi;
	L = L*180/pi;
end







