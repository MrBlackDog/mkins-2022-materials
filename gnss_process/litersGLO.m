function [F1,F2, lam1,lam2] = litersGLO(actualLiters)
% ������ ������ � ����� ����� ��� ��� �������
% ����: ������ ����������� ����� ������� �� ������ ���������, �������� ������
% �����: ������� �� ������ ��� � �������

%���������
	c = 299792458;	% �������� ����� 
    
    if isempty (actualLiters)
%         GLO_Liters = [1 -4 5 6 1 -4 5 6 -2 -7 0 -1 -2 -7 0 -1 4 -3 3 2 4 -3 3 2 0 -6];
        GLO_Liters = [1 -4 5 6 1 -4 5 6 -2 -7 0 -1 -2 -7 0 -1 4 -3 3 2 4 -3 3 2];
        
    else
        GLO_Liters = actualLiters;
    end
    
	GLO_F0_L1	= 1602000000; %   ������� 0 ������ �� L1, ��
 	GLO_F0_L2	= 1246000000; %   ������� 0 ������ �� L2, ��
% 	GLO_F0_L3	= 1202025000; %   ������� �� L3, ��
	GLO_dF_L1	= 562500;		%	��� �� ������ �� L1, ��
 	GLO_dF_L2	= 437500;		%	��� �� ������ �� L2, ��
    
    Nglo = length(GLO_Liters);
    F1=NaN(1,Nglo);
    F2=NaN(1,Nglo);
%     F3=NaN(1,Nglo);
    lam1=NaN(1,Nglo);
    lam2=NaN(1,Nglo);

	for nSV=1:Nglo
		Litera = GLO_Liters(nSV);
			F1(nSV) = GLO_F0_L1 + Litera * GLO_dF_L1;
            lam1(nSV) = c/F1(nSV); 
 			F2(nSV) = GLO_F0_L2 + Litera * GLO_dF_L2;
            lam2(nSV) = c/F2(nSV);
% 			F3 = GLO_F0_L3;
	end


end

