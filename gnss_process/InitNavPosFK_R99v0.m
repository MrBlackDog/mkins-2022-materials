function [F,G,Q,E]=InitNavPosFK_R99v0(c,T_temp,Dksidyn)

%%%%% Matrix F %%%%%%%%%
       F=[1 0 0 0 T_temp 0 0 0 ; %% X(1:3)
          0 1 0 0 0 T_temp 0 0 ;
          0 0 1 0 0 0 T_temp 0 ;
          0 0 0 1 0 0 0 T_temp ; %% �����
          0 0 0 0 1 0 0 0 ; %% V(1:3)
          0 0 0 0 0 1 0 0 ;
          0 0 0 0 0 0 1 0 ;
          0 0 0 0 0 0 0 1 ]; %% �����������
%           0 0 0 0 0 0 0 0 1 0 0; %% A(1:3)
%           0 0 0 0 0 0 0 0 0 1 0;
%           0 0 0 0 0 0 0 0 0 0 1];
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Matrix G %%%% Q  ������� �_����� ���� ��������� ��%%

       G=[0 0 0 0;
          0 0 0 0;
          0 0 0 0;
          0 0 0 0;
          1 0 0 0; 
          0 1 0 0; 
          0 0 1 0;
          0 0 0 1];
      
%         NavSolM.Q=[(Dksigen*T_temp/T)^0.5 0 0 0;
%                    0 (Dksidyn*T_temp/T)^0.5 0 0; 
%                    0 0 (Dksidyn*T_temp/T)^0.5 0;
%                    0 0 0 (Dksidyn*T_temp/T)^0.5];
%                  Q=[(Dksidyn*T_temp/T) 0 0 0; %����� ���� Dksigen??
%                    0 (Dksidyn*T_temp/T) 0 0; % ����������! ��� ����� ��������!
%                    0 0 (Dksidyn*T_temp/T) 0;
%                    0 0 0 (Dksidyn*T_temp/T)];
               Q=[(Dksidyn*T_temp) 0 0 0; %����� ���� Dksigen??
                   0 (Dksidyn*T_temp) 0 0; % ����������! ��� ����� ��������!
                   0 0 (Dksidyn*T_temp) 0;
                   0 0 0 (Dksidyn*T_temp)];
        Q=G*Q*G';
        
 %%% �� ���� ��-99!! var.1A ����� ������ ���� � ��������� ������� ������!!
 h0=0.0e-19*c*c; %*.1; %*.01; % ??        
%  h1=7.0e-21*c*c; %*.1; %*.01; % h-1 ��� ���������
 h2=2.6e-21*c*c; %*.1;%  % h-2 ��� ���������
%  % 

        %% ������ 2�: ��� ����� h1, ������ h0 � h-2, �������� ������� Q ��� ��� �����20!
%         NavSolM2.Q(4,4)=.5*h0*T_temp + h2*T_temp*T_temp*T_temp/3;%������ 2
        Q(4,4)=.5*h0*T_temp + 2*pi*pi*h2*T_temp*T_temp*T_temp/3;%������ 2�
        Q(8,8)=(2*pi*pi*h2*T_temp);%0.0;%
%         NavSolM2.Q(4,8)=.5*h2*T_temp*T_temp;%0.00e-05; %������ 2
        Q(4,8)=pi*pi*h2*T_temp*T_temp;%0.00e-05; %������ 2�
        Q(8,4)=Q(4,8);
%}       
%        Q=[0 0 0 0 0 0 0 0 ;
%           0 0 0 0 0 0 0 0 ;
%           0 0 0 0 0 0 0 0 ;
%           0 0 0 q11 0 0 0 q12 ;
%           0 0 0 0 qV 0 0 0;
%           0 0 0 0 0 qV 0 0;
%           0 0 0 0 0 0 qV 0 ;
%           0 0 0 q21 0 0 0 q22];
%           
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Matrix E %%%%%% �_� ���������� ��%% �������� ��������� ��������!!%
       E=[5. 0  0  0    0  0  0  0 ;
          0  5. 0  0    0  0  0  0 ;
          0  0  5. 0    0  0  0  0 ;
          0  0  0  15.0 0  0  0  0 ;
          0  0  0  0    1. 0  0  0 ;
          0  0  0  0    0  1. 0  0 ;
          0  0  0  0    0  0  1. 0 ;
          0  0  0  0    0  0  0  10.0];
%           0 0 0 0 0 0 0 0 20. 0 0;
%           0 0 0 0 0 0 0 0 0 20. 0;
%           0 0 0 0 0 0 0 0 0 0 20.];
%%%%%%%%%%%%%%%%%%%%%%%%