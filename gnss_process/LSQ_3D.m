% 2019 02 14
% ������ ��������� � 3D ������ ��� �������� (���� �������� ����� ��� ������� ������)
function [x] = LSQ_3D(SV_pos)
% ���������
    C = 299792458; % �������� ����� 
    Omegae_dot     = 7.2921151467e-5;

    x = [0;0;0;0]; % init XYZt, center of Earth
    last_x = [0;0;0];

    dT = 0;
    
	[cnt,~] = size(SV_pos);

	H = zeros(cnt, 4);	% glo only
	v = zeros(cnt, 1);
	% ������������ ������ H � v
	for j=1:10	% 10 ��������
		H(:,4) = 1.0;
		for n = 1:cnt
			dx = x(1) - SV_pos(n,2);
			dy = x(2) - SV_pos(n,3);
			dz = x(3) - SV_pos(n,4);

			r = sqrt(dx*dx + dy*dy + dz*dz);
			v(n) = SV_pos(n,6) - r + SV_pos(n,5) * C - x(4);
			r = 1/r;
			H(n,1) = dx*r;
			H(n,2) = dy*r;
			H(n,3) = dz*r;
		end
		last_x = x(1:3);

		P = inv(H'*H);
		x = x + P * H' * v;
	end
	x(4) = x(4) / C;

 	x = x';

		
end

