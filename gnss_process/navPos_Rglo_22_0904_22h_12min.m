%% ������ ��������
% [ EPH_DMY, EPH ] = read_eph_glo( 'BRDC00GOP_R_20211950000_01D_MN.rnx' );% �� �������� � ���� ������!!
% 

%% ������ �������� *.sp3 30s!!
% file_sp3 = 'Sta30s21663.sp3'; % �� 14.07.21
% [sp3_30] = read_sp3_30s( file_sp3 );
% save('e:\VP\UWB\actualFK\sp3_140722','sp3_30')
% %% �������� .mat ����� �� sp3 ������
% load('e:\VP\UWB\actualFK\sp3_140722','sp3_30')
%% ��������� ������ ������� (�� �����)
% indexGNSS = 100; % =< 100 == �������;
% function [sp3_30s_R] = parsR_sp3_30s_v00(indexGNSS,sp3_30)
% save('e:\VP\UWB\actualFK\sp3R_140722','sp3_30s_R')
  load('sp3R_140722','sp3_30s_R')
% ������ � sp3_30s_R - ����� ������ XYZ ��� ��� ������� ����� 30 � � ������ ����� GPST
% % 
%% �������� .mat ����� �� Rinex 2.11 ����������
%   load('GNSS.mat')
%%

% ������� ������:
%
c = 299792458;
%
%%
% ��������� gnss_struct,TOW1,EPH,EPH_DMY, 
% time_block ���������� � ���������
%%
Xb=[ 2846079.9100  2200502.1000  5249252.6600];%� !!%%, � WGS-84. �� ����� 
Vb=[0 0 0]; %% � ���� �����/���

T_temp=.2;%.1;
sigmaA=20.;%20;%8; %������ �������� ������ ������??
%%% ��������� ���������� ��������� � �����. ������ �����������, ����. �^2/�^2. %%%
Dksidyn=0.2*sigmaA^2*0.005;
%
%�������� ������: 

% %%%%%%%%%%%%%%%%%%%%%

%���� ������� ������

Lam1lit0=c/1602e6;%
deltT=0.2; %���, ������ ������ ������ � �������� rinex-�����
%% ������ ���������
% N = length(gnns_struct);
N = 1000;
iStart=1;%7000

[ B, L, H ] = XYZ_to_BLH_GOST32453_2013( Xb );
	
NED2ECEF = [	-sind(B)*cosd(L)	-sind(L)	-cosd(B)*cosd(L);
				-sind(B)*sind(L)	cosd(L)		-cosd(B)*sind(L);
				cosd(B)				0			-sind(B)];
ECEF2NED = NED2ECEF';

%%
NoSV=4;%5;% ����.���������� ������� ���
% SV_pos = zeros(12, 1+4+1+2); % sv X Y Z dT CodeRange Az Elev
Result = zeros(N, 4);
ResultF = NaN(N, 8);
dPos = NaN(N, 3);
dPos2 = NaN(N, 3);
ErrENU = NaN(N, 3);
dTr = NaN(N,1);
dTrf = NaN(N,1);
dVrf = NaN(N,1);
% XSV= NaN(N,3);
dObs=NaN(N,NoSV);
dPhR=NaN(N,NoSV);
dObs0=NaN(N,24);
dPhR0=NaN(N,24);
PRCorr=NaN(N,NoSV);

Xr = [0;0;0;0;0;0;0;.008];% ��� ����������� �������
dF = 0; % ����� ��� �������� ��� �� �������

%%
% NGR_time = time;%TOW1 �������� �� time ���� ��������� � ��� ����.
NGR_time = TOW1+(3*3600);% % (3*3600) - ������� ����� UTC � GPST
%% !!! ��� �� ������ �����!!

%% � ����� obs �� ������ ������ �� UTC ��� ����� GPST
% ��� ����!! ��������� ����� �����������
%% ��������   GPST - UTC = LeapSeconds = 18
%% �������:  (UTC(SU) ��� ���)=(UTC + 3����)= GPST -LeapSeconds +3����
%  UTC(SU) ��� ��� ��� ����� �������� �������,�� �� sp3! 
% sp3 - ������ ���� � UTC
%% � �������� obs �� 1-� ������: 21  7 14 11 32 12.0000000
[GPS_wk, TOW_1obsG] = GPSweek(2021,7,14,11,32,12.0);% 300732[s] - ���� obs �� ������ � GPST
%% ����� TOW1 - ��� �� obs � GPST
LeapSeconds = 18;
%TOW_1obsG = TOW_1obsG + LeapSeconds;% 300750[s] - ���� obs �� ������ � UTC

%% ����� ��� �������� ������� ���,�� �� sp3!
%% NGR_time = TOW1+(3*3600)-LeapSeconds; %!!!!!!!���� obs �� ������ � GPST!!!!!

%% ����� ��� sp3!
%% Tsp3 = TOW1(i) -LeapSeconds;%!!!!!!!���� obs �� ������ � GPST , � sp3 � UTC!!!!! 
%% Tsp3 = TOW1(i);%!!!!!!!���� obs �� ������ � GPST +  sp3 � GPST!!!!!
%%%%�������� ������ ���������� ����������
iM = 1;
dTime = diff(NGR_time);
intMax = find(abs(dTime-deltT)>deltT*0.5)+1;
if isempty(intMax)
%     iM = 1;
    intMax(iM) = NaN;
end    

%     [F,G,Q,E]=InitNavPosFK_RCM3_v0(c,T_temp,Dksidyn);
%     [F,G,Q,E]=InitNavPosFK_R99v0(c,T_temp,Dksidyn); %% !! ����� ����������
%     [F,G,Q,E]=InitNavPosFK_RCM3_v01delt(c,T_temp,Dksidyn);

N_mnk=1;
iepCorr=10000;% ������ ������ ���������

limCor=1000;%500;
iep4end = 0;
PosCorr = [];
RRg = NaN(N,4);
VVg = NaN(N,4);

iepLimX = [];
%%
for i=iStart:N
	if NGR_time(i)==0 
		break
	end
	
	% �����(�������) ������ �����
	t = rem(NGR_time(i), 86400);

	m = 1;
%      SV_pos = zeros(12, 1+4+1+2);
%      SV_vel = NaN(12, 6);
    SV_pos0 = NaN(24, 8);
    SV_vel0 = NaN(24, 6);
    PhaseRange0 =  NaN(24,1);
%     PhaseRangePrev = NaN(1,12);
%     PhaseRange= NaN(1,12);
    
%    for j=1:24 %12
    for j=1:length(gnss_struct(i).GLO_obs)
		sv = str2double(gnss_struct(i).GLO_obs(j).sv(2:3));
		if (sv == 0)
%             disp(sv)
			break
		end
		
% 		if (~isempty(EPH{sv}))
        answer=find(sp3_30s_R(:,3)==sv);% �������� ������� ��������� ��� � ������� sp3_30s_R
          if (~isempty(answer))   % ��������� ��� sp3_30s_R!! 
             
         %%          ��� ������ ����� ��������� ������� BRDC 
         % ������ ��� ������� ��� � ������� ���� ��������� ��������� �� Tb � ������������ ��������� ���
% 			Tbs = [EPH{sv}.Tb_sec]';
% 			[a,idx] = min(abs(Tbs - t));
% 			idx - ����� ����� �������� ������� ���� ������������
% 			
        %%
            CodeRange = gnss_struct(i).GLO_obs(j).CodeRange_L1;
            PhaseRange0(sv,1) = gnss_struct(i).GLO_obs(j).Phase_L1; %!!! ���� � ������ ������� �� ���������
%% �������� ������ ������� � �����
            
%%
% 			[ X, Y, Z, dT ] = satpos_glo(t, CodeRange, EPH{sv}(idx), Xb);% ��������!
%%          ��� ������ ����� ��������� ������� BRDC
%             [ Xsv_B, dX, ddX, dT_B ] = satpos_Glo1(t, CodeRange, EPH{sv}(idx), Xb); % ���������� ��������� EPH{sv}(idx) 
            % �� ��� �������� �� 07_04 19:28 �� ���
%%          ��� ������ ����� ���������� ��� � ������� sp3_30s_R

%% �������� ��������� ��� �� ������ �������!! ������� ���������
            Tsp3 = TOW1(i);%! - LeapSeconds;%!!!!!!!���� obs �� ������ � sp3 � GPST!!
%             dTsp3 = min(abs(Tsp3 - t));
%             [Xsv] = precise_orbit_interpR(Tsp3, sv, sp3_30s_R,0);
            %% TEST!!
%             % ������������ ����� ��������� �����! ��� ����� �������� �����!
%             Ttx = Tsp3 - CodeRange/c;
%             [Xsv, Vsv, dTsv, dotTsv] = precise_orbit_interpR_vp(Ttx, sv, sp3_30s_R,0);
%             %% end TEST
            
            %% ��������� �������� dTsv � ���� �������� �����! ���� ��� ����� ���������!!
            [ Xsv, Vsv, Ttx, dTsv, dotTsv] = satellite_positionR_sp3_v00(sv, Tsp3, CodeRange, sp3_30s_R );
            
            X=Xsv(1);% [�]
            Y=Xsv(2);
            Z=Xsv(3);
            %mod ��� ���������� �� ������� ��
            dT=dTsv;% [c] 
            dX(1)=Vsv(1); % [�/c]
            dX(2)=Vsv(2);
            dX(3)=Vsv(3); 
            dF=dotTsv;% %[c/c])???? ��������� � �� ??
            
			Kecef = [X Y Z] - Xb;
			NED = ECEF2NED * Kecef';

			elevation = atan2d( -NED(3), sqrt(NED(1)*NED(1) + NED(2)*NED(2) ) );
			azimuth = atan2d( NED(2), NED(1) );
			if azimuth < 0
				azimuth = azimuth + 360.0;
			end

			if (elevation > 1.0)% ���. 10 �� 1.0 ����.
                
                %%%%%%%%%%%%% tropo corr      
%             [~,el,~] = topocent(Xr(1:3),Rot_X-Xr(1:3));                                                            
%             El(i,1)=el;
%                 trop = tropo(sin(elevation*deg2rad),0.0,1013.0,293.0,50.0,...
%                     0.0,0.0,0.0);
%                 CodeRange=CodeRange-trop; % ���������: �� - �����
            

                SV_pos0(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation];
%
                SV_vel0(sv, 1:6) = [sv dX(1) dX(2) dX(3) dF NaN];
				m = m + 1;
			end
		end
   end


         %%%%%% ���������! ����� ������ ������� �� ������� �������� ������� ���
     SV_pos = SV_pos0(~isnan(SV_pos0(:,1)),:);%
     SV_vel = SV_vel0(~isnan(SV_vel0(:,1)),:);


    
        %%%%%%%%%%%%%%% ���������� �� ��� ������� ���  {Iono Corr Krasn }
     if i>iStart

         dObs0(i,:) = (SV_pos0(:,6) - ObsPrev0(:,6))';%���������� �� ��� ������� ��� 
         dPhR0(i,:) = PhaseRange0(:) - PhaseRangePrev0(:);

%         for k=1:numel(SV_pos(:, 1)) %NoSV !!�������������� � ������ ���������� ������� ��� � ������
%           dObs(i,k) =  dObs0(i,SV_pos(k, 1));
%           dPhR(i,k) =  dPhR0(i,SV_pos(k, 1));
%         end

     end
%end

     ObsPrev0=SV_pos0(:,:);%
     PhaseRangePrev0=PhaseRange0;
     
    
     if (m-1) >= 4%
        R = LSQ_3D(SV_pos);
        Result(i, :) = R;
        dPos(i, :) = Result(i,1:3) - Xb;
        dTr(i) = Result(i,4); %�������!
     else
            step=i;
     end
     
     if (m-1<4)&&(iep4end<1)
        iep4end=i;
     end
    
end % i ���� ����

%% !! ��� = LSQ_3D ���-�� �������� �� 701 ���� ��� 300872 � ��� (2021,7,14,11,34,32.0)
% �����������,������ ��� ������� �� �������, ��� 3 ��� (701-708) � (762-768)

%% !! �� ������ 701 ������ ������ ��� R12 ������� � ������������ ������ ��� R4 � gnss_struct(701).GLO_obs 
% �����������,������ ��� ������� �� �������, ��� 3 ��� (701-708) � (762-768)

%% PS � precise_orbit_interpR_vp (line 74 - 76) ���� A\Xi: Warning: Matrix is close to singular or badly scaled
% ������� � ���� �� �����? ������ �� �������� ����� � ����������� ��������!