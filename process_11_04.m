[TOW_ALL,TOW_UWB] = create_all_time_vect(TOW1,uwb_t_new);
%%
c = 299792458;
% load все, что нужно
%%

    prev_obs = struct('ObsPrev0',[],'PhaseRangePrev0',[]);
    X_est_FK_GNSS = struct('Xr',[],'E',[]);
%%
% Xb=[ 2846079.9100  2200502.1000  5249252.6600];%м !!%%, м WGS-84. из файла 
X0 = lla2ecef(rtk(2:4,571)'); % только для начала ГНСС с 1187 такта TOW1
X0_prev = lla2ecef(rtk(2:4,570)');
Vb=(X0' - X0_prev')/0.2; %% в ГЦСК метры/сек
Xini = lla2ecef(rtk(2:4,595)');
Vini = (Xini - lla2ecef(rtk(2:4,594)'))/0.2;
% X_est_FK_GNSS.Xr = [Xini';0;Vini';.008];% Начальное приближение ФК ГНСС
%%
T_temp=.2;
% X_est_GNSS_UWB = nan(9,1700);   
Dx_est_prev_3d = diag([0.1;0.1;0.1;15;2;2;2;10;0.1*c]);
%% коррекции для дифф режима

%%
ind_gnss = [];
ind_uwb = [];
iStart = 1190;
X_est_FK = [];
X_est_FK_lla = [];
[gnss_rez] = create_gnss_rez(N);
X_est_FK_GNSS.Xr = [Xini';0;Vini';.008];% Начальное приближение ФК ГНСС
for i = 1150:1700 % тут такты по TOW_ALL
    [~,ind_gnss] = find([gnss_struct.TOW] == TOW_ALL(i));
    [~,ind_uwb] = find(TOW_UWB == TOW_ALL(i));
    if(~isempty(ind_gnss))
%         process_gnss?
        [gnss_rez,prev_obs,X_est_FK_GNSS] = process_gnss(i,gnss_struct,gnss_rez,sp3_30,prev_obs,iStart,X_est_FK_GNSS,T_temp,X0,rtk,EPH);
%         [gnss_rez] = process_gnss(i,gnss_struct,gnss_rez,sp3_30s_R);
        X_est_GNSS_UWB([1;3;5;7],i) = gnss_rez.Result(i,:)';
        X_est_GNSS_UWB([2;4;6;8],i) = [0;0;0;0];
        X_est_FK(:,i) =  X_est_FK_GNSS.Xr;
    elseif(~isempty(ind_uwb))
%         [X_est,Dx_est,nev,nev_T,Delta_k2] = EKF_UWB_3D_v3(X_est_prev,Dx_est_prev,Y,Y_prev,radius,SatPos);
%         process_uwb?
    end
end

X_est_FK_lla = ecef2lla(X_est_FK(1:3,:)');
Result_lla = ecef2lla([gnss_rez.Result(:,1:3)]);