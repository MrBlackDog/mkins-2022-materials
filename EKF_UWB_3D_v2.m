function [X_est,Dx_est,nev,nev_T,Delta_k2] = EKF_UWB_3D_v2(X_est_prev,Dx_est_prev,Y,Y_prev,radius,SatPos)

%% предобработка, ОЦЕНИВАЕМ Delta,k на текущий момент экстраполяции измерений
    c = 299792458;
% %     disp('в первой подпрограмме')
% %     disp(X_est_prev(end))
% %     disp(c)
% %     disp(X_est_prev(end)/c)
% %   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%     T_nom = 0.0978623589687248;% номинальный интервал между излучениями, сек
%     T_min = 0.088; % минимальный интервал между излучениями, сек 
    T_min = 0.088799050026921;
%     delta_aloxa = 1e-3; % дискрет по времени в алохе, сек
    toa_diff = Y-Y_prev-T_min; % секунды, Y-сек
    non_nan_ind = ~isnan(toa_diff); 
%     m_est_raw = toa_diff(non_nan_ind)/delta_aloxa; % с плавающей точкой
%     m_est = round(m_est_raw(1)); % целое число от 0 до 20
%     % !!!!!!!!!!!!!!!!!!!!!!!
%     % засовываем в дельту разницу расстояний
%     Delta_k = T_min+m_est_raw(1)*delta_aloxa;% сек
%%  
    
    for i = 1:length(non_nan_ind)
        R(i) = sqrt( (X_est_prev(1)-SatPos(1,i))^2 + (X_est_prev(3)-SatPos(2,i))^2 + (X_est_prev(5)-SatPos(3,i))^2 );
        R_dot(i) = ((X_est_prev(1)-SatPos(1,i))*X_est_prev(2)+(X_est_prev(3)-SatPos(2,i))*X_est_prev(4)+(X_est_prev(5)-SatPos(3,i))*X_est_prev(6))/R(i);
%         R_dot(i) = (X_est_prev(1)*X_est_prev(2)+X_est_prev(3)*X_est_prev(4))/R(i);
        Delta_k2(i) = (toa_diff(i)+T_min) * (1+R_dot(i)/c)^-1;
    end
    
    Delta_k2 = min(Delta_k2);
%     disp('3d');
%     disp([Delta_k2])
%     disp('m')
%     disp(Delta_k)
[X_est,Dx_est,nev,nev_T(1)] = KF_step(X_est_prev,Dx_est_prev,Y,SatPos,radius,Delta_k2,non_nan_ind);
%% Cнова решаемся если
% k=1;
% while abs(nev_T(k))>1e-9
%     Delta_k2 = Delta_k2 + nev_T(k);
%     k = k + 1;
%     [X_est,Dx_est,nev,nev_T(k)] = KF_step(X_est_prev,Dx_est_prev,Y,SatPos,Delta_k2,non_nan_ind);
% end
end
%% функция шага ФК
function [X_est,Dx_est,nev,nev_T] = KF_step(X_est_prev,Dx_est_prev,Y,SatPos,radius,Delta_k,non_nan_ind)
        %% матрички
    %   X_est = [x_k; Vx_k;y_k; Vy_k;T_tr_k];
    c = 299792458;
%     disp('во второй подпрограмме')
%     disp(X_est_prev(end))
%     disp(X_est_prev(end)/c)
    F = [1 Delta_k 0 0       0 0       0;
         0 1       0 0       0 0       0;
         0 0       1 Delta_k 0 0       0;
         0 0       0 1       0 0       0;
         0 0       0 0       1 Delta_k 0;
         0 0       0 0       0 1       0;
         0 0       0 0       0 0       1];
    G = [0       0       0;
         Delta_k 0       0;
         0       0       0;
         0       Delta_k 0;
         0       0       0;
         0       0       Delta_k;
         0       0       0;];
    
%     sigma_ksi_vx = 0.1; % CКО движения в м/c^2
%     sigma_ksi_vy = 0.1; % CКО движения в м/c^2
%     sigma_ksi_vz = 0.001; % CКО движения в м/c^2
    sigma_ksi_vx = 1; % CКО движения в м/c^2
    sigma_ksi_vy = 1; % CКО движения в м/c^2
    sigma_ksi_vz = 0.001; % CКО движения в м/c^2
    D_ksi = diag([sigma_ksi_vx^2 sigma_ksi_vy^2 sigma_ksi_vz^2]);
    
    sigma_n = 10e-2; % СКО измерений, метры ХОРОШО
    sigma_n_raduis = 1e-6; % СКО измерения высоты, метры
%     sigma_n = 10; % СКО измерений, метры
    D_n = diag([sigma_n^2*ones(1,size(SatPos(:,non_nan_ind),2)) sigma_n_raduis^2]);
    
    U = [0;0;0;0;0;0;1]; % вектор управления
    B = [0 0 0 0 0 0 0;
         0 0 0 0 0 0 0;
         0 0 0 0 0 0 0;
         0 0 0 0 0 0 0;
         0 0 0 0 0 0 0;
         0 0 0 0 0 0 0;
         0 0 0 0 0 0 Delta_k*c];% матрица управления
    %% перевод наблюдений в метры
    non_nan_ind = [non_nan_ind;true];
    y = [Y*c;radius]; %метры
    
    %% экстраполяция
    X_ext = F*X_est_prev + B*U; % X_est_prev - метры
    D_ext = F*Dx_est_prev*F' + G*D_ksi*G';
    %% оценка
    dS1 = dS_matrix(X_ext,SatPos(:,non_nan_ind(1:4)),radius); % градиентная матрица
    S =  S_matrix(X_ext,SatPos(:,non_nan_ind(1:4)),radius); % невязка (экстраполяция наблюдений)
    K = D_ext * dS1' * inv(dS1 * D_ext * dS1' + D_n);
    Dx_est = D_ext - K * dS1 * D_ext;
    X_est = X_ext + K * (y(non_nan_ind) - S);
    nev = norm(y(non_nan_ind) - S);% метры
    nev_T = (X_est(7) - X_ext(7))/c; % секунды
end
%%
function [S] = S_matrix(x,SatPos,raduis)
   c = 299792458;
%     SatPos размер 3 на N
        S = zeros(size(SatPos,2)+1,1); % + 1 - наблюдение высоты
    for i = 1:size(SatPos,2)% не нан
        R(i) = sqrt( (x(1)-SatPos(1,i))^2 + (x(3)-SatPos(2,i))^2 + (x(5)-SatPos(3,i))^2);
        S(i,1) = x(7)+R(i); %Экстраполяция измерение, в метрах (S-время приема)
    end

    S(i+1,1) = raduis; 
    %S(i+1,1) = sqrt( x(1)^2 + x(3)^2 + x(5)^2);
    
end

%%
function [dS] = dS_matrix(x,SatPos,radius)
    c = 299792458;
%     SatPos размер 3 на N (x,y,z)
    dS = zeros(size(SatPos,2)+1,7);
    for i = 1:size(SatPos,2)% не нан
        R = sqrt( (x(1)-SatPos(1,i))^2 + (x(3)-SatPos(2,i))^2  + (x(5)-SatPos(3,i))^2 );
        dS(i,1) = (x(1)-SatPos(1,i))/R; %dYdx
        dS(i,2) = 0;                    %dYdVx
        dS(i,3) = (x(3)-SatPos(2,i))/R; %dYdy
        dS(i,4) = 0;                    %dYdVy
        dS(i,5) = (x(5)-SatPos(3,i))/R; %dYdz
        dS(i,6) = 0;                    %dYdVz
        dS(i,7) = 1;                    %dYdTrt
    end
    
        dS(i+1,1) = x(1)/radius;          %dradiusdx
        dS(i+1,2) = 0;                    %dradiusdVx
        dS(i+1,3) = x(3)/radius;          %dradiusdy
        dS(i+1,4) = 0;                    %dradiusVy
        dS(i+1,5) = x(5)/radius;          %dradiusdz
        dS(i+1,6) = 0;                    %dradiusdVz
        dS(i+1,7) = 0;                    %dradiusdTrt
end