function [t_new] = pereshet_uwb_to_gps(t_uwb_cur,t0_uwb,t0_gps,drift)

% delta = t0_gps - t0_uwb;%сек
drift = 1;
delta = t0_gps;
t_new = nan(1,length(t_uwb_cur));
for i = 1:length(t_uwb_cur)
    if(t_uwb_cur(i)~=0)
        t_new(i) = delta + drift*(t_uwb_cur(i) - t0_uwb);
    end
%     t_new(i) = delta + drift*(t_uwb_cur(i));
end
end