%%
% T_nom = 0.098;
 T_nom = 0.0978623589687248;
 T_min = 0.088;
delta_aloxa = 1e-3;
[~,m_ind_prev] = min(uwb_toa(:,172));
for i = 173:256
    [~,m_ind] = min(uwb_toa(:,i));
%     toa_diff = (uwb_toa(m_ind,i)-uwb_toa(m_ind_prev,i-1))-T_nom;
%     toa_diff = (uwb_toa(1,i)-uwb_toa(1,i-1))-T_nom;

    toa_diff(i) = (uwb_toa(1,i)-uwb_toa(1,i-1))-T_min;
    m_est_raw(i) = toa_diff(i)/delta_aloxa;
    m_est(i) = round(m_est_raw(i));
end
%%
% prir_minus_m = nan(4,256);
% prir_minus_m2 = nan(4,256);
for i = 173:256
    prir(:,i) = uwb_toa(:,i) - uwb_toa(:,i-1);
    prir_minus_m(:,i) = uwb_toa(:,i) - uwb_toa(:,i-1) - m_est(1,i)*delta_aloxa-T_nom;% это производная от дальности R_dot
    prir_minus_m2(:,i) = uwb_toa(:,i) - uwb_toa(:,i-1) - m_est(1,i)*delta_aloxa-T_min;
    R_est_test(:,i) = (R_est(:,i)-R_est(:,i-1));
end
%%
% cords1(4,3)/c == cords_new(4,192) по тактам, НЕ ПО ЗНАЧЕНИЮ
X_est_prev = [cords1(2,3);0.5;cords1(3,3);0.5;cords1(4,3)];% cords1 - оценка по МНК в ШВ мастера
X_est2(:,192) = X_est_prev;
Dx_est_prev = diag(ones(1,5));
for i = 193:206
    if(i==216)
        disp(i)
    end
%     [X_est(:,i),Dx_est,nev(:,i)] = EKF_UWB(X_est_prev,Dx_est_prev,uwb_toa(:,i),uwb_toa(:,i-1),SatPos);
    [X_est2(:,i),Dx_est,nev(:,i)] = EKF_UWB_v2(X_est_prev,Dx_est_prev,uwb_toa(:,i),uwb_toa(:,i-1),SatPos);
    Dx_est_prev = Dx_est;
    X_est_prev = X_est2(:,i);
end