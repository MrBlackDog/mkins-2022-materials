% [TOW_ALL,TOW_UWB] = create_all_time_vect(GPS_sec_wk_22',uwb_t_new);%
[TOW_ALL,TOW_UWB] = create_all_time_vect([gnss_struct.TOW]',uwb_t_new(:,193:end));
%%
% Xb=[ 2846079.9100  2200502.1000  5249252.6600];%м !!%%, м WGS-84. из файла 
X0 = lla2ecef(rtk(2:4,571)'); % только для начала ГНСС с 1187 такта TOW1
X0_prev = lla2ecef(rtk(2:4,570)');
Vb=(X0' - X0_prev')/0.2; %% в ГЦСК метры/сек
Xini = lla2ecef(rtk(2:4,595)');
Vini = (Xini - lla2ecef(rtk(2:4,594)'))/0.2;
% X_est_FK_GNSS.Xr = [Xini';0;Vini';.008];% Начальное приближение ФК ГНСС
%%
T_temp=.2;
% X_est_GNSS_UWB = nan(9,1700);   
Dx_est_prev_3d_ini = diag([0.1;0.1;0.1;15;2;2;2;10;1e-5*c]);
%% коррекции для дифф режима

%%
uwb_ind_sync = 192; %индекс первого решения МНК по СШП
ind_end = 3000;
ind_gnss = [];
ind_uwb = [];
ind_start = 900;
iStart = ind_start+100;
X_est_FK = [];
X_est_FK_lla = [];
[gnss_rez] = create_gnss_rez(ind_end);
X_est_FK_GNSS.Xr = [Xini';0;Vini';.008];% Начальное приближение ФК ГНСС
% X_est_GNSS_UWB(:,1100) = [Xini';0;Vini';.008;0];
X_est_GNSS_UWB = zeros(9,ind_end);
X_est_UWB = [];
X_est_FK_UWB_lla = [];
ind_uwb_last = 0;
ind_uwb_prev = [];
ind_uwb_start = 1;
ind_uwb = [];
sol_type = [];
% F = [];%???

for i = ind_start:ind_end % тут такты по TOW_ALL
    [~,ind_gnss] = find([gnss_struct.TOW] == TOW_ALL(i));
%     [~,ind_gnss] = find([GPS_sec_wk_22] == TOW_ALL(i));
    [~,ind_uwb] = find(TOW_UWB == TOW_ALL(i));

    if(~isempty(ind_gnss))
%         process_gnss?
        disp('GNSS FILTER')
%         T = TOW_ALL(i)-TOW_ALL(i-1)
        T = 0.2;
%         [gnss_rez,prev_obs,X_est_FK_GNSS] = process_gnss(i,gnss_struct,gnss_rez,sp3_30,prev_obs,iStart,X_est_FK_GNSS,T_temp,X0,rtk,EPH);
        [gnss_rez,prev_obs,X_est_FK_GNSS,F] = process_gnss_v2vp(i,gnss_struct,gnss_rez,sp3_30s_R,prev_obs,iStart,X_est_FK_GNSS,T,rtk_ecef(627,1:3),rtk,EPH,ind_gnss); % ЗАМЕН�?Л T_temp на Т

        X_est_GNSS_UWB(1:8,i) = X_est_FK_GNSS.Xr;
%         X_est_GNSS_UWB(9,i) = 0;
        X_est_FK(:,i) =  X_est_FK_GNSS.Xr;
        sol_type(i) = 1;
        gnss_ind_last = i;
        if i > 1306
%             popravka = [ -7.89012446906418;5.32038170704618;55.6771079329774];% разница между SA в ртклибе по GPS+ГЛОНАСС и МНК ВП на момент времени gnss_struct(1289).TOW
%             X_est_FK(1:3,i) = X_est_FK(1:3,i) - popravka;
           
%            popravka = rtk_ecef(627,1:3) - Result(1306,1:3);
%            X_est_GNSS_UWB(1:3,i) = X_est_GNSS_UWB(1:3,i) + popravka';
        end
%         if(ind_uwb_last~=0)
%             X_est_GNSS_UWB(9,i) =  X_est_GNSS_UWB(9,ind_uwb_last);
%         else
%             X_est_GNSS_UWB(9,i) =  NaN;
%         end
    elseif(~isempty(ind_uwb))
%         if ind_uwb < ind_uwb_start
            X_est_GNSS_UWB(:,i) = NaN;
%         elseif ind_uwb == ind_uwb_start
% 
%             X_est_UWB(:,ind_uwb) = [x_ecef(1);%x
%                                     1.34202189277858;        %Vx
%                                     x_ecef(2);%y
%                                     4.4611857784912;       %Vy
%                                     x_ecef(3);%z
%                                     -3.07257682550699;      %Vz
%                                     cords_new_GPST(3,193)];%T_tr
% %                                     РАБОТАЕТ
%             Dx_est_prev_3d_UWB = diag([0.1;2;0.1;2;0.1;2;1e-5*c]);
%             
%             X_est_GNSS_UWB(1:8,i) = X_est_GNSS_UWB(1:8,i-1);
%             X_est_GNSS_UWB(9,i) = cords_new_GPST(3,193);
%             Dx_est_prev_3d = Dx_est_prev_3d_ini;
%             sol_type(i) = 2;
%             ind_uwb_prev = ind_uwb;
%             ind_uwb_last = i;
% 
%         elseif ind_uwb > ind_uwb_start
%             disp('UWB FILTER')
%             T = TOW_ALL(i)-TOW_ALL(i-1)
%             ind_uwb
%             i
% %             [X_est_UWB(:,ind_uwb),Dx_est_UWB,nev_uwb,nev_T_uwb,Delta_k2_uwb] = EKF_UWB_3D_v2(X_est_UWB(:,ind_uwb_prev),Dx_est_prev_3d_UWB,uwb_t_new(:,ind_uwb),uwb_t_new(:,ind_uwb_prev),radius,SatPosECEF);
% %             [X_est_UWB(:,1),Dx_est_UWB,nev_uwb,nev_T_uwb,Delta_k2_uwb] = EKF_UWB_3D_v3(X_est_GNSS_UWB(:,i-1),Dx_est_prev_3d_UWB,uwb_t_new(:,ind_uwb),uwb_t_new(:,ind_uwb-1),radius,SatPosECEF);
%             [X_est_GNSS_UWB(:,i),Dx_est,nev,nev_T,Delta_k2] = EKF_UWB_3D_v4([X_est_GNSS_UWB(1:8,i-1);X_est_GNSS_UWB(9,ind_uwb_last)],Dx_est_prev_3d,uwb_t_new(:,uwb_ind_sync+ind_uwb),uwb_t_new(:,uwb_ind_sync+ind_uwb_prev),radius,SatPosECEF,T);
%             
%             Dx_est_prev_3d = Dx_est;
%             Dx_est_prev_3d_UWB = Dx_est_UWB;
%             ind_uwb_prev = ind_uwb;
%             sol_type(i) = 2;
%             ind_uwb_last = i;
%         end
    end
end
%%
X_est_FK_lla = ecef2lla(X_est_GNSS_UWB(1:3,:)');
% X_est_FK_UWB_lla = ecef2lla(X_est_UWB([1;3;5],:)');
Result_lla = ecef2lla([gnss_rez.Result(:,1:3)]);
X_est_FK_GNSS_lla = ecef2lla(X_est_FK(1:3,:)');
sa_lla = ecef2lla([sa_pos_vel(2:4,:)']);
%%

figure()
plot(sa_lla(1:1500,2),sa_lla(1:1500,1),'x-')
grid on
hold on
plot(rtk(3,600:668),rtk(2,600:668),'. b')
plot(X_est_FK_lla(1100:ind_end,2),X_est_FK_lla(1100:ind_end,1),'o- r');
% plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'d- m')
plot(X_est_FK_GNSS_lla(2,:),X_est_FK_GNSS_lla(1,:),'x- g')
% plot(X_est_FK_UWB_lla(:,2),X_est_FK_UWB_lla(:,1),'o');
plot(Result_lla(1100:ind_end,2),Result_lla(1100:ind_end,1),'.')
plot(anc1(2),anc1(1),'d k')
plot(anc2(2),anc2(1),'d k')
plot(anc3(2),anc3(1),'d k')
plot(anc4(2),anc4(1),'d k')
axis([37.68 37.72  55.75 55.76])
%%
figure(2)
plot(rtk(3,600:668),rtk(2,600:668),'. b')
hold on
plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-')
plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'x- m')
% plot(X_est_FK_lla(:,2),X_est_FK_lla(:,1),'v m')

grid on
% plot(X_est_FK_UWB_lla(:,2),X_est_FK_UWB_lla(:,1),'o');
plot(anc1(2),anc1(1),'d k')
plot(anc2(2),anc2(1),'d k')
plot(anc3(2),anc3(1),'d k')
plot(anc4(2),anc4(1),'d k')
axis([37.68 37.72  55.75 55.76])
%%
[rtk_enu(1,:),rtk_enu(2,:),rtk_enu(3,:)] = geodetic2enu(rtk(2,627:668),rtk(3,627:668),rtk(4,627:668),rtk(2,627),rtk(3,627),rtk(4,627),wgs84Ellipsoid);
[FK_enu(1,:),FK_enu(2,:),FK_enu(3,:)] = ecef2enu(X_est_GNSS_UWB(1,1306:1420),X_est_GNSS_UWB(2,1306:1420),X_est_GNSS_UWB(3,1306:1420),rtk(2,627),rtk(3,627),rtk(4,627),wgs84Ellipsoid);
%%
FK_enu1(1,:) = FK_enu(1,~isnan(FK_enu(1,:)));
FK_enu1(2,:) = FK_enu(2,~isnan(FK_enu(2,:)));
FK_enu1(3,:) = FK_enu(3,~isnan(FK_enu(3,:)));
%%
mat = mean(rtk_enu'-FK_enu1(:,1:42)','omitnan')
sko = std(rtk_enu'-FK_enu1(:,1:42)','omitnan')