super_cadr = 0;
for i = 1:length(tags4(1).data(1,:))
    if(tags4(1).data(2,i) == 0)
        super_cadr = super_cadr + 1; 
    end
    seq(i) = tags4(1).data(2,i) + super_cadr*255;
end
%%
for i = 1:length(tags4(1).data(1,:))
    ksi2(i) = tags4(1).data(3,i) - (tags4(1).data(2,i)-1)*0.1;
end
%%
%38, 196

%% 
% k(1) = 231, k(end)=400
% i(1) = 172 i(end)=256
% tags4(1).data(3,i)
% tags(1).toa(1,ia2)
t0 = tags(1).toa(1,172);
prir =    tags(1).toa(1,172:256)-tags(1).toa(1,172);
prir_2 =  tags(1).toa(2,172:256)-tags(1).toa(1,172);
prir_3 =  tags(1).toa(3,172:256)-tags(1).toa(1,172);
prir_4 =  tags(1).toa(4,172:256)-tags(1).toa(1,172);
for i = 1:84
    ksi3(i) = prir(i) - (i-1)*0.098;
    ksi3_2(i) = prir_2(i) - (i-1)*0.098;
    ksi3_3(i) = prir_3(i) - (i-1)*0.098;
    ksi3_4(i) = prir_4(i) - (i-1)*0.098;
    m_ksi_est0(i) = round(ksi3(i)/.01);
    m_ksi_est0_2(i) = round(ksi3_2(i)/.01);
    m_ksi_est0_3(i) = round(ksi3_3(i)/.01);
    m_ksi_est0_4(i) = round(ksi3_4(i)/.01);
    time_est0(i) = tags(1).toa(1,172) +  m_ksi_est0(i)/1e-3;
end
%%
figure;plot(m_ksi_est0,'x')
hold on
plot(m_ksi_est0_2,'o')
plot(m_ksi_est0_3,'v m')
plot(m_ksi_est0_4,'d k')
%%
t0 = tags(1).toa(1,172);
prir =  uwb_toa(1,172:256)-t0;
prir_2 =  uwb_toa(2,172:256) - uwb_toa(1,172);
prir_3 =  uwb_toa(3,172:256) - uwb_toa(1,172);
prir_4 =  uwb_toa(4,172:256) - uwb_toa(1,172);
for i = 1:length(uwb_toa(1,172:256))
    ksi3(i)   = prir(i) - (i-1)*0.098;
    ksi3_2(i) = prir_2(i) - (i-1)*0.098;
    ksi3_3(i) = prir_3(i) - (i-1)*0.098;
    ksi3_4(i) = prir_4(i) - (i-1)*0.098;
    m_ksi_est1(i) = round(ksi3(i)/1e-3);
    m_ksi_est1_2(i) = round(ksi3_2(i)/1e-3);
    m_ksi_est1_3(i) = round(ksi3_3(i)/1e-3);
    m_ksi_est1_4(i) = round(ksi3_4(i)/1e-3);
end
%%
figure;plot(m_ksi_est1,'x')
hold on
plot(m_ksi_est1_2,'o')
plot(m_ksi_est1_3,'v m')
plot(m_ksi_est1_4,'d k')
%%
figure()
plot(R_est(1,172:265),'.-')
hold on
plot(R_est(2,172:265),'.-')
plot(R_est(3,172:265),'.-')
plot(R_est(4,172:265),'.-')
%%
for i = 1:length(uwb_toa(1,172:256))
    ksi3_sum(i) = sum(ksi3(1:i),'omitnan');
end
%%
T_nom = 0.098;
[~,m_ind_prev] = min(uwb_toa(:,172));
for i = 173:256
    [~,m_ind] = min(uwb_toa(:,i));
    toa_diff = (uwb_toa(m_ind,i)-uwb_toa(m_ind_prev,i-1))-T_nom;
    m_est2(i) = round(toa_diff/1e-3);
    m_ind_prev = m_ind;
end