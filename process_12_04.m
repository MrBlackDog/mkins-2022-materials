% X_est_GNSS_UWB = nan(9,1700);
X_est_prev_GNSS_UWB_3d = [x_ecef(1);%x
                 x_ecef(2);%y
                 x_ecef(3);%z
                 0;
                 1.34202189277858;       %Vx
                 4.4611857784912;        %Vy
                 -3.07257682550699;      %Vz
                 0;
                 cords_new_GPST(3,193)];%T_tr
Dx_est_3d = diag([0.1;0.1;0.1;15;2;2;2;10;0.1*c]);

%%
% в нач приближении можно брать скорость по двум МНК
X_est_GNSS_UWB_3d(:,193) = X_est_prev_GNSS_UWB_3d; % 172 или 193
Dx_est_prev_3d = Dx_est_3d;
for i = 194:256% 173 - 256 весь первый кусок, 194-256 - три и более наблюдения

    [X_est_GNSS_UWB_3d(:,i),Dx_est_3d,nev2(:,i)] = EKF_UWB_3D_v3(X_est_GNSS_UWB_3d(:,i-1),Dx_est_prev_3d,uwb_t_new(:,i),uwb_t_new(:,i-1),radius,SatPosECEF); % новая версия расчета дельты
%     Dx_est_prev_2d = Dx_est_2d;
    Dx_est_prev_3d = Dx_est_3d;
end
%%
X_est_lla = ecef2lla(X_est_GNSS_UWB_3d([1 2 3],:)')';