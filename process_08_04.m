%% 
% [all_meas] = create_all_meas(TOW1,TOW_UWB,uwb_t_new,new_meas);
%% функция для получения двух раздельных наборов наблюдений из всего набора
% [gnss,uwb] = get_GNSS_and_UWB_obs(all_meas);
c = 299792458; % в метрах/секунду
R_z = 6371e3; % в метрах
%%
% SV_pos = zeros(12, 1+4+1+2);
% [ X, Y, Z, dT ] = satpos_glo(t, CodeRange, EPH{sv}(idx), Xb);
% SV_pos(m, 1:8) = [sv X Y Z dT CodeRange добавить доплер и фазу azimuth elevation];sv - системный номер аппарата

% SV_Pos = x,y,z НКА, 
enu = [55.7550107860000;37.7060240010000;161.971100000000];
% anc_oleg = enu;
[e1,n1,u1] = geodetic2enu(anc_nikita(1),anc_nikita(2),anc_nikita(3),anc_oleg(1),anc_oleg(2),anc_oleg(3),wgs84Ellipsoid);
[e2,n2,u2] = geodetic2enu(anc_sanya(1),anc_sanya(2),anc_sanya(3),anc_oleg(1),anc_oleg(2),anc_oleg(3),wgs84Ellipsoid);
[e3,n3,u3] = geodetic2enu(anc_tanya(1),anc_tanya(2),anc_tanya(3),anc_oleg(1),anc_oleg(2),anc_oleg(3),wgs84Ellipsoid);
%%
anc1_ecef = lla2ecef(anc2','WGS84');
anc2_ecef = lla2ecef(anc1','WGS84');
anc3_ecef = lla2ecef(anc3','WGS84');
anc4_ecef = lla2ecef(anc4','WGS84');
SatPosLLA = [anc_oleg anc_nikita anc_sanya anc_tanya];
SatPosECEF(:,1) = anc1_ecef';
SatPosECEF(:,2) = anc2_ecef';
SatPosECEF(:,3) = anc3_ecef';
SatPosECEF(:,4) = anc4_ecef';
%% Пересчет кооридинат МНК из ТЦСК в BLH
for i = 193:256
    [mnk_lat(i),mnk_lon(i),mnk_h(i)] = enu2geodetic(cords_new_GPST(1,i),cords_new_GPST(2,i),0,anc2(1),anc2(2),anc2(3), wgs84Ellipsoid);
end
%% EKF 
% X_est_prev = [cords_new2(1,193);2;cords_new2(2,193);-5;cords_new2(3,193)];% cords_new2 - оценка по МНК в ШВ мастера
x_ecef = lla2ecef(rtk(2:4,636)');
radius = sqrt( x_ecef(1)^2+x_ecef(2)^2+x_ecef(3)^2);
%%
X_est_prev_3d = [x_ecef(1);%x
              1.34202189277858;        %Vx
              x_ecef(2);%y
              4.4611857784912;       %Vy
              x_ecef(3);%z
              -3.07257682550699;      %Vz
              cords_new_GPST(3,193)];%T_tr
          
X_est_prev_2d = [cords_new_GPST(1,193);
                 2;
                 cords_new_GPST(2,193);
                 -5;
                 cords_new_GPST(3,193)];
%%
% в нач приближении можно брать скорость по двум МНК
X_est_3d(:,193) = X_est_prev_3d; % 172 или 193
X_est_2d(:,193) = X_est_prev_2d;
Dx_est_prev_2d = diag(ones(1,5));
% Dx_est_prev_3d = diag(ones(1,7)); % OLD
Dx_est_prev_3d = diag([0.1;2;0.1;2;0.1;2;0.1*c]);

for i = 194:256% 173 - 256 весь первый кусок, 194-256 - три и более наблюдения
%     if(i==216)
        disp(i)
%     end
%     [X_est(:,i),Dx_est,nev(:,i)] =
%     EKF_UWB(X_est_prev,Dx_est_prev,uwb_toa(:,i),uwb_toa(:,i-1),SatPos);%с расчетом дельты
    
%     [X_est_2d(:,i),Dx_est_2d,nev(:,i)] = EKF_UWB_v3(X_est_2d(:,i-1),Dx_est_prev_2d,uwb_t_new(:,i),uwb_t_new(:,i-1),SatPos); % старая версия расчета через m
%     disp('в цикле')
%     disp(X_est_3d(:,i-1))
%     disp(c)
%     disp(X_est_3d(end,i-1)/c)
    [X_est_3d(:,i),Dx_est_3d,nev2(:,i)] = EKF_UWB_3D_v2(X_est_3d(:,i-1),Dx_est_prev_3d,uwb_t_new(:,i),uwb_t_new(:,i-1),radius,SatPosECEF); % новая версия расчета дельты
%     Dx_est_prev_2d = Dx_est_2d;
    Dx_est_prev_3d = Dx_est_3d;
end
X_est_lla = ecef2lla(X_est_3d([1 3 5],:)')';
%%
% cords_new_GPST(4,193:256) = zeros(1,193:256);
% X_mnk_lla = enu2lla(cords_new_GPST([1;2;4],193:256),anc_oleg','flat');
%% координаты в lla
figure()
plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'x-')
hold on
plot(rtk(3,627:680),rtk(2,627:680),'.-')
plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-') % перенести в BLH
% plot(X_est(1,193:256),X_est(3,193:256),'x-')
% hold on
% plot(X_est(1,193:256),X_est(3,193:256),'o-')
% plot(cords_new2(1,193:256),cords_new2(2,193:256),'.- g')
% plot(rtk_enu(2,:),rtk_enu(3,:),'.-')
% legend('Новый фильтр','Фильтр через m','МНК','РТК')
%% кооридната x от тиков
figure()
plot(193:256,X_est_3d(1,193:256),'x')
hold on
plot(193:256,cords_new2(1,193:256),'.')
%% кооридната y от тиков
figure()
plot(193:256,X_est_3d(3,193:256),'x')
hold on
plot(193:256,cords_new2(2,193:256),'.')
%% Vx от тиков
figure()
plot(193:256,X_est_3d(2,193:256),'x')
%% Vy от тиков
figure()
plot(193:256,X_est_3d(4,193:256),'x')