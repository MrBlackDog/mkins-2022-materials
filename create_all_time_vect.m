function [sorted_TOW,TOW_UWB] = create_all_time_vect(TOW1,uwb_t_new)


%% получение общего вектора СШП
for i = 1:length(uwb_t_new(1,:))
    if(~isnan(uwb_t_new(1,i)))
        TOW_UWB(i) = uwb_t_new(1,i);
%         continue
%         break;
    elseif(~isnan(uwb_t_new(2,i)))
        TOW_UWB(i) = uwb_t_new(2,i);
%         continue
%         break;
    elseif(~isnan(uwb_t_new(3,i)))
        TOW_UWB(i) = uwb_t_new(3,i);
%         continue
%         break;
    elseif(~isnan(uwb_t_new(4,i)))
        TOW_UWB(i) = uwb_t_new(4,i);
%         continue
%         break;
    end
end
%% поиск пересечений
TOW_all = [TOW1' TOW_UWB];
sorted_TOW = sort(TOW_all);
end
