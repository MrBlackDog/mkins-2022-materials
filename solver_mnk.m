function [cords, toa] = solver_mnk(tag,SatPos)
    toa = tag.toa;
    c = 299792458;
    k = 0;
    for i = 1:length(toa)
    k = k + 1;
    nums = find(toa(:,i));
        if length(nums) > 2
            
            [X, dop, nev, flag] = coord_solver2D(toa(nums,i)*c, SatPos(:,nums), [30;30;max(toa(nums,i))*c], 0);
            if flag 
               
%                cords(:,k) = [tag.time(i);X];
               cords(:,k) = [X];
               toa(:,k) = toa(:,i);
            else
%                cords(:,k)= [NaN;NaN;NaN;NaN];
                cords(:,k)= [NaN;NaN;NaN];
            end
        else
%                cords(:,k)= [NaN;NaN;NaN;NaN];
                cords(:,k)= [NaN;NaN;NaN];
        end
    end
end

