% %%
% rowHeadings = {'SVid', 'R1', 'phase1', 'Fdop1','SNR1'...
%    'R2', 'phase2', 'Fdop2','SNR2'};
% output_struct = cell2struct(output, rowHeadings, 2);
% %%

%%
% svids = extractfield(output_struct , 'SVid');
% for i = 1:length(SVlist)
% %     [~,meas_ind(i)] = find(svids==SVlist{i});
% meas_ind(i,:) = strcmp(svids,SVlist{i});
% 
% end
% %%
% GPS_ind = contains(SVlist,'G');
% GLO_ind = contains(SVlist,'R');
% for i = 1:length(output)/numSV
% %     new_meas(i).time = time_block(i*numSV);
%     new_meas(i).TOW = out_date_gps(i,:);
%     obs = output(i*(1:numSV),[1,2,3,4,5,7,8,9,10]);
%     new_meas(i).GPS_obs = obs(GPS_ind,:);
%     new_meas(i).GLO_obs = obs(GLO_ind,:);
%     new_meas(i).UWB_osb = 0;
% end
% %%\
%%
sv = struct('sv',[],'X',[],'Y',[],'Z',[],'dT',[],'CodeRange_L1',[],'Fdop_L1',[],'Phase_L1',[],'SNR_L1',[],'CodeRange_L2',[],'Fdop_L2',[],'Phase_L2',[],'SNR_L2',[],'azimuth',[],'elevation',[]);
st = struct('TOW',[],'T_GPST',[],'GPS_obs',sv,'GLO_obs',sv,'UWB_obs',[]);
new_meas2 = repmat(st,length(out_date_gps),1);
%%
k = 1;
gps_obs = 0;
glo_obs = 0;
i = 0;
while i ~= length(output)
    gps_obs = 0;
    glo_obs = 0;
    if(k==701)
        disp k
    end
    while 1
        i = i + 1;
        new_meas2(k).TOW = TOW1(k);
        new_meas2(k).T_GPST = out_date(k,4)*3600 + out_date(k,5)*60 + out_date(k,6);
        if( contains(output(i,1),'G'))
            gps_obs = gps_obs + 1;
            new_meas2(k).GPS_obs(gps_obs).sv = output{i,1};
            new_meas2(k).GPS_obs(gps_obs).CodeRange_L1 = output{i,2};
            new_meas2(k).GPS_obs(gps_obs).Fdop_L1 = output{i,4};
            new_meas2(k).GPS_obs(gps_obs).Phase_L1 = output{i,3};
            new_meas2(k).GPS_obs(gps_obs).SNR_L1 = output{i,5};
            new_meas2(k).GPS_obs(gps_obs).CodeRange_L2 = output{i,7};
            new_meas2(k).GPS_obs(gps_obs).Fdop_L2 = output{i,9};
            new_meas2(k).GPS_obs(gps_obs).Phase_L2 = output{i,8};
            new_meas2(k).GPS_obs(gps_obs).SNR_L2 = output{i,10};
        elseif ( contains(output(i,1),'R'))
            glo_obs = glo_obs + 1;
            new_meas2(k).GLO_obs(glo_obs).sv = output{i,1};
            new_meas2(k).GLO_obs(glo_obs).CodeRange_L1 = output{i,2};
            new_meas2(k).GLO_obs(glo_obs).Fdop_L1 = output{i,4};
            new_meas2(k).GLO_obs(glo_obs).Phase_L1 = output{i,3};
            new_meas2(k).GLO_obs(glo_obs).SNR_L1 = output{i,5};
            new_meas2(k).GLO_obs(glo_obs).CodeRange_L2 = output{i,7};
            new_meas2(k).GLO_obs(glo_obs).Fdop_L2 = output{i,9};
            new_meas2(k).GLO_obs(glo_obs).Phase_L2 = output{i,8};
            new_meas2(k).GLO_obs(glo_obs).SNR_L2 = output{i,10};
        end

        if(~(time_block(i+1)-time_block(i)==0))
            break;
        end
    end
    k = k + 1;
end
%%
%Формат:
% массив
% время(UTC/GPS), дата, номер спутника, измерения по порядку
% Формат, чтобы подходил в калман
% разделить GPS и GLO 
% время, GPSobs,GLOobs,UWBobs
% время,      0,     0,UWBobs %пока не используем
% темп фильтра 0.2
% темп фильтра 0.1 % пока не используем
%%
% коорды опорных точек в отдельную структуру по такому же принципу
% время, GPSpos,GLOpos,UWBpos
% момент обращения к коордам спутника на момент излучения на момент
% как на момент приема найти время излучения
% если меняется созвездие, то надо будет отследить этот момент нужно
% сгенерить поправку относительно последнего решения
% считаем геометрическую дальность, вычитаем её из измерения, полученную
% поправку вычесть из измерения и закинуть в фильтр.
%%
nav_SVlist_GPS = 1:32;
for i = 1:length(nav_SVlist_GPS)
%     [~,meas_ind(i)] = find(svids==SVlist{i});
    meas_ind_nav_gps(i,:) = eph_gps(1,:)==nav_SVlist_GPS(i);
end
%%
for i = 1:length(nav_SVlist_GPS)
    gps_sat_eph(i).sv_num = mean(eph_gps(1,meas_ind_nav_gps(i,:)));
    gps_sat_eph(i).eph.af2 = eph_gps(2,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.M0 = eph_gps(3,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.roota = eph_gps(4,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.deltan = eph_gps(5,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.ecc = eph_gps(6,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.omega = eph_gps(7,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.cuc = eph_gps(8,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.cus = eph_gps(9,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.crc = eph_gps(10,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.crs = eph_gps(11,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.i0 = eph_gps(12,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.idot = eph_gps(13,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.cic = eph_gps(14,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.cis = eph_gps(15,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.Omega0 = eph_gps(16,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.Omegadot = eph_gps(17,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.toe = eph_gps(18,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.af0 = eph_gps(19,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.af1 = eph_gps(20,meas_ind_nav_gps(i,:));
    gps_sat_eph(i).eph.tgd = eph_gps(21,meas_ind_nav_gps(i,:));  
end
%%
satp = satposRTK(t,k);