figure()
plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'x-')
hold on
plot(rtk(3,627:680),rtk(2,627:680),'.-')
plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-') % перенести в BLH
%%
figure()
plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'x-')
hold on
plot(rtk(3,627:680),rtk(2,627:680),'.-')
plot(Result_lla(1300:1360,2),Result_lla(1300:1360,1),'.-')
plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-')
%%
figure()
plot(rtk(3,627:680),'.-')
hold on
plot(Result_lla(:,2),'.-')
% plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-')
%%
figure()
plot3(X_est_3d(1,193:256),X_est_3d(3,193:256),X_est_3d(5,193:256),'x-')
hold on
plot3(rtk_ecef(627:680,1),rtk_ecef(627:680,2),rtk_ecef(627:680,3),'.-')
plot3(Result(1300:1360,1),Result(1300:1360,2),Result(1300:1360,3),'.-')
plot3(Result_new(1,:),Result_new(2,:),Result_new(3,:),'o-')
% plot3(cords_new_GPST(1,193:256),cords_new_GPST(2,193:256),cords_new_GPST(3,193:256),'o-')
%%
figure()
plot(rtk_ecef(627:680,1)-Result(1300:1353,1),'.')
figure()
plot(rtk_ecef(627:680,2)-Result(1300:1353,2),'.')
%%
figure()
% plot(rtk_ecef(627:680,3)-Result(1300:1353,3),'.')
hold on
plot(Result(1300:1353,3)-Result_new(3,1300:1353)','x')
%% с ФК ГНСС
figure()
plot(X_est_lla(2,193:256),X_est_lla(1,193:256),'x-')
hold on
plot(rtk(3,560:680),rtk(2,560:680),'.-')
plot(Result_lla(1150:1360,2),Result_lla(1150:1360,1),'.')
% plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-')
plot(X_est_FK_lla(1190:1700,2),X_est_FK_lla(1190:1700,1),'v')
%% с ФК ГНСС
figure()
plot3(X_est_lla(2,193:256),X_est_lla(1,193:256),X_est_lla(3,193:256),'x-')
hold on
plot3(rtk(3,560:680),rtk(2,560:680),rtk(4,560:680),'.-')
plot3(Result_lla(1150:1360,2),Result_lla(1150:1360,1),Result_lla(1150:1360,3),'.-')
% plot3(mnk_lon(2,193:256),mnk_lat(1,193:256),mnk_lat(3,193:256),'o-')
plot3(X_est_FK_lla(1190:1700,2),X_est_FK_lla(1190:1700,1),X_est_FK_lla(1190:1700,3),'v')
axis([37.7 37.8  55.7 55.8 100 200])
%%
figure()
plot(rtk(1,560:680),rtk(4,560:680),'.')
hold on
plot([gnss_struct(1150:1360).T_GPST],Result_lla(1150:1360,3),'.')
plot([gnss_struct(1190:1700).T_GPST],X_est_FK_lla(1190:1700,3),'.')
%%
figure()
plot(gnss_rez.dPos(1150:1700,1),'.')
hold on
plot(gnss_rez.dPos(1150:1700,2),'.')
plot(gnss_rez.dPos(1150:1700,3),'.')