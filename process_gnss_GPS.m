function [gnss_rez,prev_obs,X_est_FK_GNSS,F] = process_gnss_GPS(k,gnss_struct,gnss_rez,sp3_30s_R,prev_obs,iStart,X_est_FK_GNSS,T_temp,X0,rtk,EPH)

sigmaA=20.;%20;%8; %меньше проблема сужать полосу??
%%% Дисперсия приращений ускорения в динам. модели потребителя, разм. м^2/с^2. %%%
Dksidyn=0.2*sigmaA^2*0.005;
%%

Xb=[ 2846079.9100  2200502.1000  5249252.6600];%м !!%%, м WGS-84. из файла 
% Vb=[0 0 0]; %% в ГЦСК метры/сек
%%
% gnss_rez.PosCorr = Xb';
%%
c = 299792458;
%
actualLiters = [];
 [F1,F2, lam1,lam2] = litersGLO(actualLiters);

 %%
 [ B, L, H ] = XYZ_to_BLH_GOST32453_2013( X0 );
	
NED2ECEF = [	-sind(B)*cosd(L)	-sind(L)	-cosd(B)*cosd(L);
				-sind(B)*sind(L)	cosd(L)		-cosd(B)*sind(L);
				cosd(B)				0			-sind(B)];
ECEF2NED = NED2ECEF';
%%
    NGR_time = [gnss_struct.TOW]+(3*3600);
%     NGR_time = [gnss_struct.T_GPST]+(3*3600);
	if NGR_time(k)==0 
		return
	end
	
	% время(секунды) внутри суток
	t = rem(NGR_time(k), 86400);
%     gnss_struct(k).TOW
	m = 1;
%      SV_pos = zeros(12, 1+4+1+2);
%      SV_vel = NaN(12, 6);
    SV_pos0 = NaN(24, 8);
    SV_vel0 = NaN(24, 6);
    PhaseRange0 =  NaN(24,1);
%     PhaseRangePrev = NaN(1,12);
%     PhaseRange= NaN(1,12);
    
%    for j=1:24 %12
    for j=1:length(gnss_struct(k).GPS_obs)
		sv = str2double(gnss_struct(k).GPS_obs(j).sv(2:3));
		if (sv == 0)
%             disp(sv)
			break
		end
		
% 		if (~isempty(EPH{sv}))
        answer=find(sp3_30s_R(:,3)==sv);% проверка наличия координат НКА в массиве sp3_30s_R
%         answer=find(sp3_30s_R.data(:,3)==sv);% проверка наличия координат НКА в массиве sp3_30s_R
          if (~isempty(answer))   % переделка под sp3_30s_R!! 
             
         %%          для работы через эфемериды штатные BRDC 
         % теперь для каждого НКА в отсчёте ищем ближайшие эфемериды по Tb и рассчитываем положение НКА
			Tbs = [EPH{sv}.Tb_sec]';
			[a,idx] = min(abs(Tbs - t));
% 			idx - номер блока эфемерид которые надо использовать
% 			
        %%
            CodeRange = gnss_struct(k).GLO_obs(j).CodeRange_L1;
            PhaseRange0(sv,1) = gnss_struct(k).GLO_obs(j).Phase_L1; %!!! фаза в циклах берется из структуры
%% пересчёт циклов ГЛОНАСС в метры
            PhaseRange0(sv,1) = PhaseRange0(sv,1)*lam1(sv); %!!! теперь метры!
            
%%
% 			[ X, Y, Z, dT ] = satpos_Glo1_good(t, CodeRange, EPH{sv}(idx), X0);% работало!
%%          для работы через эфемериды штатные BRDC
%             [ Xsv, Vsv, ddX, dTsv ] = satpos_Glo1(t, CodeRange, EPH{sv}(idx), Xb); % переделать обращение EPH{sv}(idx) 
            % До сих работает на 07_04 19:28 по мск
%%          для работы через координаты НКА в массиве sp3_30s_R

%% пересчет координат НКА на момент нужного!! момента измерения
            Tsp3 = gnss_struct(k).TOW;%! - LeapSeconds;%!!!!!!!если obs от НАВИСА и sp3 в GPST!!
%             dTsp3 = min(abs(Tsp3 - t));
%             [Xsv] = precise_orbit_interpR(Tsp3, sv, sp3_30s_R,0);
            %% TEST!!
%             % Рассчитываем время излучения ГРУБО! без учета вращения Земли!
            Ttx = Tsp3 - CodeRange/c;
%             [Xsv, Vsv, dTsv, dotTsv] = precise_orbit_interpR_vp(Ttx, sv, sp3_30s_R,0);
%             [Xsv_test] = precise_orbit_interp(Ttx, sv, sp3_30s_R,0);
%             %% end TEST
            
            %% Уточнение поправок dTsv и учет вращения Земли! Пока без учёта рефракции!!
            if k==1187
                disp k
            end
            
            [ Xsv, Vsv, Ttx, dTsv, dotTsv] = satellite_positionR_sp3_v00(sv, Tsp3, CodeRange, sp3_30s_R );
%             [ Xsv,Vsv, dTsv, dotTsv] = satellite_positionR_sp3_v00(sv, Tsp3, CodeRange, sp3_30s_R );

            
            X=Xsv(1);% [м]
            Y=Xsv(2);
            Z=Xsv(3);
            %mod для сопряжения со старыми пп
            dT=dTsv;% [c] 
            dX(1)=Vsv(1); % [м/c]
            dX(2)=Vsv(2);
            dX(3)=Vsv(3); 
%             gnss_rez.dF=dotTsv;% %[c/c])???? перевести в Гц ??
            
			Kecef = [X Y Z] - X0;
			NED = ECEF2NED * Kecef';

			elevation = atan2d( -NED(3), sqrt(NED(1)*NED(1) + NED(2)*NED(2) ) );
			azimuth = atan2d( NED(2), NED(1) );
			if azimuth < 0
				azimuth = azimuth + 360.0;
			end

			if (elevation > 1.0)% изм. 10 на 1.0 град.
                
                %%%%%%%%%%%%% tropo corr      
%             [~,el,~] = topocent(Xr(1:3),Rot_X-Xr(1:3));                                                            
%             El(i,1)=el;
%                 trop = tropo(sin(elevation*deg2rad),0.0,1013.0,293.0,50.0,...
%                     0.0,0.0,0.0);
%                 CodeRange=CodeRange-trop; % коррекция: ПД - тропо
            

                SV_pos0(sv, 1:8) = [sv X Y Z dT CodeRange azimuth elevation];
%
                
                SV_vel0(sv, 1:6) = [sv dX(1) dX(2) dX(3) gnss_rez.dF NaN];
				m = m + 1;
			end
		end
    end
    

         %%%%%% дополнено! Отбор только видимых по порядку убывания номеров НКА
     SV_pos = SV_pos0(~isnan(SV_pos0(:,1)),:);%
     SV_vel = SV_vel0(~isnan(SV_vel0(:,1)),:);
     NoSV = size(SV_pos,1);

    
        %%%%%%%%%%%%%%% приращение ПД для каждого НКА  {Iono Corr Krasn }
       
%     if k == iStart
    if k == 1151
        [F,G,Q,X_est_FK_GNSS.E]=InitNavPosFK_R99v0(c,T_temp,Dksidyn);
         gnss_rez.dObs0(k,:) = (SV_pos0(:,6) - prev_obs.ObsPrev0(:,6))';%приращение ПД для каждого НКА 
         gnss_rez.dPhR0(k,:) = PhaseRange0(:) - prev_obs.PhaseRangePrev0(:);
%                  gnss_rez.PosCorr = Xb';
%         prev_obsPhaseRangePrev0
    end
     if k>iStart

        [F,G,Q,~]=InitNavPosFK_R99v0(c,T_temp,Dksidyn);
         gnss_rez.dObs0(k,:) = (SV_pos0(:,6) - prev_obs.ObsPrev0(:,6))';%приращение ПД для каждого НКА 
         gnss_rez.dPhR0(k,:) = PhaseRange0(:) - prev_obs.PhaseRangePrev0(:);

        for j=1:numel(SV_pos(:, 1)) %NoSV !!переприсвоение в формат количества видимых НКА в момент
            gnss_rez.dObs(k,j) =  gnss_rez.dObs0(k,SV_pos(j, 1));
            gnss_rez.dPhR(k,j) =  gnss_rez.dPhR0(k,SV_pos(j, 1));
        end
     end
%end

     prev_obs.ObsPrev0=SV_pos0(:,:);%
     prev_obs.PhaseRangePrev0=PhaseRange0;

%      if (m-1<4)&&(iep4end<1)
%         iep4end=k;
%      end
%     
    
     if (m-1) >= 4%
        R = LSQ_3D(SV_pos);

        gnss_rez.Result(k, :) = R;
        [~,rtk_ind] = find(abs(rtk(1,:) - gnss_struct(k).T_GPST)<0.02);
        if(~isempty(rtk_ind))
            gnss_rez.dPos(k, :) = gnss_rez.Result(k,1:3) - lla2ecef(rtk(2:4,rtk_ind)');
        else
            gnss_rez.dPos(k, :) = [NaN NaN NaN];
        end
        
        
%        gnss_rez.dPos(k, :) = gnss_rez.Result(k,1:3) - X0;%сюда надо бы подставить вместо X0 решение РТК, если есть
        gnss_rez.dTr(k) = gnss_rez.Result(k,4); %секунды!
     else
            step=k;
     end
%      
%      if ( k==1151)    
%         AA=['Ntest=',num2str(k)];
% %          disp(AA)
% %         Ntest=i
% %         PosCorr=Pos(1:3,iep-1); %% вектор-столбец!
% %         mPos=mean(ResultF(iepCorr-1000:iepCorr-1,:),1);
%         mPos=mean(ResultF(iepCorr-limCor:iepCorr-1,:),1);
%         PosCorr=mPos(1:3); %% вектор-столбец!
% %         countFK=1;
% %         Xr=[mPos;0;0;0;0];
%      end 

     if k>1150
         kk=1;
         for j=1:NoSV
%              if (~isnan(gnss_rez.dPhR(k,j)))
%                  SV_vel(kk, 6) = gnss_rez.dPhR(k,j)';
                 %%%%%%%%%%% расчёт дифф.поравки в псевдодальность
                 if (~isempty(gnss_rez.PosCorr))
                     RangeCorr=norm(SV_pos(kk,2:4) - gnss_rez.Result(k,1:3)) - norm(SV_pos(kk,2:4) - X0);
                     SV_pos(kk,6)=SV_pos(kk,6)-RangeCorr; % коррекция ПД, на 12_04 работает некорректно 
                     gnss_rez.PRCorr(k,j)=RangeCorr;
                 end
                 kk=kk+1;
%              end
         end % NoSV
     end
     
     if k>iStart
         [X_est_FK_GNSS.Xr,X_est_FK_GNSS.E] = NavPosFK_99_MKINS(X_est_FK_GNSS.Xr,1,SV_pos,SV_vel,1,F,Q,X_est_FK_GNSS.E,T_temp);
     end
     
end