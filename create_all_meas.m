function [all_meas] = create_all_meas(TOW1,uwb_t_new,new_meas)


%% получение общего вектора СШП
for i = 1: length(uwb_t_new(1,:))
    if(~isnan(uwb_t_new(1,i)))
        TOW_UWB(i) = uwb_t_new(1,i);
    elseif(~isnan(uwb_t_new(2,i)))
        TOW_UWB(i) = uwb_t_new(2,i);
    elseif(~isnan(uwb_t_new(3,i)))
        TOW_UWB(i) = uwb_t_new(3,i);
    elseif(~isnan(uwb_t_new(4,i)))
        TOW_UWB(i) = uwb_t_new(4,i);
    end
end
%% поиск пересечений
TOW_all = [TOW1' TOW_UWB];
sorted_TOW = sort(TOW_all);
[~,ia,ib] = intersect(sorted_TOW,TOW_UWB);
[~,ia2,ib2] = intersect(sorted_TOW,TOW1');
%% формирование общего вектора измерений
ic = 1;
ic2 = 1;
for i = 1:length(sorted_TOW)
    if(i==1477)
        disp(i)
    end
    all_meas(i).TOW = sorted_TOW(i);
    if(i==ia(ic))
        all_meas(i).UWB_OBS =  uwb_t_new(:,ib(ic));
        ic = ic + 1;
    elseif(i==ia2(ic2))
        all_meas(i).T_GPST = new_meas(ib2(ic2)).T_GPST;
        all_meas(i).GPS_OBS = new_meas(ib2(ic2)).GPS_obs;
        all_meas(i).GLO_OBS = new_meas(ib2(ic2)).GLO_obs;
        ic2 = ic2 + 1;
    end
end

end

