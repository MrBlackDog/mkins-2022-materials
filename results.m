%% RTK lla -> ENU
wgs84 = wgs84Ellipsoid;
for i = 1:length(rtk)
[rtk_enu2(1,i),rtk_enu2(2,i),rtk_enu2(3,i)] = geodetic2enu(rtk(2,i),rtk(3,i),rtk(4,i),anc2(1),anc2(2),anc2(3),wgs84);
end
%% Sa lla -> ENU
wgs84 = wgs84Ellipsoid;
for i = 1:length(sa_pos_vel)
    [sa_enu(1,i),sa_enu(2,i),sa_enu(3,i)] = ecef2enu(sa_pos_vel(2,i),sa_pos_vel(3,i),sa_pos_vel(4,i),anc2(1),anc2(2),anc2(3),wgs84);
    [sa_enu(4,i),sa_enu(5,i),sa_enu(6,i)] = ecef2enuv(sa_pos_vel(5,i),sa_pos_vel(6,i),sa_pos_vel(7,i),anc2(1),anc2(2));
end
%% UWB only ECEF -> ENU
wgs84 = wgs84Ellipsoid;
for i = 1:length(X_est_GNSS_UWB)
    [gnss_uwb_enu(1,i),gnss_uwb_enu(2,i),gnss_uwb_enu(3,i)] = ecef2enu(X_est_GNSS_UWB(1,i),X_est_GNSS_UWB(2,i),X_est_GNSS_UWB(3,i),anc2(1),anc2(2),anc2(3), wgs84);
    [gnss_uwb_enu(4,i),gnss_uwb_enu(5,i),gnss_uwb_enu(6,i)] = ecef2enuv(X_est_GNSS_UWB(5,i),X_est_GNSS_UWB(6,i),X_est_GNSS_UWB(7,i),anc2(1),anc2(2));
end
%% GLO EKF only ECEF -> ENU
wgs84 = wgs84Ellipsoid;
k = 0;
X_est_GNSS_only(X_est_GNSS_only==0) = NaN;
for i = 1:length(X_est_GNSS_only)
      if(~isnan(X_est_GNSS_only(1,i)))
        k = k + 1;
        [glo_fk_enu(1,k),glo_fk_enu(2,k),glo_fk_enu(3,k)] = ecef2enu(X_est_GNSS_only(1,i),X_est_GNSS_only(2,i),X_est_GNSS_only(3,i),anc2(1),anc2(2),anc2(3), wgs84);
        [glo_fk_enu(4,k),glo_fk_enu(5,k),glo_fk_enu(6,k)] = ecef2enuv(X_est_GNSS_only(5,i),X_est_GNSS_only(6,i),X_est_GNSS_only(7,i),anc2(1),anc2(2));
      end
end
%% GLO LSQ only ECEF -> ENU
wgs84 = wgs84Ellipsoid;
% k = 0;
gnss_rez.Result(gnss_rez.Result==0) = NaN
for i = 1:length(gnss_rez.Result)
      if(gnss_rez.Result(i,:)~=0)
%         k = k + 1;
        [glo_mnk_enu(1,i),glo_mnk_enu(2,i),glo_mnk_enu(3,i)] = ecef2enu(gnss_rez.Result(i,1),gnss_rez.Result(i,2),gnss_rez.Result(i,3),anc2(1),anc2(2),anc2(3), wgs84);
      end
end
%% UWB EKF only ECEF -> ENU
wgs84 = wgs84Ellipsoid;
k = 0;
for i = 1:length(X_est_UWB)
      if(X_est_UWB(1,i)~=0)
        k = k + 1;
        [uwb_enu(1,k),uwb_enu(2,k),uwb_enu(3,k)] = ecef2enu(X_est_UWB(1,i),X_est_UWB(3,i),X_est_UWB(5,i),anc2(1),anc2(2),anc2(3), wgs84);
        [uwb_enu(4,k),uwb_enu(5,k),uwb_enu(6,k)] = ecef2enuv(X_est_UWB(2,i),X_est_UWB(4,i),X_est_UWB(6,i),anc2(1),anc2(2));
      end
end
%%
wgs84 = wgs84Ellipsoid;
k = 0;
% for i = 1:length(X_est_UWB)
%       if(X_est_UWB(1,i)~=0)
%         k = k + 1;
% %         [uwb_enu(1,k),uwb_enu(2,k),uwb_enu(3,k)] = geodetic2enu(X_est_UWB(1,i),X_est_UWB(3,i),X_est_UWB(5,i),anc3(1),anc3(2),anc3(3), wgs84);
%       end
% end
%% FK GNSS + UWB vs RTK ENU
figure() 
plot(rtk_enu2(1,636:669),rtk_enu2(2,636:669),'.-')
hold on
plot(gnss_uwb_enu(1,1300:1440),gnss_uwb_enu(2,1300:1440),'.-')
grid on
plot(SatPos(1,:),SatPos(2,:),'k v')
%% MNK,FK UWB vs RTK ENU
figure()
plot(rtk_enu2(1,627:700),rtk_enu2(2,627:700),'.-')
hold on
plot(sa_enu(1,627:700),sa_enu(2,627:700),'.-')
% plot(cords_new_GPST(1,193:256),cords_new_GPST(2,193:256),'o-')
grid on
plot(uwb_enu(1,:),uwb_enu(2,:),'.-')
plot(gnss_uwb_enu(1,1310:1480),gnss_uwb_enu(2,1310:1480),'.-')
plot(SatPos(1,:),SatPos(2,:),'k v')
%% FK GNSS + UWB vs RTK LLA
% figure()
% plot(rtk(3,627:680),rtk(2,627:680),'.-')
% hold on
% plot(X_est_FK_lla(1300:1400,2),X_est_FK_lla(1300:1400,1),'.-');
% grid on
% plot(anc1(2),anc1(1),'d k')
% plot(anc2(2),anc2(1),'d k')
% plot(anc3(2),anc3(1),'d k')
% plot(anc4(2),anc4(1),'d k')
%% MNK,FK UWB vs RTK LLA
% ind_uwb = find([X_est_FK_UWB_lla(:,1)]~=0);
% figure()
% plot(rtk(3,500:680),rtk(2,500:680),'.-')
% hold on
% plot(mnk_lon(1,193:256),mnk_lat(1,193:256),'o-')
% plot(X_est_FK_UWB_lla(ind_uwb,2),X_est_FK_UWB_lla(ind_uwb,1),'x-');
% grid on
% plot(anc1(2),anc1(1),'d k')
% plot(anc2(2),anc2(1),'d k')
% plot(anc3(2),anc3(1),'d k')
% plot(anc4(2),anc4(1),'d k')
%% FK GNSS + GLO FK, nmk
figure()
plot(rtk_enu2(1,636:669),rtk_enu2(2,636:669),'.-')
hold on
plot(glo_fk_enu(1,1306:1413),glo_fk_enu(2,1306:1413),'.-')% 1348
plot(glo_mnk_enu(1,1306:1413),glo_mnk_enu(2,1306:1413),'.-')
%% Vel East
figure()
plot(sa_enu(7,636:700),sa_enu(4,636:700),'.-')
hold on
plot(gnss_time,glo_fk_enu(4,:),'.-')
plot(TOW_ALL(1300:1440),gnss_uwb_enu(4,1300:1440),'.-')
plot(uwb_time,uwb_enu(4,1:end-1),'.-')
grid on
legend('Абсолютный ГНСС(GPS)','Фильтр ГНСС(GLO)','Фильтр ГНСС(GLO) + ЛНС','Фильтра ЛНС')
xlabel('TOW,s')
ylabel('Проекция вектора скорости на восток,m/s')
%% Vel North 
figure()
plot(sa_enu(7,636:700),sa_enu(5,636:700),'.-')
hold on
plot(gnss_time,glo_fk_enu(5,:),'.-')
plot(TOW_ALL(1300:1440),gnss_uwb_enu(5,1300:1440),'.-')
plot(uwb_time,uwb_enu(5,1:end-1),'.-')
grid on
legend('Абсолютный ГНСС(GPS)','Фильтр ГНСС(GLO)','Фильтр ГНСС(GLO) + ЛНС','Фильтр ЛНС')
ylabel('Проекция вектора скорости на север,m/s')
xlabel('TOW,s')

% figure()
% plot(sa_enu(5,636:669))
% hold on
% plot(glo_fk_enu(5,1306:1413))
% plot(gnss_uwb_enu(6,1300:1440))
% plot(uwb_enu(5,:))
%%
time_uwb = cords_new_GPST(3,193:256)/c;
%% find RTK time
[~,ind_a,ind_b] = intersect([gnss_struct.T_GPST],round(rtk(1,:),1));
time_rtk = [gnss_struct(ind_a).TOW];
rtk(5,:) = time_rtk;
%% find SA time
[~,ind_a,ind_b] = intersect([gnss_struct.T_GPST],round(sa_pos_vel(1,:),1));
time_sa = [gnss_struct(ind_a).TOW];
sa_enu(7,:) = time_sa;
%%
a =  ~isnan(cords_new_GPST(1,193:256));
idx = 193:256;

%%
% rtk_int_enu(1,:) = interp1(rtk(5,636:669),rtk_enu2(1,636:669),cords_new_GPST(3,idx(a))/c);
% rtk_int_enu(2,:) = interp1(rtk(5,636:669),rtk_enu2(2,636:669),cords_new_GPST(3,idx(a))/c);

rtk_int_enu(1,:) = interp1(rtk(5,636:669),rtk_enu2(1,636:669),uwb_time);
rtk_int_enu(2,:) = interp1(rtk(5,636:669),rtk_enu2(2,636:669),uwb_time);

%%
fk_uwb_sdt_x = std(rtk_int_enu(1,:)-uwb_enu(1,1:53))
fk_uwb_sdt_y = std(rtk_int_enu(2,:)-uwb_enu(2,1:53))
fk_uwb_mean_x  = mean(rtk_int_enu(1,:)-uwb_enu(1,1:53))
fk_uwb_mean_y  = mean(rtk_int_enu(2,:)-uwb_enu(2,1:53))
%%
mnk_uwb_sdt_x = std(rtk_int_enu(1,:)-cords_new_GPST(1,idx(a)))
mnk_uwb_sdt_y = std(rtk_int_enu(2,:)-cords_new_GPST(2,idx(a)))
mnk_uwb_mean_x  = mean(rtk_int_enu(1,:)-cords_new_GPST(1,idx(a)))
mnk_uwb_mean_y  = mean(rtk_int_enu(2,:)-cords_new_GPST(2,idx(a)))
%%
rtk_int_enu2(1,:) = interp1(rtk(5,636:669),rtk_enu2(1,636:669),TOW_ALL(1318:1412));
rtk_int_enu2(2,:) = interp1(rtk(5,636:669),rtk_enu2(2,636:669),TOW_ALL(1318:1412));
%%
fk_gnss_uwb_sdt_x = std(rtk_int_enu2(1,:) - gnss_uwb_enu(1,1318:1412))
fk_gnss_uwb_sdt_y = std(rtk_int_enu2(2,:) - gnss_uwb_enu(2,1318:1412))
fk_gnss_uwb_mean_x  = mean(rtk_int_enu2(1,:) - gnss_uwb_enu(1,1318:1412))
fk_gnss_uwb_mean_y  = mean(rtk_int_enu2(2,:) - gnss_uwb_enu(2,1318:1412))
%%
b =  ~isnan(glo_fk_enu(1,1306:1413));
idx_g = 1306:1413;

%%
fk_gnss_sdt_x = std(rtk_enu2(1,627:669) - glo_fk_enu(1,idx_g(b)))
fk_gnss_sdt_y = std(rtk_enu2(2,627:669) - glo_fk_enu(2,idx_g(b)))
fk_gnss_mean_x  = mean(rtk_enu2(1,627:669) - glo_fk_enu(1,idx_g(b)))
fk_gnss_mean_y  = mean(rtk_enu2(2,627:669) - glo_fk_enu(2,idx_g(b)))
%%
mnk_gnss_sdt_x = std(rtk_enu2(1,627:669) - glo_mnk_enu(1,idx_g(b)))
mnk_gnss_sdt_y = std(rtk_enu2(2,627:669) - glo_mnk_enu(2,idx_g(b)))
mnk_gnss_mean_x  = mean(rtk_enu2(1,627:669) - glo_mnk_enu(1,idx_g(b)))
mnk_gnss_mean_y  = mean(rtk_enu2(2,627:669) - glo_mnk_enu(2,idx_g(b)))