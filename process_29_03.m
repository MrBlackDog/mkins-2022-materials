uwb_toa = tags(1).toa;
for i=1:4
    for j = 1:length(uwb_toa)
        if( uwb_toa(i,j)~=0)
            uwb_toa(i,j) = uwb_toa(i,j);
        else
            uwb_toa(i,j) = NaN;
        end
    end
end
%%
 uwb_t_prir = uwb_toa - t0_uwb; % делаем приращения начиная от t0_uwb
%%
t0_uwb = uwb_toa(1,172);
% t0_gps = rtk(1,627);
t0_gps = TOW(627);
%%
uwb_t_gps = t0_gps + uwb_t_prir;
%%
for i=1:4
    [uwb_t_new(i,:)] = pereshet_uwb_to_gps(uwb_toa(i,:),t0_uwb,t0_gps,1);
end

%%
    c = 299792458;
    k = 0;
    cur_t = uwb_t_new;
    for i = 1:length(cur_t)
    k = k + 1;
%     nums = find(cur_t(:,i));
    nums = find(~isnan(cur_t(:,i)));
        if length(nums) > 2
            
            [X, dop, nev, flag] = coord_solver2D(cur_t(nums,i)*c, SatPos(:,nums), [30;30;max(cur_t(nums,i))*c], 0);
            if flag 
               
               cords_new(:,k) = [X];
            else
                cords_new(:,k)= [NaN;NaN;NaN];
            end
        else
                cords_new(:,k)= [NaN;NaN;NaN];
        end
    end
%%
figure()
plot(diff(uwb_t_new(1,:)),'.')
hold on
plot(diff(uwb_toa(1,:)),'.')
grid on
legend('diff времен прихода после пересчета','diff времен прихода до пересчета')
%%
figure()
plot(enu_rtk(2,:),enu_rtk(3,:),'.')
hold on
plot(cords1(2,:),cords1(3,:),'.')
plot(cords_new(1,:),cords_new(2,:),'.k')
plot(SatPos(1,:),SatPos(2,:),'v')
%%
figure()
plot(uwb_cords(2,:),'.')
hold on
plot(cords_new(1,:),'.')
grid on
legend('до пересчета','после пересчета')
%%
figure()
hold on
plot((uwb_toa(4,:)-uwb_toa(1,:))*c,'.')
plot((uwb_t_new(4,:)-uwb_t_new(1,:))*c,'.')
plot(uwb_toa(3,:)-uwb_toa(1,:),'.b')
plot(uwb_t_new(3,:)-uwb_t_new(1,:),'.r')
plot(uwb_toa(2,:)-uwb_toa(1,:),'.b')
plot(uwb_t_new(2,:)-uwb_t_new(1,:),'.r')
legend('до пересчета','после пересчета')