%% Получение индексов
[~,ind_a2,ind_b2] = intersect(TOW_ALL,rtk(5,636:675));
%%
rtk_int_enu2(1,:) = interp1(rtk(5,636:675),rtk_enu2(1,636:675),TOW_ALL(ind_a2));
rtk_int_enu2(2,:) = interp1(rtk(5,636:675),rtk_enu2(2,636:675),TOW_ALL(ind_a2));
%%
uwb_int_enu2(1,:) = interp1(uwb_time,uwb_enu(1,1:end-1),TOW_ALL(ind_a2));
uwb_int_enu2(2,:) = interp1(uwb_time,uwb_enu(2,1:end-1),TOW_ALL(ind_a2));
%% вроде работает
figure
plot(TOW_ALL(ind_a2),rtk_int_enu2(1,:),'.-')
hold on
plot(TOW_ALL(ind_a2),gnss_uwb_enu(1,ind_a2),'.-')
plot(TOW_ALL(ind_a2),uwb_int_enu2(1,:),'.-')
%%
plot(TOW_ALL(ind_a2),rtk_int_enu2(2,:),'.-')
hold on
plot(TOW_ALL(ind_a2),gnss_uwb_enu(2,ind_a2),'.-')
plot(TOW_ALL(ind_a2),uwb_int_enu2(2,:),'.-')