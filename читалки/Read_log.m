

[fid, rinexHeader, gnssType, markerName, anttInterval, apcoords,...
    numOfObsTypes, typesOfObs, tFirstObs, tLastObs, tInterval, ... 
    timeSystem, numHeaderLines, clockOffsetsON, leapSec, eof] = ...
    rinexReadsObsFileHeader211('XXXX1950.21O');
    time_block = [];time = [];output = [];out_date = [];out_date_gps = [];
%%

%concatenate Rinex Obs Block to sngle matrix
for i=1:1e6
    line = fgetl(fid); % returns -1 if only reads EOF
    if line == -1
        break
    end
    %read the Block header only to retrive for each epoch: name and
    %number of the operative satellite.
    [fid,epochflag,clockOffset,date,numSV,SVlist] = rinexReadsObsBlockHead211(line,fid);
    [Obs, LLI, SS] = rinexReadsObsBlock211(fid, numSV, numOfObsTypes);
    
    out_date = [out_date; date'];
%     time_block = [time_block; date(4) + date(5)/60 + round(date(6))/3600 *ones(numSV,1)];
    time_block = [time_block; date(4)*24 + date(5)*60 + date(6) * ones(numSV,1)]; % в секунды
    time = [time; date(4)*60*60 + date(5)*60 + date(6)]; % в секунды
    [GPS_wk, GPS_sec_wk] = GPSweek(date(1),date(2),date(3),date(4),date(5),date(6));
    out_date_gps = [out_date_gps; GPS_sec_wk ];
    Obs_cel = num2cell(Obs);
    Obs_block = [ SVlist Obs_cel];
    output = [output;Obs_block];

end
%% GLO nav file
% [ EPH_DMY, eph_glo ] = read_eph_glo( 'hour1950.21g' );
%% GPS nav file
% eph_gps = rinexe('hour1950.21n','eph.dat');%
% rinexe('D:\Work\Laboratory\Trana\rec\r302_long\Bas_log_2016_05_07_12.00.00.16N','eph.dat');
% Eph = get_eph('eph.dat');
%%