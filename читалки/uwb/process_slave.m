function [X, tags,cur_tx] = process_slave(out, master, R)

    dw_unit = (1.0 / 499.2e6 / 128.0);
    T_max = 2^40 * dw_unit;
    c = 299792458;
    Range = R/c;
    
    tags = [];
    
    sync_flag = 0;
    X = [0;0];
    Dx = [2.46e-20 4.21e-20; 4.21e-20 1.94e-19];
    tx = [];
    rx = [];
    rx_last_cs = -1;
    tx_last_cs = -1;
    k_skip = 0;
    current_tx = -1;
    current_rx = -1;
    startnumber = 5;
    
    k = 0;
    
    
    for i = 1:length(out)
        if strcmp(out(i).type,'RX')
            [cur_tx] = find_match(out(i), master);
            if ~isempty(cur_tx)
%                 k = k + 1;
%                 out1(:,k) = [out(i).data(2);out(i).data(3);cur_tx(3)];
                if sync_flag
                    dt = cur_tx(3) - tx_last_cs;
                    [ X1, Dx1, nev, flag ] = CS_filter( X, Dx, dt, cur_tx(3), out(i).data(3), Range );
                    if flag
                        X = X1;
                        Dx = Dx1;
                        rx_last_cs = out(i).data(3);
                        tx_last_cs = cur_tx(3);
                    else
                        k_skip = k_skip + 1;
                        if k_skip == 5
                           sync_flag = 0;
                           k_skip = 0;
                           X = [0;0];
                           Dx = [2.46e-20 4.21e-20; 4.21e-20 1.94e-19];
                           disp("sync_lost")
                        end
                    end
                else
                    if length(tx) == startnumber
                        tx(1) = [];
                        rx(1) = [];
                    end
                    tx(end+1) = cur_tx(3);
                    rx(end+1) = out(i).data(3);
                    if length(tx) == startnumber
                        [flag, ax, delta] = make_initial(tx, rx, Range);
                        if flag
                            X = [ax(1) + ax(2) * tx(1);ax(2)];
                            for j = 2:startnumber
                                dt = tx(j) - tx(j-1);
                                [ X, Dx, nev, flag ] = CS_filter( X, Dx, dt, tx(j), rx(j), Range );
                            end
                            rx_last_cs = rx(end);
                            tx_last_cs = tx(end);
                            rx = [];
                            tx = [];
                            sync_flag = 1;
                            disp("Synchronized")
                        end
                    end
                end


            end
        end
        
        if strcmp(out(i).type,'BLINK')
            if sync_flag
                dt = out(i).data(3) - rx_last_cs;
                blink = out(i).data(3) - (X(1) + X(2) * dt);
                
                match_flag = 0;
                for j = 1:length(tags)
                    if tags(j).id == out(i).id
                       match_flag = 1;
                       break
                    end
                end

                if match_flag == 0
                    tag.id = out(i).id;
                    tag.count = 1;
                    tag.data(:,1) = [out(i).data(1);out(i).data(2);blink];
                    if isempty(tags)
                        tags = tag;
                    else
                        tags(end+1) = tag;
                    end
                else
                    tags(j).count = tags(j).count + 1;
                    tags(j).data(:,tags(j).count) = [out(i).data(1);out(i).data(2);blink];
                end
            end
        end
        
    end
end

function [cur_tx] = find_match(rx, tx)
    nums = find(tx(2,:) == rx.data(2));
    min_i = 0;
    cur_tx = [];
    for i = 1:length(nums)
        delta = abs(rx.data(1) - tx(1,nums(i)));
        if delta < 10
            cur_tx = tx(:,nums(i));
            break;
        end
    end
end

