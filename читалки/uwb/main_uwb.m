[tx, tags1] = read_master_log(); 
[out2] = read_slave_log();
[out3] = read_slave_log();
[out4] = read_slave_log();

dt =  1626220780.53831;
load('SatPos.mat');
SatPos(3,:) = 0;

R2 = norm(SatPos(:,2) - SatPos(:,1));
R3 = norm(SatPos(:,3) - SatPos(:,1));
R4 = norm(SatPos(:,4) - SatPos(:,1));
%% 
[X2, tags2] = process_slave(out2, tx, R2);
[X3, tags3] = process_slave(out3, tx, R3);
[X4, tags4] = process_slave(out4, tx, R4);
%%
[tags] = process_blink(tags1, tags2, tags3, tags4);
%%
[cords1, toa1] = solver_mnk(tags(1),SatPos);
[cords2, toa2] = solver_mnk(tags(2),SatPos);
[cords3, toa3] = solver_mnk(tags(3),SatPos);
[cords4, toa4] = solver_mnk(tags(4),SatPos);
%%
cords1(1,:) = cords1(1,:) - dt;
cords2(1,:) = cords2(1,:) - dt;
cords3(1,:) = cords3(1,:) - dt;
cords4(1,:) = cords4(1,:) - dt;
%%
[car_sa_blh] = read_pos_auto();
[car_enu_sa] = toENU(car_sa_blh);
[car_rtk_blh] = read_pos_auto();
[car_enu_rtk] = toENU(car_rtk_blh);
[smart_rtk_blh] = read_pos_auto();
[smart_enu_rtk] = toENU(smart_rtk_blh);

cords1(1,:) = cords1(1,:) - 41794.2797129154;
cords2(1,:) = cords2(1,:) - 41794.2797129154;
cords3(1,:) = cords3(1,:) - 41794.2797129154;
cords4(1,:) = cords4(1,:) - 41794.2797129154;
car_enu_sa(1,:) = car_enu_sa(1,:) - 41794.2797129154;
car_enu_rtk(1,:) = car_enu_rtk(1,:) - 41794.2797129154;
car_sa_blh(1,:) = car_sa_blh(1,:) - 41794.2797129154;
car_rtk_blh(1,:) = car_rtk_blh(1,:) - 41794.2797129154;
smart_enu_rtk(1,:) = smart_enu_rtk(1,:) - 41794.2797129154;


t1 = [0 8];
t2 = [233 241];
t3 = [521.2 530];
t4 = [654 664];
% t5 = [930 937];

t6 = [1321.5 1350];
t7 = [1405 1433];
t8 = [1538 1586];

[cords0, toa0] = get_time_interval(cords1, t3, toa1);
[cords0] = process_uwb(cords0, toa0);
uwb = toGEO(cords0);
[sa] = get_time_interval(car_sa_blh, t3);
[rtk] = get_time_interval(car_rtk_blh, t3);