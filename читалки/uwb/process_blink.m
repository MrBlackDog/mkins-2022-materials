function [tags] = process_blink(tags1, tags2, tags3, tags4)
    [toa, time] = get_tag('c8a9', tags1, tags2, tags3, tags4);
    tags(1).id = 'c8a9';
    tags(1).toa = toa;
    tags(1).time = time;
    
    [toa, time] = get_tag('148c', tags1, tags2, tags3, tags4);
    tags(2).id = '148c';
    tags(2).toa = toa;
    tags(2).time = time;
    
    [toa, time] = get_tag('d69b', tags1, tags2, tags3, tags4);
    tags(3).id = 'd69b';
    tags(3).toa = toa;
    tags(3).time = time;
    
    [toa, time] = get_tag('811e', tags1, tags2, tags3, tags4);
    tags(4).id = '811e';
    tags(4).toa = toa;
    tags(4).time = time;
end

function [toa, time] = get_tag(name, tags1, tags2, tags3, tags4)
    for i = 1:length(tags1)
        if tags1(i).id == name
            t1 = tags1(i).data;
            break;
        end
    end
    for i = 1:length(tags2)
        if tags2(i).id == name
            t2 = tags2(i).data;
            break;
        end
    end
    for i = 1:length(tags3)
        if tags3(i).id == name
            t3 = tags3(i).data;
            break;
        end
    end
    for i = 1:length(tags4)
        if tags4(i).id == name
            t4 = tags4(i).data;
            break;
        end
    end
    
    times = [t1(3,:) t2(3,:) t3(3,:) t4(3,:)];
    t = sort(times);
    seqs = zeros(length(times),1);
    
    k = 0;
    
    for i = 1:length(t)
        
        n = find(t1(3,:) == t(i));
        if ~isempty(n)
            seqs(i) = t1(2,n(1));
            
            if i > 1
                if seqs(i) ~= seqs(i-1)
                    k = k + 1;
                end
            else
                k = k + 1;
            end
            toa(1,k) = t1(3,n(1));
            time(k) = t1(1,n(1));
            
        end
        
        n = find(t2(3,:) == t(i));
        if ~isempty(n)
            seqs(i) = t2(2,n(1));
            
            if i > 1
                if seqs(i) ~= seqs(i-1)
                    k = k + 1;
                end
            else
                k = k + 1;
            end
            toa(2,k) = t2(3,n(1));
            time(k) = t2(1,n(1));
            
        end
        
        n = find(t3(3,:) == t(i));
        if ~isempty(n)
            seqs(i) = t3(2,n(1));
            
            if i > 1
                if seqs(i) ~= seqs(i-1)
                    k = k + 1;
                end
            else
                k = k + 1;
            end
            toa(3,k) = t3(3,n(1));
            time(k) = t3(1,n(1));
            
        end
        
        n = find(t4(3,:) == t(i));
        if ~isempty(n)
            seqs(i) = t4(2,n(1));
            
            if i > 1
                if seqs(i) ~= seqs(i-1)
                    k = k + 1;
                end
            else
                k = k + 1;
            end
            toa(4,k) = t4(3,n(1));
            time(k) = t4(1,n(1));
            
        end
        
    end
    
end

