function [out] = read_slave_log(filename)

if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end

    f = fopen(filename);
    k = 0;
    
    overflow_count = 0;
    dw_unit = (1.0 / 499.2e6 / 128.0);
    T_max = 2^40 * dw_unit;
    
    cur_ts = 0;
    prev_ts = 0;
    
    while feof(f)==0 
        
        s=fgetl(f);
        
        if contains(s,'CS_RX') || contains(s,'BLINK') 
            S = split(s);
            id = S{4,1};
            time = str2num(S{1,1});
            seq = str2num(S{5,1});
            cur_ts = str2num(S{6,1});

            if cur_ts - prev_ts < -10
                overflow_count = overflow_count + 1;
            end
            prev_ts = cur_ts;

            ts = cur_ts + T_max * overflow_count;
        end
            
        if contains(s,'CS_RX')
            
            
            k = k + 1;
            out(k).type = 'RX';
            out(k).data = [time; seq; ts];
            out(k).id = '';
           
        end
        
        if contains(s,'BLINK')
            
            
            k = k + 1;
            
            out(k).type = 'BLINK';
            out(k).data = [time; seq; ts];
            out(k).id = id;
        end
    end
    
    
fclose(f);

end



