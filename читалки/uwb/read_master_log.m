function [tx, tags,all_time] = read_master_log(filename)

if nargin == 0
    [file, path] = uigetfile('*.*');
    filename = fullfile(path,file);  
end

    f = fopen(filename);
    k = 0;
    
    overflow_count = 0;
    dw_unit = (1.0 / 499.2e6 / 128.0);
    T_max = 2^40 * dw_unit;
    
    cur_ts = 0;
    prev_ts = 0;
    tags = struct([]);
    
    all_time = [];
    abb = 0;
    while feof(f)==0 
        
        s=fgetl(f);
        
        if contains(s,'CS_TX') || contains(s,'BLINK') 
            S = split(s);
            id = S{4,1};
            time = str2num(S{1,1});
            
            seq = str2num(S{5,1});
            cur_ts = str2num(S{6,1});
            
            if cur_ts - prev_ts < -10
                overflow_count = overflow_count + 1;
            end
            prev_ts = cur_ts;

            ts = cur_ts + T_max * overflow_count;
            if contains(s,'CS_TX')
                abb = abb + 1;
                all_time(abb).time = ts;
                all_time(abb).type = S{2,1};
            end
            if contains(s,'BLINK') && strcmp(id,'c8a9')
                abb = abb + 1;
                all_time(abb).time = ts;
                all_time(abb).type = S{2,1};
                all_time(abb).seq = seq;
            end
        end
        
        if contains(s,'CS_TX')
            
            
            k = k + 1;
            tx(:,k) = [time; seq; ts];
           
        end
        
        if contains(s,'BLINK')
            
            match_flag = 0;
            for i = 1:length(tags)
                if tags(i).id == id
                   match_flag = 1;
                   break
                end
            end
            
            if match_flag == 0
                tag.id = id;
                tag.count = 1;
                tag.data(:,1) = [time;seq;ts];
                if isempty(tags)
                    tags = tag;
                else
                    tags(end+1) = tag;
                end
            else
                tags(i).count = tags(i).count + 1;
                tags(i).data(:,tags(i).count) = [time;seq;ts];
            end
        end
        
        
    end
    
    
fclose(f);

end


