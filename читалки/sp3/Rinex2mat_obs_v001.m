% ������� Rinex2mat(FileO) ����������� rinex ����� ������ 3.02 � mat ����
%    FileO - ������ ���� � rinex �����
% ������������ �������:
% ��� ���������� ������������� ������
%   GPS_ephemeris.m
%   GLO_ephemeris.m
%   SBAS_ephemeris.m
%   rod.m - read observation data ������� �� ���������� ������ ���������� 
%   rfs.m - (read Phase Shift) ������� �� ���������� ������ ����
%   svpeph.m - ����������� ��������� ��������
%   ������ ������� �� 23.11.2017


function Rinex2mat_obs_v001(FileO,OutputPath,sp3)
tic
%% �������������� ����������
    GPS=nan;
    GLO=nan;
    SBAS=nan;
    Gal=nan;%% mod
    Eph_GPS=nan;
    Eph_GLO=nan;
    Eph_SBAS=nan;
    Eph_Gal=nan; %% mod
    Fs=nan;
    Time_start=nan;
    Time_end=nan;
    LeapSeconds=nan;
    Position_X=nan;
    Position_Y=nan;
    Position_Z=nan;
    %% ��������� ���� � �����
%    FileO='E:\YandexDisk\Work\�����\2015_10_13\sigma_2015_09_13_18_00_00.15o';
%% ��������� ������ � ���� ��������� ���������� A
    fid=fopen(FileO);
    A=textscan(fid,'%s','Delimiter','\n');
    fclose all;
    A=A{1,1};
    clear fid ans
%% �������� ��������� ���������
for k=1:size(A,1)
    line=A{k};
    % ���������� ������ Rinex �����
                answer = strfind(line,'RINEX VERSION / TYPE');  % ���� ������� ���������� ������ Rinex �����
                if isempty(answer)==0
                    RinVer=str2double(line(1:9));   % ���������� ������ Rinex �����. ������ ������ F9.2
                    if RinVer~=3.01%3.02
                        error('������������ ������ Rinex �����. ��������� Rinex ���� ������ 3.02.'); % �������� ������ Rinex �����
                    end
                end
    %% ��������� ����������������� ������� ��������� � ������� ECEF
            answer = strfind(line,'APPROX POSITION XYZ');  % ���� ������� ���������� ����������������� ������� ���������
                if isempty(answer)==0
                    Position_X=str2double(line(1:14));   % ��������� X ���������� �������� � ������� ECEF. ������ ������ F14.4
                    Position_Y=str2double(line(15:28));  % ��������� Y ���������� �������� � ������� ECEF. ������ ������ F14.4
                    Position_Z=str2double(line(29:42));  % ��������� Y ���������� �������� � ������� ECEF. ������ ������ F14.4
%                   lla = ecef2lla([X,Y,Z]);    % ��������� ���������� �� ������� ECEF � �������������� ������ ������� � ������
%                   E=wgs84Ellipsoid;           % ������ ��������� ��������
                end
    %% ���� ������� �������������
            answer = strfind(line,'INTERVAL');  % ���� ������� ���������� ������� ������������
                if isempty(answer)==0
                    Fs=str2double(line(1:10)); % ��������� ������� ������������� Fs. ������ ������ F10.3
                end
    %% ���� ����� ������� ����������   
            answer = strfind(line,'TIME OF FIRST OBS');  % ���� ������� ���������� ����� ������� ����������
                if isempty(answer)==0
                    Y_s=str2double(line(1:6));      % ��� ������� ����������. ������ ������ I6
                    M_s=str2double(line(7:12));     % ����� ������� ����������. ������ ������ I6
                    D_s=str2double(line(13:18));    % ���� ������� ����������. ������ ������ I6
                    h_s=str2double(line(19:24));    % ��� ������� ����������. ������ ������ I6
                    m_s=str2double(line(25:30));    % ������ ������� ����������. ������ ������ I6
                    s_s=str2double(line(31:43));    % ������� ������� ����������. ������ ������ F13.7
                    Sistem_time=(line(49:51));      % ��������� ����������� ������� � ������� ������� ����� ����������. ������ ������ A3
                    Time_start=datenum([Y_s, M_s, D_s, h_s, m_s, s_s]); %#ok<*NASGU> % ���������� ����� ������ � ������ ������� MatLab
                    WD = weekday(datestr([Y_s M_s D_s 0 0 0],1));       % ���������� ���� ������
                    clear Y_s M_s D_s h_s m_s s_s   % ������� �������� ���������
                end
    %% ���� ����� ���������� ����������  
            answer = strfind(line,'TIME OF LAST OBS');  % ���� ������� ���������� ����� ���������� ����������
                if isempty(answer)==0
                    Y_e=str2double(line(1:6));      % ��� ���������� ����������. ������ ������ I6
                    M_e=str2double(line(7:12));     % ����� ���������� ����������. ������ ������ I6
                    D_e=str2double(line(13:18));    % ���� ���������� ����������. ������ ������ I6
                    h_e=str2double(line(19:24));    % ��� ���������� ����������. ������ ������ I6
                    m_e=str2double(line(25:30));    % ������ ���������� ����������. ������ ������ I6
                    s_e=str2double(line(31:43));    % ������� ���������� ����������. ������ ������ F13.7
                    Sistem_time_1=(line(49:51));    % ��������� ����������� ������� � ������� ������� ����� ����������. ������ ������ A3
                        if Sistem_time_1==Sistem_time   % ���������� ����������� ������� � ������� ������� ����� ������ ��������� � ����� ���������
                            clear Sistem_time_1         % ���� ��������� ������� �������� Sistem_time_1
                        end
                    Time_end=datenum([Y_e, M_e, D_e, h_e, m_e, s_e]); % ���������� ����� ��������� � ������ ������� MatLab
                    clear Y_e M_e D_e h_e m_e s_e   % ������� �������� ���������
                end
    %% ���� Leap seconds
            answer = strfind(line,'LEAP SECONDS');  % ���� ������� ���������� LEAP SECONDS
                if isempty(answer)==0
                    LeapSeconds=str2double(line(1:6));  % ��������� �������� LEAP SECONDS. ������ ������ I6
                end
    %% ���� ������ � ����������� ����������� (����� � SYS / # / OBS TYPES)
            answer = strfind(line,'SYS / # / OBS TYPES');
                if isempty(answer)==0
                    Satellite_code=line(1);         % ��������� ��� ����������� �������
                    Num_Obs=str2double(line(4:6));  % ��������� ����� ��������� ���������� ���������� ��� ��� ������� ���������
                    switch Satellite_code           % ��������� ��� ����������� �������
                    case 'G'
                        Num_Obs_GPS=Num_Obs;
                        if Num_Obs_GPS>13
                            line=[line(7:answer-1) A{k+1}];
                            A{k+1}=' ';
                            GPS=textscan(line(1:end-20),'%3s');
                            GPS(1:3:Num_Obs_GPS*3)=GPS{1,1};
                        else
                            GPS=textscan(line(7:answer-1),'%3s');
                            GPS(1:3:Num_Obs_GPS*3)=GPS{1,1};
                        end
                            GPS(2:3:Num_Obs_GPS*3)={'L'};
                            GPS(3:3:Num_Obs_GPS*3)={'S/N'};
                    case 'R'
                        Num_Obs_GLO=Num_Obs;
                        if Num_Obs_GLO>13
                            line=[line(7:answer-1) A{k+1}];
                            A{k+1}=' ';
                            GLO=textscan(line(1:end-20),'%3s');
                            GLO(1:3:Num_Obs_GLO*3)=GLO{1,1};
                        else
                            GLO=textscan(line(7:answer-1),'%3s');
                            GLO(1:3:Num_Obs_GLO*3)=GLO{1,1};
                        end
                            GLO(2:3:Num_Obs_GLO*3)={'L'};
                            GLO(3:3:Num_Obs_GLO*3)={'S/N'};
                    case 'S'
                        Num_Obs_SBAS=Num_Obs;
                        if Num_Obs_SBAS>13
                            line=[line(7:answer-1) A{k+1}];
                            A{k+1}=' ';
                            SBAS=textscan(line(1:end-20),'%3s');
                            SBAS(1:3:Num_Obs_SBAS*3)=SBAS{1,1};                          
                        else
                            SBAS=textscan(line(7:answer-1),'%3s');
                            SBAS(1:3:Num_Obs_SBAS*3)=SBAS{1,1};
                        end
                            SBAS(2:3:Num_Obs_SBAS*3)={'L'};
                            SBAS(3:3:Num_Obs_SBAS*3)={'S/N'};
                     case 'E'%% MOD!!
                        Num_Obs_Gal=Num_Obs;
                        if Num_Obs_Gal>13
                            line=[line(7:answer-1) A{k+1}];
                            A{k+1}=' ';
                            Gal=textscan(line(1:end-20),'%3s');
                            Gal(1:3:Num_Obs_Gal*3)=Gal{1,1};
                        else
                            Gal=textscan(line(7:answer-1),'%3s');
                            Gal(1:3:Num_Obs_Gal*3)=Gal{1,1};
                        end
                            Gal(2:3:Num_Obs_Gal*3)={'L'};
                            Gal(3:3:Num_Obs_Gal*3)={'S/N'};

                    end                    
                end
            
    %% ���� ����� Header
            answer = strfind(line,'END OF HEADER'); 
                if  ~isempty(answer)
                    EoH=k;
                break;  
                end 
end
% ��������� ����� ���  !!! �������� Galileo!!
    [PhaseShift_GPS,PhaseShift_GLO,PhaseShift_SBAS] = rfs(A(1:EoH)); %#ok<ASGLU>
    PhaseShift_Gal = PhaseShift_SBAS;%% �������!!

%% �������������� ���������� 
    g=1;    % ������� ��� GPS
    r=1;    % ������� ��� GLO
    s=1;    % ������� ��� SBAS
    e=1;    % ������� ��� Gal
    % �������������� �������
    if iscell(GPS)==0
        Matr_GPS=nan;
    else
        Matr_GPS=zeros(30000,10);
    end
    if iscell(GLO)==0
        Matr_GLO=nan;
    else
        Matr_GLO=zeros(30000,10);
        gps=cell(30000,1);
    end
    if iscell(SBAS)==0
        Matr_SBAS=nan;
        glo=cell(30000,1);
    else
        Matr_SBAS=zeros(30000,10);
        sbas=cell(30000,1);
    end
    if iscell(Gal)==0
        Matr_Gal=nan;
    else
        Matr_Gal=zeros(30000,10);
        gal=cell(30000,1);
    end
%     gps=cell(30000,1);
%     glo=cell(30000,1);
%     sbas=cell(30000,1);
    global_parameters=cell(8,1);
    global_parameters{1,1}='Time';  global_parameters{2}='PRN';         global_parameters{3}='X ECEF';        global_parameters{4}='Y ECEF';        global_parameters{5}='Z ECEF';        global_parameters{6}='Flag';        global_parameters{7}='N S';        global_parameters{8}='Clock ofset';
for i=EoH+1:max(size(A))
    line=A{i,1};
    if find(strncmp('>',line,1))==1
        C=textscan(line(3:end),'%f');
        %�������� �� ��������� ������ � ��������� �����
            m=cell2mat(C);
            nn=size(m,1);
            M=zeros(9,1);
            M(1:nn,1)=m;
            clear mm %%% ������ mm ??
        
        N=M(8); %% ���������� ���� ��� � ������� �����
      for j=i+1:i+N
        line=A{j};
            switch line(1)
                case 'G'
                    Matr_GPS(g,10)=str2double(line(2:3)); % ���������� ����� ��������
                    line=[line(4:end) blanks(Num_Obs_GPS*16-length(line)+3)];
                    Matr_GPS(g,1:9)=M;
                    gps{g,1}=line;
                    g=g+1;
                case 'R'
                    Matr_GLO(r,10)=str2double(line(2:3));
                    line=[line(4:end) blanks(Num_Obs_GLO*16-length(line)+3)];
                    Matr_GLO(r,1:9)=M;
                    glo{r,1}=line;
                    r=r+1;
                case 'S'                    
                    Matr_SBAS(s,10)=str2double(line(2:3));
                    line=[line(4:end) blanks(Num_Obs_SBAS*16-length(line)+3)];
                    sbas{s,1}=line;
                    Matr_SBAS(s,1:9)=M;
                    s=s+1;  
                case 'E'
                    Matr_Gal(e,10)=str2double(line(2:3));
                    line=[line(4:end) blanks(Num_Obs_Gal*16-length(line)+3)];
                    Matr_Gal(e,1:9)=M;
                    gal{e,1}=line;
                    e=e+1;
            end
      end
    end
end
%% ��������� ������������� ������� �� GPS ����������
if iscell(GPS)==1
        Matr_GPS(g:end,:)=[];
        gps(g:end,:)=[];
        Data = rod(gps,Num_Obs_GPS);
        Matr_GPS=[Matr_GPS Data];
        T0=datenum(Matr_GPS(:,1:6));
        Matr_GPS(:,1)=Matr_GPS(:,4)*3600+Matr_GPS(:,5)*60+Matr_GPS(:,6);
        Matr_GPS(:,2)=Matr_GPS(:,10);
        Matr_GPS(:,10)=[];
        Matr_GPS(:,3:5)=nan;%%% ��� XYZ  ���
        Matr_GPS(:,6)=[];
        GPS=[global_parameters' GPS];
        clear gps g i k j Data M N line EoH C Num_Obs Satellite_code Sistem_time
        % ��������� ������ �������� �� �������������� ����� GPS
        %{
                FileN=[FileO(1:end-1) 'N'];
                Eph_GPS = GPS_ephemeris(FileN);                                                
                        % ������� ��������� ��� ������� ��������
                        for i=1:size(Matr_GPS,1)
                            b=find(Eph_GPS(:,2)==Matr_GPS(i,2));
                            if isempty(b)==1
                                Matr_GPS(i,3)=nan;   
                                Matr_GPS(i,4)=nan;
                                Matr_GPS(i,5)=nan;
                            else
                                %����� GPS � �������� �� ������ ������
                                Seconds = Matr_GPS(i,1) + 3600*24*(WD-1);
                                %% ������� ����� ������� �������� ��������
                                Eph=Eph_GPS(b,:); %#ok<*FNDSB>
                                [~,ii]= min(abs((Eph(:,1)-T0(i)))); 
                           
                                if sum(Eph(ii,3:18))==0
                                    Matr_GPS(i,3)=nan;   
                                    Matr_GPS(i,4)=nan;
                                    Matr_GPS(i,5)=nan;
                                else
                                    SatECEF = svpeph (floor(Seconds), Eph(ii,3:18));
                                    Matr_GPS(i,3)=SatECEF(1);   % X ���������� �������� � ������� ECEF
                                    Matr_GPS(i,4)=SatECEF(2);   % Y ���������� �������� � ������� ECEF
                                    Matr_GPS(i,5)=SatECEF(3);   % Z ���������� �������� � ������� ECEF
                                end
                            end
                          clear ee ii Eph  
                        end
        %}
         %%mod!!  % ������� ���������� XYZ � ECEF �� ������� ����� ������ *.sp3 ����� "30s" ��� ������� ��������
                        for i=1:size(Matr_GPS,1)
                             %����� GPS � �������� �� ������ ������
                                Seconds = Matr_GPS(i,1) + 3600*24*(WD-1);
                                
                            b=find((sp3.data(:,3)==Matr_GPS(i,2) + 100)&(sp3.data(:,2)==Seconds)); %% ???
                            if ~any(b)%==1
                                Matr_GPS(i,3)=nan;   
                                Matr_GPS(i,4)=nan;
                                Matr_GPS(i,5)=nan;
                            else
                                Matr_GPS(i,3)=sp3.data(b,4);   % X ���������� �������� � ������� ECEF
                                Matr_GPS(i,4)=sp3.data(b,5);   % Y ���������� �������� � ������� ECEF
                                Matr_GPS(i,5)=sp3.data(b,6);   % Z ���������� �������� � ������� ECEF 
%                                 
                            end
                           clear b% ee ii Eph  
                        end
end
%% ��������� ������������� ������� �� GLO ����������        
if iscell(GLO)==1
        Matr_GLO(r:end,:)=[];
        glo(r:end,:)=[];
        Data = rod(glo,Num_Obs_GLO);
        Matr_GLO=[Matr_GLO Data];
        T0=datenum(Matr_GLO(:,1:6));%%%%%
        Matr_GLO(:,1)=Matr_GLO(:,4)*3600+Matr_GLO(:,5)*60+Matr_GLO(:,6);
        Matr_GLO(:,2)=Matr_GLO(:,10);
        Matr_GLO(:,10)=[];
        Matr_GLO(:,3:5)=nan;
        Matr_GLO(:,6)=[];
        GLO=[global_parameters' GLO];
        clear glo r
        % ��������� ������ �������� �� �������������� ����� GLO
        %{
                FileG=[FileO(1:end-1) 'G'];
                Eph_GLO = GLO_ephemeris(FileG);  
                % ������� ����� ������� �������� ��������
                    for i=1:size(Matr_GLO,1)
                            b=find(Eph_GLO(:,2)==Matr_GLO(i,2));
                            if isempty(b)==1
                                Matr_GLO(i,3)=nan;   % X ���������� �������� � ������� ECEF
                                Matr_GLO(i,4)=nan;   % Y ���������� �������� � ������� ECEF
                                Matr_GLO(i,5)=nan;   % Z ���������� �������� � ������� ECEF
                            else
                                Eph=Eph_GLO(b,:); %#ok<*FNDSB>
                                [~,ii]= min(abs((Eph(:,1)-T0(i))));
                           
                                T=(T0(i)-Eph(ii,1))*24*3600; % �������� ������� ������� � ��������
                                 if isempty(ii)==1
                                    Matr_GLO(i,3)=nan;   % X ���������� �������� � ������� ECEF
                                    Matr_GLO(i,4)=nan;   % Y ���������� �������� � ������� ECEF
                                    Matr_GLO(i,5)=nan;   % Z ���������� �������� � ������� ECEF
                                 else
                                    Matr_GLO(i,3)=Eph(ii,3)+Eph(ii,4)*T+Eph(ii,5)*T*T/2;   % X ���������� �������� � ������� ECEF
                                    Matr_GLO(i,4)=Eph(ii,6)+Eph(ii,7)*T+Eph(ii,8)*T*T/2;   % Y ���������� �������� � ������� ECEF
                                    Matr_GLO(i,5)=Eph(ii,9)+Eph(ii,10)*T+Eph(ii,11)*T*T/2; % Z ���������� �������� � ������� ECEF
                                 end
                            end
                    end
        %}
        %%mod!!  % ������� ���������� XYZ � ECEF �� ������� ����� ������ *.sp3 ����� "30s" ��� ������� ��������
                        for i=1:size(Matr_GLO,1)
                             %����� GPS � �������� �� ������ ������
                                Seconds = Matr_GLO(i,1) + 3600*24*(WD-1);
                                
                            b=find((sp3.data(:,3)==Matr_GLO(i,2) )&(sp3.data(:,2)==Seconds)); %% ???
                            if ~any(b)%==1
                                Matr_GLO(i,3)=nan;   
                                Matr_GLO(i,4)=nan;
                                Matr_GLO(i,5)=nan;
                            else
                                Matr_GLO(i,3)=sp3.data(b,4);   % X ���������� �������� � ������� ECEF
                                Matr_GLO(i,4)=sp3.data(b,5);   % Y ���������� �������� � ������� ECEF
                                Matr_GLO(i,5)=sp3.data(b,6);   % Z ���������� �������� � ������� ECEF 
%                                 
                            end
                           clear b %ee ii Eph  
                        end
end
%% ��������� ������������� ������� �� SBAS ����������        
if iscell(SBAS)==1        
        Matr_SBAS(s:end,:)=[];
        sbas(s:end,:)=[];
        Data = rod(sbas,Num_Obs_SBAS);
        Matr_SBAS=[Matr_SBAS Data];
        T0=datenum(Matr_SBAS(:,1:6));%%%%%
        Matr_SBAS(:,1)=Matr_SBAS(:,4)*3600+Matr_SBAS(:,5)*60+Matr_SBAS(:,6);
        Matr_SBAS(:,2)=Matr_SBAS(:,10);
        Matr_SBAS(:,10)=[];
        Matr_SBAS(:,3:5)=nan;
        Matr_SBAS(:,6)=[];
        SBAS=[global_parameters' SBAS];
        clear sbas s
                 % ��������� ������ �������� �� �������������� ����� 
        %{
                FileH=[FileO(1:end-1) 'H'];
                Eph_SBAS = SBAS_ephemeris(FileH);
                for k=1:size(Matr_SBAS,1)
                  b=find(Eph_SBAS(:,2)==Matr_SBAS(k,2));
                       if isempty(b)==1
                            Matr_SBAS(k,3)=nan;   % X ���������� �������� � ������� ECEF
                            Matr_SBAS(k,4)=nan;   % Y ���������� �������� � ������� ECEF
                            Matr_SBAS(k,5)=nan;   % Z ���������� �������� � ������� ECEF
                         else
                         % ������� ����� ������� �������� ��������
                            Eph=Eph_SBAS(b,:); %#ok<*FNDSB>
                            [~,ii]= min(abs((Eph(:,1)-T0(k))));
                      
                            
                         T=(T0(k)-Eph(ii,1))*24*3600; % �������� ������� ������� � ��������
                         if isempty(ii)==1
                            Matr_SBAS(i,3)=nan;   % X ���������� �������� � ������� ECEF
                            Matr_SBAS(i,4)=nan;   % Y ���������� �������� � ������� ECEF
                            Matr_SBAS(i,5)=nan;   % Z ���������� �������� � ������� ECEF
                         else   
                            Matr_SBAS(k,3)=Eph(ii,3)+Eph(ii,4)*T+Eph(ii,5)*T*T/2;   % X ���������� �������� � ������� ECEF
                            Matr_SBAS(k,4)=Eph(ii,6)+Eph(ii,7)*T+Eph(ii,8)*T*T/2;   % Y ���������� �������� � ������� ECEF
                            Matr_SBAS(k,5)=Eph(ii,9)+Eph(ii,10)*T+Eph(ii,11)*T*T/2; % Z ���������� �������� � ������� ECEF
                         end
                       end
                end
        %}         
end
%% ��������� ������������� ������� �� Gal ����������        
if iscell(Gal)==1
        Matr_Gal(e:end,:)=[];
        gal(e:end,:)=[];
        Data = rod(gal,Num_Obs_Gal);
        Matr_Gal=[Matr_Gal Data];
        T0=datenum(Matr_Gal(:,1:6));%%%%%
        Matr_Gal(:,1)=Matr_Gal(:,4)*3600+Matr_Gal(:,5)*60+Matr_Gal(:,6);
        Matr_Gal(:,2)=Matr_Gal(:,10);
        Matr_Gal(:,10)=[];
        Matr_Gal(:,3:5)=nan;
        Matr_Gal(:,6)=[];
        Gal=[global_parameters' Gal];
        clear gal e
        % ��������� ������ �������� �� �������������� ����� Gal
        %{
                FileG=[FileO(1:end-1) 'G'];
                Eph_GLO = GLO_ephemeris(FileG);  
                % ������� ����� ������� �������� ��������
                    for i=1:size(Matr_GLO,1)
                            b=find(Eph_GLO(:,2)==Matr_GLO(i,2));
                            if isempty(b)==1
                                Matr_GLO(i,3)=nan;   % X ���������� �������� � ������� ECEF
                                Matr_GLO(i,4)=nan;   % Y ���������� �������� � ������� ECEF
                                Matr_GLO(i,5)=nan;   % Z ���������� �������� � ������� ECEF
                            else
                                Eph=Eph_GLO(b,:); %#ok<*FNDSB>
                                [~,ii]= min(abs((Eph(:,1)-T0(i))));
                           
                                T=(T0(i)-Eph(ii,1))*24*3600; % �������� ������� ������� � ��������
                                 if isempty(ii)==1
                                    Matr_GLO(i,3)=nan;   % X ���������� �������� � ������� ECEF
                                    Matr_GLO(i,4)=nan;   % Y ���������� �������� � ������� ECEF
                                    Matr_GLO(i,5)=nan;   % Z ���������� �������� � ������� ECEF
                                 else
                                    Matr_GLO(i,3)=Eph(ii,3)+Eph(ii,4)*T+Eph(ii,5)*T*T/2;   % X ���������� �������� � ������� ECEF
                                    Matr_GLO(i,4)=Eph(ii,6)+Eph(ii,7)*T+Eph(ii,8)*T*T/2;   % Y ���������� �������� � ������� ECEF
                                    Matr_GLO(i,5)=Eph(ii,9)+Eph(ii,10)*T+Eph(ii,11)*T*T/2; % Z ���������� �������� � ������� ECEF
                                 end
                            end
                    end
        %}
        %%mod!!  % ������� ���������� XYZ � ECEF �� ������� ����� ������ *.sp3 ����� "30s" ��� ������� ��������
                        for i=1:size(Matr_Gal,1)
                             %����� GPS � �������� �� ������ ������
                                Seconds = Matr_Gal(i,1) + 3600*24*(WD-1);
                                
                            b=find((sp3.data(:,3)==Matr_Gal(i,2) + 200)&(sp3.data(:,2)==Seconds)); %% ???
                            if ~any(b)%==1
                                Matr_Gal(i,3)=nan;   
                                Matr_Gal(i,4)=nan;
                                Matr_Gal(i,5)=nan;
                            else
                                Matr_Gal(i,3)=sp3.data(b,4);   % X ���������� �������� � ������� ECEF
                                Matr_Gal(i,4)=sp3.data(b,5);   % Y ���������� �������� � ������� ECEF
                                Matr_Gal(i,5)=sp3.data(b,6);   % Z ���������� �������� � ������� ECEF 
%                                 
                            end
                           clear b %ee ii Eph  
                        end
end

%% ������� ��������  %% MOD!!
    description=cell(28,1);
    description{1}='Eph_GLO - �������� ��������� GLO';
    description{2}='Eph_GPS - �������� ��������� GPS';
    description{3}='Eph_Gal - �������� ��������� Gal';
    
    description{5}='Fs - ������� ���������';
    
    description{7}='LeapSeconds - c���� ������� GPS ������������ UTC';
    
    description{9}='Matr_GLO - ������� � ������� �� ���� ��������� ������� GLO';
    description{10}='Matr_GPS - ������� � ������� �� ���� ��������� ������� GPS';
    description{11}='Matr_Gal - ������� � ������� �� ���� ��������� ������� Gal';
    
    description{13}='Param_GLO - ���������� ��������� ��� ������� GLO (������������ �������� Matr_GLO)';
    description{14}='Param_GPS - ���������� ��������� ��� ������� GLO (������������ �������� Matr_GPS)';
    description{15}='Param_Gal - ���������� ��������� ��� ������� Gal (������������ �������� Matr_Gal)';
    
    description{17}='PhaseShift_GLO - ����� ���� ��� ������� GLO';
    description{18}='PhaseShift_GPS - ����� ���� ��� ������� GPS';
    description{19}='PhaseShift_Gal - ����� ���� ��� ������� Gal';
    
    description{21}='RinVer - ������ rinex �����';
    
    description{23}='Time_end - ����� ��������� ������';
    description{24}='Time_start - ����� ������ ������';
    
    description{26}='Position_X - � ���������� ��������� ������ � ������� ECEF (�����)';
    description{27}='Position_Y - y ���������� ��������� ������ � ������� ECEF (�����)';
    description{28}='Position_Z - z ���������� ��������� ������ � ������� ECEF (�����)';
%% ��������� mat ����
                FileMat=[FileO(1:end-3) 'mat'];
                [~, deepestFolder, ~] = fileparts( FileMat); %�������� ������ ��� �����
                Param_GPS=GPS';
                Param_GLO=GLO';
                Param_SBAS=SBAS';
                Param_Gal=Gal';
                FileMat=[OutputPath  deepestFolder '.mat']; % ����������� ������ �������� ����
                save(FileMat,'RinVer','description','Matr_GPS','Matr_GLO','Matr_Gal','Eph_GPS','Eph_GLO','Eph_Gal','Param_GPS','Param_GLO','Param_Gal','Fs','Time_start','Time_end','LeapSeconds','Position_X','Position_Y','Position_Z','PhaseShift_GPS','PhaseShift_GLO','PhaseShift_Gal');
toc