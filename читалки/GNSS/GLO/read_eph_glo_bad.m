% 2019 02 22 kapa
% 2019 11 27 kapa (�����. ��� javad)
% ������ � ������� �������� ������� �� nav-rinex'a
function [ EPH_DMY, EPH ] = read_eph_glo( filename )

    oi_glo_struct = struct( 'Tb_sec',   double(0), ...
                            'Gamma',    double(0), ...
                            'Tau',      double(0), ...
                            'X',        double(0), ...
                            'Y',        double(0), ...
                            'Z',        double(0), ...
                            'dX',       double(0), ...
                            'dY',       double(0), ...
                            'dZ',       double(0), ...
                            'ddX',      double(0), ...
                            'ddY',      double(0), ...
                            'ddZ',      double(0), ...
                            'B',        double(0), ...
							'E',        double(0) );

	EPH = cell(1, 24);
	for i=1:24
		EPH{i} = repmat(oi_glo_struct, 1, 48); % �� ������ 48 ������� �� �������
	end
	eph_cntr = ones(1,24);
	
	
	f = fopen(filename, 'rt');
	%% ���� ����� ���������
	for i=1:7 % ������ ��������� 4-5 �����, �� ���� ��...
		str = fgetl(f);
		s = str(61:end);
		if ( strcmp(s, 'END OF HEADER') == 1)
			break;
		end
	end
	%% ������ ����� ��������� ������ ��

	while(~feof(f))
		str1 = fgetl(f);
		if (str1 == -1)
			break;
		end

		str2 = fgetl(f);
		if (str2 == -1)
			break;
		end

		str3 = fgetl(f);
		if (str3 == -1)
			break;
		end

		str4 = fgetl(f);
		if (str4 == -1)
			break;
		end
		
		% ��� 4 ������ ��, ���������
		% ������ ������: ����� ���, ��� ����� ���� ���� ������ �������, -tau, gamma, tk_utc (������ �����)
		nsv		= str2num(str1(1:3));
		year	= str2num(str1(4:5));
		month	= str2num(str1(8:9));
		day		= str2num(str1(10:11));
		hour	= str2num(str1(13:14));
		min		= str2num(str1(14:15));
		sec		= str2num(str1(16:17));
		tau_inv = str2num(str1(23:41));	% � rinex'e � tau ������� ����
		gamma	= str2num(str1(42:60));
		tk_utc	= str2num(str1(61:79));
		% ������ ������: X Vx Ax (�� �� ��), ����������� B (0=OK)
		X	= str2num(str2(1:22));
		dX	= str2num(str2(23:41));
		ddX	= str2num(str2(42:60));
		B	= str2num(str2(61:79));
		% ������ ������: Y Vy Ay (�� �� ��), ������
		Y	= str2num(str3(1:22));
		dY	= str2num(str3(23:41));
		ddY	= str2num(str3(42:60));
		lit	= str2num(str3(61:79));
		% ��������� ������: Y Vy Ay (�� �� ��), ������� �� (E) � ����
		Z	= str2num(str4(1:22));
		dZ	= str2num(str4(23:41));
		ddZ	= str2num(str4(42:60));
		E	= str2num(str4(61:79));
		
%!!! Tb ���������� ������ ����� UTC, � �� ���, �.�. ��� ������� 3 ���� �������
		% ������� 3 ����
		Tb = ((hour+3) * 3600 + min * 60 + sec)/(900); % Tb - ��� ����� 15��� ���������
		if (Tb > 96) 
			Tb = Tb - 96;
		end
		
		% ��������� ��������� ��� ���������
		if ( (nsv >= 1) && (nsv <= 24) )
			j = eph_cntr(nsv);
			
			EPH{nsv}(j).Tb_sec	= Tb*900;
			EPH{nsv}(j).Gamma	= gamma;
			EPH{nsv}(j).Tau		= -tau_inv;
			EPH{nsv}(j).X		= X;
			EPH{nsv}(j).Y		= Y;
			EPH{nsv}(j).Z		= Z;
			EPH{nsv}(j).dX		= dX;
			EPH{nsv}(j).dY		= dY;
			EPH{nsv}(j).dZ		= dZ;
			EPH{nsv}(j).ddX		= ddX;
			EPH{nsv}(j).ddY		= ddY;
			EPH{nsv}(j).ddZ		= ddZ;
			EPH{nsv}(j).B		= B;
			EPH{nsv}(j).E		= E;
			
			eph_cntr(nsv) = j + 1;
		else
			fprintf('�������� ����� ��� = %d\n', nsv);
		end
				
	end
	
	EPH_DMY = [day month year];
	
	fclose(f);
	
	% ������� ���������������� ���� � �������
	for i=1:24
% 		if (eph_cntr(i) > 1)
			EPH{i} = EPH{i}(1:eph_cntr(i)-1);
% 		end
	end

end

