function [X_est,Dx_est,nev] = EKF_UWB_v2(X_est_prev,Dx_est_prev,Y,Y_prev,SatPos)

%% предобработка, ОЦЕНИВАЕМ Delta,k на текущий момент экстраполяции измерений
    c = 299792458;
%   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    T_nom = 0.0978623589687248;% номинальный интервал между излучениями, сек
    T_min = 0.088; % минимальный интервал между излучениями, сек 
    delta_aloxa = 1e-3; % дискрет по времени в алохе, сек
    toa_diff = Y-Y_prev-T_min; % секунды, Y-сек
    non_nan_ind = ~isnan(toa_diff); 
    m_est_raw = toa_diff(non_nan_ind)/delta_aloxa; % с плавающей точкой
    m_est = round(m_est_raw(1)); % целое число от 0 до 20
    % !!!!!!!!!!!!!!!!!!!!!!!
    % засовываем в дельту разницу расстояний
    [X_mnk, ~, ~, flag] = coord_solver2D(Y(non_nan_ind)*c, SatPos(:,non_nan_ind), X_est_prev([1,3,5]), 0);
%     Delta_k = T_min+m_est_raw(1)*delta_aloxa;% сек
    Delta_k = (X_mnk(3)-X_est_prev(5))/c;
%% матрички
%   X_est = [x_k; Vx_k;y_k; Vy_k;T_tr_k];
    F = [1 Delta_k 0 0       0;
         0 1       0 0       0;
         0 0       1 Delta_k 0;
         0 0       0 1       0;
         0 0       0 0       1];
    G = [0       0      ;
         Delta_k 0      ;
         0       0      ;
         0       Delta_k;
         0       0      ];
    
    sigma_ksi_vx = 0.5; % CКО движения в м/c 
    sigma_ksi_vy = 0.5; % CКО движения в м/c 
    D_ksi = diag([sigma_ksi_vx^2 sigma_ksi_vy^2]);
    
    sigma_n = 10e-2; % СКО измерений, метры
    D_n = diag(sigma_n*ones(1,size(SatPos(:,non_nan_ind),2)));
    
    U = [0;0;0;0;1]; % вектор управления
    B = [0 0 0 0 0;
         0 0 0 0 0;
         0 0 0 0 0;
         0 0 0 0 0;
         0 0 0 0 Delta_k*c];% матрица управления
%% перевод наблюдений в метры
    y = Y*c; %метры
%% экстраполяция
    X_ext = F*X_est_prev + B*U; % X_est_prev - метры 
    D_ext = F*Dx_est_prev*F' + G*D_ksi*G';
%% оценка
    dS1 = dS_matrix(X_ext,SatPos(:,non_nan_ind)); % градиентная матрица
    S =  S_matrix(X_ext,SatPos(:,non_nan_ind)); % невязка (экстраполяция наблюдений)
    K = D_ext * dS1' * inv(dS1 * D_ext * dS1' + D_n);
    Dx_est = D_ext - K * dS1 * D_ext;
    X_est = X_ext + K * (y(non_nan_ind) - S);
    nev = norm(y(non_nan_ind) - S);
end
%%
function [S] = S_matrix(x,SatPos)
   c = 299792458;
%     SatPos размер 3 на N
        S = zeros(size(SatPos,2),1);
    for i = 1:size(SatPos,2)% не нан
        R = sqrt( (x(1)-SatPos(1,i))^2 + (x(3)-SatPos(2,i))^2 );
        S(i,1) = x(5)+R; %Экстраполяция измерение, в метрах (S-время приема)
    end
end
%%
function [dS] = dS_matrix(x,SatPos)
    c = 299792458;
%     SatPos размер 3 на N (x,y,z)
    dS = zeros(size(SatPos,2),5);
    for i = 1:size(SatPos,2)% не нан
        R = sqrt( (x(1)-SatPos(1,i))^2 + (x(3)-SatPos(2,i))^2 );
        dS(i,1) = (x(1)-SatPos(1,i))/R; %dYdx
        dS(i,2) = 0;                    %dYdVx
        dS(i,3) = (x(2)-SatPos(2,i))/R; %dYdy
        dS(i,4) = 0;                    %dYdVy
        dS(i,5) = 1;                    %dYdTrt
    end
end