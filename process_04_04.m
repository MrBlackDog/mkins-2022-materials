%% MNK
k = 0;
cur_t = uwb_t_new;
    for i = 1:length(cur_t)
    k = k + 1;
%     nums = find(cur_t(:,i));
    nums = find(~isnan(cur_t(:,i)));
        if length(nums) > 2
            
            [X, dop, nev, flag] = coord_solver2D(cur_t(nums,i)*c, SatPos(:,nums), [30;30;max(cur_t(nums,i))*c], 0);
            if flag 
               
               cords_new_GPST(:,k) = [X];
            else
                cords_new_GPST(:,k)= [NaN;NaN;NaN];
            end
        else
                cords_new_GPST(:,k)= [NaN;NaN;NaN];
        end
    end
 %% EKF
X_est_prev = [cords_new2(1,193);2;cords_new2(2,193);-5;cords_new2(3,193)];% cords_new2 - оценка по МНК в ШВ мастера
% в нач приближении можно брать скорость по двум МНК
X_est2(:,193) = X_est_prev; % 172 или 193
X_est(:,193) = X_est_prev;
Dx_est_prev = diag(ones(1,5));
Dx_est_prev2 = diag(ones(1,5));
for i = 194:256% 173 - 256 весь первый кусок, 194-256 - три и более наблюдения
    if(i==216)
        disp(i)
    end
%     [X_est(:,i),Dx_est,nev(:,i)] =
%     EKF_UWB(X_est_prev,Dx_est_prev,uwb_toa(:,i),uwb_toa(:,i-1),SatPos);%с расчетом дельты
    [X_est(:,i),Dx_est,nev(:,i)] = EKF_UWB(X_est(:,i-1),Dx_est_prev,uwb_toa(:,i),uwb_toa(:,i-1),SatPos); % старая версия расчета через m
    [X_est2(:,i),Dx_est2,nev2(:,i)] = EKF_UWB_v3(X_est2(:,i-1),Dx_est_prev2,uwb_toa(:,i),uwb_toa(:,i-1),SatPos); % новая версия расчета дельты
    Dx_est_prev = Dx_est;
    Dx_est_prev2 = Dx_est2;
end
%% координаты в плане
figure()
plot(X_est2(1,193:256),X_est2(3,193:256),'x-')
hold on
plot(X_est(1,193:256),X_est(3,193:256),'o-')
plot(cords_new2(1,193:256),cords_new2(2,193:256),'.- g')
plot(rtk_enu(2,:),rtk_enu(3,:),'.-')
legend('Новый фильтр','Фильтр через m','МНК','РТК')
%% кооридната x от тиков
figure()
plot(193:256,X_est2(1,193:256),'x')
hold on
plot(193:256,cords_new2(1,193:256),'.')
%% кооридната y от тиков
figure()
plot(193:256,X_est2(3,193:256),'x')
hold on
plot(193:256,cords_new2(2,193:256),'.')
%% Vx от тиков
figure()
plot(193:256,X_est2(2,193:256),'x')
%% Vy от тиков
figure()
plot(193:256,X_est2(4,193:256),'x')