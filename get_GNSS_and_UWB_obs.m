function [gnss,uwb] = get_GNSS_and_UWB_obs(obs)

k1 = 0;
k2 = 0;

for i = 1:length(obs)
    
    if(~isempty(obs(i).GPS_OBS) && ~isempty(obs(i).GLO_OBS) && isempty(obs(i).UWB_OBS))
        k1 = k1 + 1;
        gnss(k1).TOW = obs(i).TOW;
        gnss(k1).GPS_obs = obs(i).GPS_OBS;
        gnss(k1).GLO_obs = obs(i).GLO_OBS;
    elseif(isempty(obs(i).GPS_OBS) && isempty(obs(i).GLO_OBS) && ~isempty(obs(i).UWB_OBS))
        k2 = k2 + 1;
        uwb(k2).TOW = obs(i).TOW;
        uwb(k2).UWB_obs = obs(i).UWB_OBS;
    end
    
end

end