function [car_enu] = toENU(car)
    enu = [55.7550107860000;37.7060240010000;161.971100000000];
    for i = 1:size(car,2)
        car_enu(1,i) = car(1,i);
        [car_enu(2,i),car_enu(3,i),car_enu(4,i)] = geodetic2enu(car(2,i),car(3,i),car(4,i),enu(1),enu(2),enu(3),wgs84Ellipsoid);
    end
end

